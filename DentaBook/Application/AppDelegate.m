//
//  AppDelegate.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "DBMainViewController.h"
#import "DBPatientsViewController.h"
#import "DBSideMenuViewController.h"
#import "LoginViewController.h"

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "User+Custom.h"
#import "DentalDiagnosis.h"
#import "AdditionDentalDiagnosis.h"
#import "PatientIllness.h"
#import "DataManager.h"

#import "CDManager.h"

#import "DBLayoutManager.h"
#import "DataBase.h"
#import "DiagnosisManager.h"
#import "IllnessManager.h"
#import "KeyboardListener.h"
#import <MessageUI/MessageUI.h>
#import "UILabel+FontAppearance.h"

//denisdbv.DentaBook
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /*UIDatePicker *picker = [UIDatePicker appearance];
    picker.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    
    UIView *view;
    view = [UIView appearanceWhenContainedIn:[UITableView class], [UIDatePicker class], nil];
    view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];*/
    
    //UILabel *label = [UILabel appearanceWhenContainedIn:/*[UITableView class],*/ [UIDatePickerContentView class], nil];
    //label.font = [UIFont systemFontOfSize:14];
    //label.textColor = [UIColor blueColor];
    
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:72.0/255.0 green:100.0/255.0 blue:124.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    //[[UILabel appearanceWhenContainedIn:UIAlertController.class, nil] setAppearanceFont:nil];
    //[[UIButton appearanceWhenContainedIn:[MFMailComposeViewController class], nil] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[[UILabel appearanceWhenContainedIn:[MFMailComposeViewController class], nil] setTextColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor colorWithWhite:74 alpha:1]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];
    
    [Fabric with:@[[Crashlytics class]]];
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    //[self createTempData];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if([User currentUser] && [DataManager loadCookies])  {
        [self switchToApp];
    } else  {
        [self switchToLogin];
    }
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    UIView* statusBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, 20)];
    statusBg.backgroundColor = [UIColor colorWithRed:45.0/255.0 green:62.0/255.0 blue:80.0/255.0 alpha:1.0];
    //Add the view behind the status bar
    [self.window.rootViewController.view addSubview:statusBg];
    //set the constraints to auto-resize
    statusBg.translatesAutoresizingMaskIntoConstraints = NO;
    [statusBg.superview addConstraint:[NSLayoutConstraint constraintWithItem:statusBg attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:statusBg.superview attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [statusBg.superview addConstraint:[NSLayoutConstraint constraintWithItem:statusBg attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:statusBg.superview attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [statusBg.superview addConstraint:[NSLayoutConstraint constraintWithItem:statusBg attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:statusBg.superview attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [statusBg.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[statusBg(==20)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(statusBg)]];
    [statusBg.superview setNeedsUpdateConstraints];
    self.statusBg = statusBg;
    
    NSArray* diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    if(diagnoses.count==0)
    {
        [[DiagnosisManager shared] createDiagnosisByTitle:@"Пломба" subTitle:@"П" success:^{

        } failure:^{

        }];

    }

    diagnoses = [DataBase getDBArray:@"illness" withKey:@"" andValue:@""];
    if(diagnoses.count==0)
    {
        [[IllnessManager shared] createIllnessByTitle:@"Беременность" success:^{
            
        } failure:^{
            
        }];
        
        [[IllnessManager shared] createIllnessByTitle:@"Гипертония 1 степень" success:^{

        } failure:^{

        }];
        
        [[IllnessManager shared] createIllnessByTitle:@"Гипертония 2 степень" success:^{
            
        } failure:^{
            
        }];
        
    }
    
    diagnoses = [DataBase getDBArray:@"services_group" withKey:@"" andValue:@""];
    if(diagnoses.count==0)
    {
        NSArray* cats= @[@{@"id":GUIDString(), @"title": @"ОБЩИЕ"}, @{@"id":GUIDString(), @"title": @"ИССЛЕДОВАНИЯ"},@{@"id":GUIDString(), @"title": @"ТЕРАПИЯ"},@{@"id":GUIDString(), @"title": @"ГИГИЕНА / ПАРОДОНТОЛОГИЯ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / ОБЩЕЕ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / НЕСЪЕМНОЕ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / НА ИМПЛАНТАНТАХ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / УДАЛЕНИЕ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / ОПЕРАЦИИ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / ПРОЧЕЕ"},@{@"id":GUIDString(), @"title": @"ИМПЛАНТАЦИЯ"}];
        
        int i=0;
        
        
        
        for(NSDictionary* dd in cats)
        {
            [DataBase putDB:dd toTable:@"services_group" withKey:@"title"];
            NSArray* servs;
            if(i==0)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Анестезия", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коффердам / optragate", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Микроскоп", @"cost":@(2970)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Ретракция десны", @"cost":@(396)}
                         ];
            if(i==1)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Прицельный снимок", @"cost":@(396)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Панорамный снимок", @"cost":@(2600)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Диагностическая модель", @"cost":@(1485)},
                         ];
            if(i==2)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Реставрация", @"cost":@(7425)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Эстетическая реставрация", @"cost":@(9900)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 1 канал", @"cost":@(2475)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 2 канала", @"cost":@(3960)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 3 канала", @"cost":@(5940)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка доп канала", @"cost":@(3960)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канал", @"cost":@(4950)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канала", @"cost":@(5940)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канала", @"cost":@(8910)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill доп канала", @"cost":@(5940)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Распломбировка 1 канал", @"cost":@(3960)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временное пломбирование 1 канал", @"cost":@(2475)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Повторная мех и мед обработка 1 канал", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Извлечение инородного тела", @"cost":@(14850)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление штифта анкерного", @"cost":@(3465)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"ProRoot", @"cost":@(4950)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба СИЦ", @"cost":@(4455)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Штифт Unimetric", @"cost":@(1980)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Штифт DT Light Post", @"cost":@(2475)},
                         ];
            if(i==3)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"у/з 1 зуб", @"cost":@(264)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Airflow 1 зуб", @"cost":@(264)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Полировка пастой 1 зуб", @"cost":@(247)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Глубокое фторирование 1 зуб", @"cost":@(264)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Кюретаж закрытый 1 зуб", @"cost":@(990)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мед обработка 1 з/д кармана", @"cost":@(247)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Шинирование 1 зуб", @"cost":@(1980)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Отбеливание офисное Zoom 3", @"cost":@(27225)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Отбеливание домашнее Zoom", @"cost":@(12375)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Каппа для домашнего отбеливания 1 шт", @"cost":@(7425)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Обработка Vector 1 зуб", @"cost":@(990)}
                         ];
            if(i==4)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Снятие слепка", @"cost":@(1485)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Диагностическая модель", @"cost":@(1485)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Восковое моделирование 1 зуб", @"cost":@(1485)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Ретракция десны", @"cost":@(396)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальная ложка", @"cost":@(2475)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Каппа при бруксизме 1 шт", @"cost":@(7425)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Фиксация", @"cost":@(1485)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Снятие коронки", @"cost":@(485)}];
            if(i==5)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка", @"cost":@(27225)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка Au сплав", @"cost":@(44550)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка ZrO", @"cost":@(64350)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"коронка, вкладка, винир EMax", @"cost":@(32175)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая", @"cost":@(13860)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая разборная", @"cost":@(17325)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая Au сплав", @"cost":@(39600)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая ZrO", @"cost":@(8910)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая CoCr", @"cost":@(8910)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коронка временная \"прямая\"", @"cost":@(3465)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коронка временная \"лабораторная\"", @"cost":@(6000)}];
            if(i==6)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка c опорой на имплантант", @"cost":@(29700)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка Au сплав c опорой на имплантант", @"cost":@(69300)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка ZrO c опорой на имплантант", @"cost":@(74250)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Стандартный абантмент AlphaBio, Miss", @"cost":@(19800)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Стандартный абантмент Ankylos, Nobel, AstraTech", @"cost":@(26730)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальный абантмент", @"cost":@(42075)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальный абантмент ZrO", @"cost":@(45540)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Хирургический шаблон", @"cost":@(26730)}];
            if(i==7)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление зуба простое", @"cost":@(1980)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление зуба сложное", @"cost":@(4455)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление ретенированного, достопированного зуба", @"cost":@(5940)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление ретенированного, достопированного зуба сложное", @"cost":@(8910)}];
            if(i==8)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Гингивоэктомия", @"cost":@(1980)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Альвеолоэктомия", @"cost":@(2970)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Резекция корня зуба", @"cost":@(6435)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пластика уздечки", @"cost":@(5940)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вскрытие и дренирование абсцесса", @"cost":@(1485)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удлинение коронковой части зуба", @"cost":@(4455)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Закрытие соустья с ВЧ синусом", @"cost":@(7425)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Гингивопластика", @"cost":@(4950)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Увеличение высоты/ширины альвеолярного гребня", @"cost":@(19800)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление исплантанта", @"cost":@(24750)}];
            if(i==9)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Кюретаж лунки", @"cost":@(759)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Наложение (снятие) 1-го шва", @"cost":@(363)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Лечебная повязка", @"cost":@(990)},
                         ];
            if(i==10)
                servs= @[
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Имплантант AlphaBio, Miss", @"cost":@(69300)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Имплантант Ankylos, Nobel, AstraTech", @"cost":@(110880)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мембрана Bio-Gide", @"cost":@(22275)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мемрана Gore-Tex, Resolute", @"cost":@(32175)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Костнозаменяющий материал Bio-Oss", @"cost":@(14850)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Костнозаменяющий материал Pep-Gen", @"cost":@(19800)},
                         @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Синус лифтинг", @"cost":@(79200)}
                         ];
            for(NSDictionary* ddd in servs)
            {
                [DataBase putDB:ddd toTable:@"services" withKey:@"id"];
            }
            
            i++;
        }
    }
    
    [KeyboardListener sharedInstance];

    
    return YES;
}


-(void) switchToLogin   {
    LoginViewController *loginVC = [LoginViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC];
    self.window.rootViewController = navigationController;
}

-(void) switchToApp {
    //[[DBLayoutManager shared] openSyncScreen];
    
    DBPatientsViewController *patientsViewController = [DBPatientsViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:patientsViewController];
    self.sideMenuViewController = [[DBSideMenuViewController alloc] initWithRootViewController:navigationController];
    self.window.rootViewController = self.sideMenuViewController;
}

-(void) createTempData  {
    
//    if([Patient MR_findAll].count > 0)
//        return;
    
    User *user = [User MR_createEntity];
    user.remoteId = @(1);
    
    [Patient MR_truncateAll];
    
    [self createPatientWithFirstName:@"Артименко" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    [self createPatientWithFirstName:@"Артименко" middleName:@"Григорий" lastName:@"Иванович" rating:5];
    [self createPatientWithFirstName:@"Артименко" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    
    [self createPatientWithFirstName:@"Бобер" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    [self createPatientWithFirstName:@"Бобер" middleName:@"Григорий" lastName:@"Иванович" rating:1];
    
    [self createPatientWithFirstName:@"Волков" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    [self createPatientWithFirstName:@"Волков" middleName:@"Григорий" lastName:@"Иванович" rating:2];
    [self createPatientWithFirstName:@"Волков" middleName:@"Григорий" lastName:@"Иванович" rating:1];
    [self createPatientWithFirstName:@"Волков" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    
    [self createPatientWithFirstName:@"Деонисов" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    [self createPatientWithFirstName:@"Деонисов" middleName:@"Григорий" lastName:@"Иванович" rating:4];
    [self createPatientWithFirstName:@"Деонисов" middleName:@"Григорий" lastName:@"Иванович" rating:0];
    
    //Категории и услуги
    [Service MR_truncateAll];
    [CategoryTherapy MR_truncateAll];
    
    NSArray* cats= @[@{@"id":GUIDString(), @"title": @"ОБЩИЕ"}, @{@"id":GUIDString(), @"title": @"ИССЛЕДОВАНИЯ"},@{@"id":GUIDString(), @"title": @"ТЕРАПИЯ"},@{@"id":GUIDString(), @"title": @"ГИГИЕНА / ПАРОДОНТОЛОГИЯ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / ОБЩЕЕ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / НЕСЪЕМНОЕ"},@{@"id":GUIDString(), @"title": @"ОРТОПЕДИЯ / НА ИМПЛАНТАНТАХ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / УДАЛЕНИЕ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / ОПЕРАЦИИ"},@{@"id":GUIDString(), @"title": @"ХИРУРГИЯ / ПРОЧЕЕ"},@{@"id":GUIDString(), @"title": @"ИМПЛАНТАЦИЯ"}];
    
    int i=0;
    

    
    for(NSDictionary* dd in cats)
    {
        [DataBase putDB:dd toTable:@"services_group" withKey:@"title"];
        NSArray* servs;
        if(i==0)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Анестезия", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коффердам / optragate", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Микроскоп", @"cost":@(2970)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Ретракция десны", @"cost":@(396)}
                    ];
        if(i==1)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Прицельный снимок", @"cost":@(396)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Панорамный снимок", @"cost":@(2600)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Диагностическая модель", @"cost":@(1485)},
                     ];
        if(i==2)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Реставрация", @"cost":@(7425)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Эстетическая реставрация", @"cost":@(9900)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 1 канал", @"cost":@(2475)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 2 канала", @"cost":@(3960)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка 3 канала", @"cost":@(5940)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мех и мед обработка доп канала", @"cost":@(3960)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канал", @"cost":@(4950)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канала", @"cost":@(5940)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill 1 канала", @"cost":@(8910)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пломбирование BeeFill доп канала", @"cost":@(5940)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Распломбировка 1 канал", @"cost":@(3960)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временное пломбирование 1 канал", @"cost":@(2475)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Повторная мех и мед обработка 1 канал", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Извлечение инородного тела", @"cost":@(14850)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление штифта анкерного", @"cost":@(3465)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"ProRoot", @"cost":@(4950)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Временная пломба СИЦ", @"cost":@(4455)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Штифт Unimetric", @"cost":@(1980)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Штифт DT Light Post", @"cost":@(2475)},
                     ];
        if(i==3)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"у/з 1 зуб", @"cost":@(264)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Airflow 1 зуб", @"cost":@(264)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Полировка пастой 1 зуб", @"cost":@(247)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Глубокое фторирование 1 зуб", @"cost":@(264)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Кюретаж закрытый 1 зуб", @"cost":@(990)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мед обработка 1 з/д кармана", @"cost":@(247)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Шинирование 1 зуб", @"cost":@(1980)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Отбеливание офисное Zoom 3", @"cost":@(27225)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Отбеливание домашнее Zoom", @"cost":@(12375)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Каппа для домашнего отбеливания 1 шт", @"cost":@(7425)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Обработка Vector 1 зуб", @"cost":@(990)}
                     ];
        if(i==4)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Снятие слепка", @"cost":@(1485)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Диагностическая модель", @"cost":@(1485)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Восковое моделирование 1 зуб", @"cost":@(1485)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Ретракция десны", @"cost":@(396)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальная ложка", @"cost":@(2475)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Каппа при бруксизме 1 шт", @"cost":@(7425)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Фиксация", @"cost":@(1485)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Снятие коронки", @"cost":@(485)}];
        if(i==5)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка", @"cost":@(27225)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка Au сплав", @"cost":@(44550)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка ZrO", @"cost":@(64350)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"коронка, вкладка, винир EMax", @"cost":@(32175)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая", @"cost":@(13860)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая разборная", @"cost":@(17325)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая Au сплав", @"cost":@(39600)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая ZrO", @"cost":@(8910)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вкладка культевая CoCr", @"cost":@(8910)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коронка временная \"прямая\"", @"cost":@(3465)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Коронка временная \"лабораторная\"", @"cost":@(6000)}];
        if(i==6)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка c опорой на имплантант", @"cost":@(29700)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка Au сплав c опорой на имплантант", @"cost":@(69300)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"м/к коронка ZrO c опорой на имплантант", @"cost":@(74250)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Стандартный абантмент AlphaBio, Miss", @"cost":@(19800)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Стандартный абантмент Ankylos, Nobel, AstraTech", @"cost":@(26730)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальный абантмент", @"cost":@(42075)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Индивидуальный абантмент ZrO", @"cost":@(45540)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Хирургический шаблон", @"cost":@(26730)}];
        if(i==7)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление зуба простое", @"cost":@(1980)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление зуба сложное", @"cost":@(4455)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление ретенированного, достопированного зуба", @"cost":@(5940)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление ретенированного, достопированного зуба сложное", @"cost":@(8910)}];
        if(i==8)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Гингивоэктомия", @"cost":@(1980)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Альвеолоэктомия", @"cost":@(2970)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Резекция корня зуба", @"cost":@(6435)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Пластика уздечки", @"cost":@(5940)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Вскрытие и дренирование абсцесса", @"cost":@(1485)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удлинение коронковой части зуба", @"cost":@(4455)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Закрытие соустья с ВЧ синусом", @"cost":@(7425)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Гингивопластика", @"cost":@(4950)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Увеличение высоты/ширины альвеолярного гребня", @"cost":@(19800)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Удаление исплантанта", @"cost":@(24750)}];
        if(i==9)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Кюретаж лунки", @"cost":@(759)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Наложение (снятие) 1-го шва", @"cost":@(363)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Лечебная повязка", @"cost":@(990)},
                     ];
        if(i==10)
            servs= @[
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Имплантант AlphaBio, Miss", @"cost":@(69300)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Имплантант Ankylos, Nobel, AstraTech", @"cost":@(110880)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мембрана Bio-Gide", @"cost":@(22275)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Мемрана Gore-Tex, Resolute", @"cost":@(32175)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Костнозаменяющий материал Bio-Oss", @"cost":@(14850)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Костнозаменяющий материал Pep-Gen", @"cost":@(19800)},
                     @{@"category_id":dd[@"id"], @"id":GUIDString(), @"title": @"Синус лифтинг", @"cost":@(79200)}
                     ];
        for(NSDictionary* ddd in servs)
        {
            [DataBase putDB:ddd toTable:@"services" withKey:@"id"];
        }
        i++;
    }
    
    
    [DentalDiagnosis MR_truncateAll];
    [AdditionDentalDiagnosis MR_truncateAll];
    [PatientIllness MR_truncateAll];
    
    DentalDiagnosis *dentaD1 = [DentalDiagnosis MR_createEntity];
    dentaD1.remoteId = @(1);
    dentaD1.name = @"Кариес 1";
    dentaD1.symbolName = @"К1";
    DentalDiagnosis *dentaD2 = [DentalDiagnosis MR_createEntity];
    dentaD2.remoteId = @(2);
    dentaD2.name = @"Кариес 2";
    dentaD2.symbolName = @"К2";
    DentalDiagnosis *dentaD3 = [DentalDiagnosis MR_createEntity];
    dentaD3.remoteId = @(3);
    dentaD3.name = @"Кариес 3";
    dentaD3.symbolName = @"К3";
    DentalDiagnosis *dentaD4 = [DentalDiagnosis MR_createEntity];
    dentaD4.remoteId = @(4);
    dentaD4.name = @"Кариес 4";
    dentaD4.symbolName = @"К4";
    DentalDiagnosis *dentaD5 = [DentalDiagnosis MR_createEntity];
    dentaD5.remoteId = @(5);
    dentaD5.name = @"Кариес 5";
    dentaD5.symbolName = @"К5";
    DentalDiagnosis *dentaD6 = [DentalDiagnosis MR_createEntity];
    dentaD6.remoteId = @(6);
    dentaD6.name = @"Кариес 6";
    dentaD6.symbolName = @"К6";
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    PatientIllness *adentaD1 = [PatientIllness MR_createEntity];
    adentaD1.remoteId = @(1);
    adentaD1.name = @"Гепатит В";
    PatientIllness *adentaD2 = [PatientIllness MR_createEntity];
    adentaD2.remoteId = @(2);
    adentaD2.name = @"Сахарный диабет";
    PatientIllness *adentaD3 = [PatientIllness MR_createEntity];
    adentaD3.remoteId = @(3);
    adentaD3.name = @"Повышенное давление";
    PatientIllness *adentaD4 = [PatientIllness MR_createEntity];
    adentaD4.remoteId = @(4);
    adentaD4.name = @"Шизофрения";
    PatientIllness *adentaD5 = [PatientIllness MR_createEntity];
    adentaD5.remoteId = @(5);
    adentaD5.name = @"Эпилепсия";
    PatientIllness *adentaD6 = [PatientIllness MR_createEntity];
    adentaD6.remoteId = @(6);
    adentaD6.name = @"Гепатит А";
    PatientIllness *adentaD7 = [PatientIllness MR_createEntity];
    adentaD7.remoteId = @(7);
    adentaD7.name = @"Гепатит С";
    PatientIllness *adentaD8 = [PatientIllness MR_createEntity];
    adentaD8.remoteId = @(8);
    adentaD8.name = @"Гепатит Е";
    PatientIllness *adentaD9 = [PatientIllness MR_createEntity];
    adentaD9.remoteId = @(9);
    adentaD9.name = @"Гепатит Д";
    PatientIllness *adentaD10 = [PatientIllness MR_createEntity];
    adentaD10.remoteId = @(10);
    adentaD10.name = @"Гепатит Ж";
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    NSMutableArray *arr = [NSMutableArray new];
    NSArray *set = [CategoryTherapy MR_findAll];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"remoteId" ascending:YES];
    NSArray *sorted = [set sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    
    for(CategoryTherapy *catTherapy in sorted)    {
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:catTherapy.name forKey:@"title"];
        
        NSMutableArray *arr2 = [NSMutableArray new];
        NSSet *set = catTherapy.services;
        NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"remoteId" ascending:YES];
        NSArray *sorted = [set sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
        for(Service *srv in sorted) {
            NSDictionary *ddd = @{
                                  @"title": srv.name,
                                  @"cost": srv.cost
                                  };
            [arr2 addObject:ddd];
        }
        [dict setObject:arr2 forKey:@"items"];
        [arr addObject:dict];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(jsonString);
    }
}

-(void) createPatientWithFirstName:(NSString*)firstName
                        middleName:(NSString*)middleName
                          lastName:(NSString*)lastName
                            rating:(NSInteger)rating    {
    
    static int remoteId = 0;
    
    Patient *patient = [Patient MR_createEntity];
    patient.remoteId = @(remoteId++);
    patient.firstName = firstName;
    patient.middleName = middleName;
    patient.lastName = lastName;
    patient.rating = @(rating);
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"Resign active %@", application);
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"Enter foreground %@", application);
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"becomeActive" object:nil];
    NSLog(@"Become active %@", application);
}

- (void)applicationWillTerminate:(UIApplication *)application {
    //[MagicalRecord cleanUp];
    [[CDManager shared] applicationWillTerminate];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "denisdbv.DentaBook" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DentaBook" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DentaBook.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
