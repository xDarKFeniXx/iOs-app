//
//  ImageCacheManager.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "ImageCacheManager.h"


@interface ImageCacheManager()

@end


@implementation ImageCacheManager
#pragma mark _______________________ Class Methods _________________________

#pragma mark - Shared Instance and Init
+ (ImageCacheManager *)shared {
    
    static ImageCacheManager *shared = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

#pragma mark ____________________________ Init _____________________________

-(id)init{
    
    self = [super init];
    if (self) {
        
        //! Setup defaults
        self.defaultFileCache = [[ImageFileCache alloc] init];
        self.defaultMemoryCach = [[ImageCache alloc] init];
        
    }
    return self;
}

#pragma mark _______________________ Privat Methods ________________________



#pragma mark _______________________ Delegates _____________________________



#pragma mark _______________________ Public Methods ________________________



#pragma mark _______________________ Notifications _________________________



@end
