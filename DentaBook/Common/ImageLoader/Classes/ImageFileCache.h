//
//  ImageFileCache.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFileCache : NSObject

@property (strong, nonatomic) NSString* cachePath;

@property (assign, nonatomic) NSInteger maxCacheAge;
@property (assign, nonatomic) NSUInteger maxCacheSize;

#pragma mark - Cache
//! Search image and return result
- (void)imageForKey:(NSString*)md5Hash
            didFind:(void(^)(ImageFileCache* cache, UIImage* image, NSString* md5Hash))find
    didFindOriginal:(void(^)(ImageFileCache* cache, UIImage* image, NSString* md5Hash))findOriginal
          didNoFind:(void(^)(ImageFileCache* cache))notFind;

//! Method to cache image
- (void)cacheImage:(UIImage*)image forKey:(NSString *)key;
- (void)deleteImageForKey:(NSString *)key;

#pragma mark - Remove or clean
//! Method remove all files from cache folder
- (void)removeAllFiles;

//! Get cached files count and size
- (void)calculateSizeWithCompletionBlock:(void (^)(NSUInteger fileCount, NSUInteger totalSize))completionBlock;

//! Clean cache in background task
- (void)backgroundCleanDisk;

//! Clean cache asyncronously
- (void)cleanDiskWithCompletionBlock:(void(^)())completionBlock;


@end
