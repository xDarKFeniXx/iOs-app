//
//  LoadOperation.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Load image operation interface.
@interface LoadOperation : NSOperation

//! Parameters
@property (strong, nonatomic) NSString* md5Hash;
@property (assign, nonatomic) BOOL isInProgress;
@property (assign, nonatomic) CGFloat progress;

//! Rquest and connection
@property (nonatomic, strong) NSURLRequest* request;
@property (nonatomic, strong) NSMutableData* connectionData;
@property (strong, nonatomic) NSURLConnection* connection;
@property (strong, nonatomic) NSURLResponse *response;

//! Call backs
@property (nonatomic, copy) UIImage* (^processingBlock)(LoadOperation* operation, NSData* connectionData);
@property (nonatomic, copy) void (^successBlock)(LoadOperation* operation, UIImage* image);
@property (nonatomic, copy) void (^failureBlock)(LoadOperation* operation, NSError* error);
@property (nonatomic, copy) void (^progressBlock)(LoadOperation* operation, float progress);
@property (nonatomic, copy) void (^cancelBlock)(LoadOperation* operation);

- (id)initWithRequest:(NSURLRequest*)request;

@end

