//
//  LoadOperation.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "LoadOperation.h"

#import <libextobjc/extobjc.h>

@interface LoadOperation() <NSURLConnectionDataDelegate>
@end

//! Load image operation implementation.
@implementation LoadOperation

- (void)dealloc
{
    //NSLog(@"LoadOperation DEALLOC");
}

- (id)initWithRequest:(NSURLRequest *)urlRequest{
    self = [super init];
    if (self) {
        self.request = urlRequest;
        self.isInProgress = YES;
        self.progress = 0;
    }
    return self;
}

//! Background method
- (void)main {
    
    //NSLog(@"OPERATION MAIN is canceled = %d", self.isCancelled);
    
    self.connection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:NO];
    
    //! If request = nil or something wrong.
    if (_connection == nil) {
        NSError* error = [[NSError alloc] initWithDomain:@"com.welpy.error" code:NSURLErrorBadURL userInfo:nil];
        @weakify(self);
        dispatch_sync(dispatch_get_main_queue(), ^{
            @strongify(self);

            if (self.failureBlock) {
                self.failureBlock(self, error);
            }
            
            self.isInProgress = NO;
        });
    }
    
    //! Schedule and start connection
    [self.connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.connection start];
    
    //! Run loop for operation thread
    while (self.connection) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
}

#pragma mark -
#pragma mark  NSURLConnectionDelegate

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response{
    self.response = response;
    
    //! Check responce code
    if ((![response respondsToSelector:@selector(statusCode)] || [((NSHTTPURLResponse *)response) statusCode] < 400)) {
        NSInteger expected = response.expectedContentLength > 0 ? (NSInteger)response.expectedContentLength : 0;
        
        self.connectionData = [[NSMutableData alloc] initWithCapacity:(NSUInteger)expected];
        
        //! Reset progress
        @weakify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self);
            self.progress = 0;
            if (self.progressBlock) {
                self.progressBlock(self, 0);
            }
        });
    }
    else {
        //! If error, cancel connection and report error
        
        NSUInteger code = (NSUInteger)[((NSHTTPURLResponse *)response) statusCode];
        NSError*error = [NSError errorWithDomain:NSURLErrorDomain code:code userInfo:nil];
        
        [self.connection cancel];
        
        [self connection:connection didFailWithError:error];
    }
}

-(void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data{
    
    [self.connectionData appendData:data];
    
    float progressValue = ((float) [self.connectionData length] / (float) self.response.expectedContentLength);
    @weakify(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        @strongify(self);
        self.progress = progressValue;
        if (self.progressBlock) {
            self.progressBlock(self, progressValue);
        }
    });
}


-(void)connectionDidFinishLoading:(NSURLConnection*)connection{
    
    UIImage* resImage = nil;
    
    //! Run processing block in background thread.
    if (self.processingBlock) {
        resImage = self.processingBlock(self, self.connectionData);
    }

    @weakify(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        @strongify(self);

        if (self.successBlock) {
            self.successBlock(self, resImage);
        }
        
        self.isInProgress = NO;
    });
    
    self.connection = nil;
    self.connectionData = nil;
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error{
    //NSLog(@"Connection failed");
    @weakify(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        @strongify(self);

        if (self.failureBlock) {
            self.failureBlock(self, error);
        }
        
        self.isInProgress = NO;
    });
    
    self.connection = nil;
}

//! Disable cache
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}


//! Nil all callback blocks and cancel operation
- (void)cancel{
    
    self.successBlock = nil;
    self.failureBlock = nil;
    self.progressBlock = nil;
    self.connection = nil;
    
    self.isInProgress = NO;
    
    if ([[NSThread currentThread] isMainThread]) {
        if (self.cancelBlock) {
            self.cancelBlock(self);
        }
    }else{
        @weakify(self);
        dispatch_sync(dispatch_get_main_queue(), ^{
            @strongify(self);

            if (self.cancelBlock) {
                self.cancelBlock(self);
            }
        });
    }
    
    [super cancel];
}


- (void)setQueuePriority:(NSOperationQueuePriority)queuePriority{
    //NSLog(@"queue prority = %d", queuePriority);
    [super setQueuePriority:queuePriority];
}

@end
