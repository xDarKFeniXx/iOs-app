//
//  UIImage+Common.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Common)

//! Scale method
- (UIImage*)scaledImageWithScaleValue:(CGFloat)scaleValue;

//! Resize image
- (UIImage*)imageResizedToSize:(CGSize)size;

//! Add corner radius to image
- (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius;

//! Crop image to square size
- (UIImage *)imageByCroppingToSquare;

//! Decompress image to fix performance
+ (UIImage *)decodedImageWithImage:(UIImage *)image;

@end
