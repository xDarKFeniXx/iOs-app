//
//  ImageRequestModel.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/10/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageCache, ImageFileCache;

//! Image resize/scale/crop policy
typedef enum {
    ImageSizePolicyOriginalSize,
    ImageSizePolicyResizeToRect,
    ImageSizePolicyScaleAspectFill,
    ImageSizePolicyScaleAspectFit,
    ImageSizePolicyCropSquare
} ImageSizePolicy;

//! Image cache policy
typedef enum {
    ImageCachePolicyIgnoreCache,
    ImageCachePolicyMemoryCache,
    ImageCachePolicyMemoryAndFileCache
} ImageCachePolicy;


@interface ImageRequestModel : NSObject

//! Result image
@property (strong, nonatomic) UIImage* image;

//! URL string
@property (strong, nonatomic) NSString* urlString;

//! Placeholder image
@property (strong, nonatomic) UIImage* placeholderImage;

//! Cancel image
@property (strong, nonatomic) UIImage* cancelImage;

//! Filure image
@property (strong, nonatomic) UIImage* failureImage;

//! Memory cache
@property (strong, nonatomic) ImageCache* memoryCache;

//! File cache
@property (strong, nonatomic) ImageFileCache* fileCache;

//! Corner radius for image
@property (assign, nonatomic) CGFloat cornerRadius;

//! Is rounded
@property (assign, nonatomic) BOOL isRounded;

//! For size
@property (assign, nonatomic) CGSize scaleToSize;

//! Image resize policy
@property (assign, nonatomic) ImageSizePolicy imageSizePolicy;

//! Image cache policy
@property (assign, nonatomic) ImageCachePolicy imageCachePolicy;

//////////////////////// Additional //////////////////////

//! Cache original and resized size
@property (assign, nonatomic) BOOL cacheOriginalImage; //! Not implemented

//! Request time out
@property (assign, nonatomic) NSTimeInterval timeOutInterval;

//! Loading priority
@property (assign, nonatomic) NSOperationQueuePriority queuePriority;

//! Load operation group key
@property (strong, nonatomic) NSString* key;

//! Parameters hash
@property (strong, nonatomic, readonly) NSString* md5Hash;

@property (copy, nonatomic) void (^progressBlock)(ImageRequestModel* object, CGFloat progress);
@property (copy, nonatomic) void (^successBlock)(ImageRequestModel* object, UIImage* image);
@property (copy, nonatomic) void (^failureBlock)(ImageRequestModel* object, NSError* error);

@property (copy, nonatomic) void (^placeholderBlock)(ImageRequestModel* object, UIImage* image);
@property (copy, nonatomic) void (^startedLoadingBlock)(ImageRequestModel* object, UIImage* image);
@property (copy, nonatomic) void (^canceledLoadingBlock)(ImageRequestModel* object, UIImage* image);


- (void)start;
- (NSString*)md5Hash;

- (NSString*)getMD5Hash:(NSString*)url;

@end
