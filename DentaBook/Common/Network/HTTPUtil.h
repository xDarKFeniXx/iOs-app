//
//  HTTPUtil.h
//  Dict
//
//  Created by Aleksandr Hruschev on 06.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "SharedErrorMessage.h"

@class Task;
@class Reminder;
@class Order;

@interface HTTPUtil : NSObject

enum operationType{opIdle, opStartTrans,opCommitTrans, opPutGroups, opPutWords, opGetGroups, opGetWords, opRegister, opError, opProcessed};

NSString* GUIDString();
int makeRequest(NSString *requesrURLString, CFStringRef requestMessage, int command);
void ParseResult(
                    CFReadStreamRef stream,
                    CFStreamEventType eventType,
                    void *clientCallBackInfo
                 );

void setLoginPassword(NSString* loginStr, NSString* passwordStr);
NSString* getOffertaAccepted();
void setOffertaAccepted();
//
UIColor* getColorScheme();
UIColor* getColorSchemeTooth();
void setColorScheme(int scheme);
int getColorSchemeInt();
UIColor* getColorSchemeBar();
NSString* getColorSchemeImg();
UIColor* getColorSchemeStatus();
UIColor* getColorSchemeXZ();
//
NSString* getLogin();
NSString* getPwd();
NSString* getURL(NSString*);
NSString* getExamURL();
void setAppSettings(NSString* loginStr, NSString* passwordStr, NSString *URLString, NSString *ExamURLString);

void clearCookies();

int getRowsCount();
void setRowsCount(int scheme);
BOOL getNeedName();
void setNeedName(BOOL scheme);
BOOL getNeedSumSplit();
void setNeedSumSplit(BOOL scheme);

void setProfile(NSString* fio, NSString* position, NSString* clinic, NSString* phone, NSString* email, NSString* address,  NSString* license,  NSString* photo_id);

NSString* getProfileFio();
NSString* getProfilePosition();
NSString* getProfileClinic();
NSString* getProfilePhone();
NSString* getProfileEmail();
NSString* getProfileAddress();
NSString* getProfileLicense();
NSString* getProfilePhoto();
@end
