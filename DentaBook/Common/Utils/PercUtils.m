//
//  PercUtils.m
//  Remindme
//
//  Created by Instream on 12.03.13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "PercUtils.h"
#import "DataBase.h"
#define LastDateKey @"last.date"
#define EnteredLocationKey @"entered.location"
#define MonitoredReminderFile @"monitored.reminder"
#define DeviceTokenFile @"device.token"
#define TipsFile @"tip.topics"
#define OffertaKey @"offerta.read"

@implementation PercUtils

+(UIColor*) _yellow
{
    return [UIColor colorWithRed:138.0f/138.0f green:126.0f/138.0f blue:23.0f/138.0f alpha:1.0f];
}

+(UIColor*) _green
{
    return [UIColor colorWithRed:53.0f/138.0f green:126.0f/138.0f blue:23.0f/138.0f alpha:1.0f];
}

//path to device_token
+ (NSString*) tokenFilePath
{
    NSArray* pats=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory=[pats objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:DeviceTokenFile];
}

+ (NSString*) tipsFilePath
{
    NSArray* pats=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory=[pats objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:TipsFile];
}

//path to device_token
+ (NSString*) getDeviceToken
{
    NSData* tmp_token=[[NSData alloc] initWithContentsOfFile:[PercUtils tokenFilePath]];
    return [[[[[NSString alloc] initWithFormat:@"%@", tmp_token] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
}

//path to last location update date
+ (NSString*) locationDateFilePath
{
    NSArray* pats=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory=[pats objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:LastDateKey];
}

//path to last location update date
+ (NSString*) offertaFilePath
{
    NSArray* pats=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory=[pats objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:OffertaKey];
}

// path to current entered locations
/*+ (NSString*) locationEnteredFilePath
{
    NSArray* pats=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDirectory=[pats objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:EnteredLocationKey];
}*/
////////////////////////////////////////////////////////////////////////////
////////////////////////// MONITORING PERSISTENCE METHODS //////////////////
// add reminder to monitored reminders list (high precision on)
+ (void) addMonitoredReminder:(NSString*) _rem_id
{
    NSLog(@"addMonitoredReminder: %@", _rem_id);
    [DataBase DBLog:@"addMonitoredReminder: " withData:_rem_id];
    /*NSMutableArray* monitored_reminders=[[NSMutableArray alloc] initWithContentsOfFile:[PercUtils dataFilePath]];
    
    if(monitored_reminders==nil)
        monitored_reminders=[[NSMutableArray alloc] init];
    
    if(monitored_reminders.count==0) //this is switch from simple to advanced
    {
        [PercUtils setLastLocationDate];//register last location date
    }
    
    [monitored_reminders addObject:_rem_id];
    [monitored_reminders writeToFile:[PercUtils dataFilePath] atomically:YES];*/
    NSArray* monitored_reminders=[DataBase getMonitoredReminders];
    if(monitored_reminders.count==0) //this is switch from simple to advanced
    {
        [DataBase DBLog:@"addMonitoredReminder: start monitoring & register last location date"];
        [PercUtils setLastLocationDate];//register last location date
    }
    [DataBase registerMonitoredReminder:_rem_id];
}
// remove reminder from monitored reminders list 
+ (BOOL) removeMonitoredReminder:(NSString*) _rem_id
{
    NSLog(@"Remove from monitored reminders: %@", _rem_id);
    [DataBase DBLog:@"removeMonitoredReminder: " withData:_rem_id];
    /*NSMutableArray* monitored_reminders=[[NSMutableArray alloc] initWithContentsOfFile:[PercUtils dataFilePath]];
    [monitored_reminders removeObject:_rem_id];
    [monitored_reminders writeToFile:[PercUtils dataFilePath] atomically:YES];*/
    [DataBase unregisterMonitoredReminder:_rem_id];
    NSArray* monitored_reminders=[DataBase getMonitoredReminders];
    
    return (monitored_reminders==nil)||(monitored_reminders.count==0);
}
// mark reminder as enetered but not exited 
+ (void) addEnteredReminderLocation:(NSString*) _rem_id
{
    NSLog(@"Add entered location: %@", _rem_id);
    [DataBase DBLog:@"addEnteredReminderLocation: " withData:_rem_id];
    /*NSMutableArray* monitored_reminders=[[NSMutableArray alloc] initWithContentsOfFile:[PercUtils locationEnteredFilePath]];
    
    if(monitored_reminders==nil)
        monitored_reminders=[[NSMutableArray alloc] init];
    
    [monitored_reminders addObject:_rem_id];
    [monitored_reminders writeToFile:[PercUtils locationEnteredFilePath] atomically:YES];*/
    [DataBase registerEnteredLocation:_rem_id];
}
// unmark reminder as enetered
+ (BOOL) removeEnteredReminderLocation:(NSString*) _rem_id
{
    NSLog(@"Remove from entered locations: %@", _rem_id);
    /*NSMutableArray* monitored_reminders=[[NSMutableArray alloc] initWithContentsOfFile:[PercUtils locationEnteredFilePath]];
    [monitored_reminders removeObject:_rem_id];
    [monitored_reminders writeToFile:[PercUtils locationEnteredFilePath] atomically:YES];*/
    [DataBase unregisterEnteredLocation:_rem_id];
    NSArray* entered_locations=[DataBase getEnteredLocations];
    
    return (entered_locations==nil)||(entered_locations.count==0);
}
//check reminder in entered locations
+ (BOOL) checkReminderInEnteredLocations:(NSString*) rem_id
{
    NSArray* entered_reminders=[DataBase getEnteredLocations];
    for(NSString* entered_rem_id in entered_reminders)
    {
        if([entered_rem_id isEqualToString:rem_id])
        {
            return true;
        }
    }
    return false;
}

//set last location update date
+ (void) setLastLocationDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];   
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    [stringFromDate writeToFile:[PercUtils locationDateFilePath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
}
//get last location update date
+ (NSDate*) getLastLocationDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"]; 
    return [formatter dateFromString:[[NSString alloc] initWithContentsOfFile:[PercUtils locationDateFilePath] encoding:NSUTF8StringEncoding error:nil]]; 
}

+ (int) getLastLocationInterval
{
    NSLog(@"getLastLocationInterval: last_location date=%@, current=%@",[PercUtils getLastLocationDate], [NSDate date]);
    return [[NSDate date] timeIntervalSinceDate:[PercUtils getLastLocationDate]];
}

//clear all of supplementary data
+ (void) clearSupplementaryData
{
    //clear monitored reminders
    NSError* err;
    
    // check for error
    if(![DataBase clearMonitoredReminders])
    {
        NSLog(@"didUpdateToLocation: error clear monitored reminders");
        err=nil;
    }
    
    [[NSFileManager defaultManager] removeItemAtPath:[PercUtils locationDateFilePath] error:&err];
    // check for error
    if(err!=nil)
    {
        NSLog(@"didUpdateToLocation: error delete file: %@",err);
        err=nil;
    }
    //
    
    // check for error
    if(![DataBase clearEnteredLocations])
    {
        NSLog(@"didUpdateToLocation: error clear entered locations");
        err=nil;
    }
    //
}

+ (NSString*) getLocationProbability:(float) radius
{
    float true_possibility=0;
    
    if(radius>=0&&radius<=500)
        true_possibility=33.0f/500.0f*radius;
    else if(radius>=500&&radius<=1000)
         true_possibility=50.0f/1000.0f*radius;
    else if(radius>=1000&&radius<=1500)
         true_possibility=60.0f/1500.0f*radius;
    else if(radius>=1500&&radius<=2000)
         true_possibility=70.0f/2000.0f*radius;
    else if(radius>=2000&&radius<=2500)
        true_possibility=75.0f/2500.0f*radius;
    else if(radius>=2500&&radius<=3000)
        true_possibility=80.0f/3000.0f*radius;
    else if(radius>=3000&&radius<=3500)
        true_possibility=82.5f/3500.0f*radius;
    else if(radius>=3500&&radius<=4000)
        true_possibility=85.0f/4000.0f*radius;
    else if(radius>=4000&&radius<=4500)
        true_possibility=87.5f/4500.0f*radius;
    else if(radius>=4500&&radius<=5000)
        true_possibility=90.0f/5000.0f*radius;
    else
        true_possibility=100;
    if(true_possibility==100)
        return @">90%";
    else
        return [[NSString alloc] initWithFormat:@"%d%%",(int)true_possibility];
}

+ (UIImage *)imageWithColor:(UIColor *)color 
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 320.0f, 35.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSString *)getRootDomain:(NSString *)domain
{
    // Return nil if none found.
    NSString * rootDomain = nil;
    
    // Convert the string to an NSURL to take advantage of NSURL's parsing abilities.
    NSURL * url = [NSURL URLWithString:domain];
    
    // Get the host, e.g. "secure.twitter.com"
    NSString * host = [url host];
    
    // Separate the host into its constituent components, e.g. [@"secure", @"twitter", @"com"]
    NSArray * hostComponents = [host componentsSeparatedByString:@"."];
    if (hostComponents.count >=2)
    {
        // Create a string out of the last two components in the host name, e.g. @"twitter" and @"com"
        rootDomain = [NSString stringWithFormat:@"%@.%@", [hostComponents objectAtIndex:(hostComponents.count - 2)], [hostComponents objectAtIndex:(hostComponents.count - 1)]];
    }
    
    return rootDomain;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


NSString* nvl(NSString* input,NSString* placeholder)
{
    
    if([input isKindOfClass:[NSNull class]])
        return placeholder;
    else
    {
        if([input isKindOfClass:[NSNumber class]])
            return input;
        else if([input isKindOfClass:[NSString class]])
        {
            if(input.length==0)
            {
                return placeholder;
            }
            else
            {
                return input;
            }
        }
        else return placeholder;
    }
}


NSString* nvl2(NSString* input,NSString* concat,NSString* placeholder)
{
    if([input isKindOfClass:[NSNull class]])
        return placeholder;
    else
    {
        if([input isKindOfClass:[NSNumber class]])
            return [NSString stringWithFormat:@"%@%@",input,concat];
        else if([input isKindOfClass:[NSString class]])
        {
            if(input.length==0)
            {
                return placeholder;
            }
            else
            {
                return [NSString stringWithFormat:@"%@%@",input,concat];
            }
        }
        else return placeholder;
    }
}

NSString* nvl3(NSString* input,NSString* concat,NSString* placeholder)
{
    if([input isKindOfClass:[NSNull class]])
        return placeholder;
    else
    {
        if([input isKindOfClass:[NSNumber class]])
            return [NSString stringWithFormat:@"%@%@",concat,input];
        else if([input isKindOfClass:[NSString class]])
        {
            if(input.length==0)
            {
                return placeholder;
            }
            else
            {
                return [NSString stringWithFormat:@"%@%@",concat,input];
            }
        }
        else return placeholder;
    }
}

NSString* substr(NSString* input, int len)
{
    if(input.length>len)
    {
        return [NSString stringWithFormat:@"%@...",[input substringToIndex:len-3]];
    }
    else return input;
}

BOOL NSStringIsValidEmail(NSString *checkString)
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

NSDate* roundTo(NSDate* input, int minutes)
{
    float seconds = (float)minutes*60.0f;
    NSTimeInterval segz = round([input timeIntervalSinceReferenceDate]/seconds)*seconds;
    return [NSDate dateWithTimeIntervalSinceReferenceDate:segz];
}


@end
