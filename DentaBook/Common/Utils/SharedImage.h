//
//  SharedImage.h
//  DentaBook
//
//  Created by Mac Mini on 30.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedImage : NSObject
{
    UIImage* img;
}
+(SharedImage*) sharedInstance;

// clue for improper use (produces compile time error)
+(SharedImage*) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(SharedImage*) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(SharedImage*) new __attribute__((unavailable("new not available, call sharedInstance instead")));
- (NSString*) description;
@property (nonatomic, retain) UIImage* img;

@end
