//
//  UIColors+Smart.h
//  OpenTaxiPassenger
//
//  Created by Instream on 26.09.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//



@interface UIColor (Smart)
+(UIColor*) _yellow;
+(UIColor*) _green;
@end
