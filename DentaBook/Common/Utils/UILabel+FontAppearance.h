//
//  UILabel+FontAppearance.h
//  DentaBook
//
//  Created by Mac Mini on 26.01.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FontAppearance)
@property (nonatomic, copy) UIFont * appearanceFont UI_APPEARANCE_SELECTOR;
@end
