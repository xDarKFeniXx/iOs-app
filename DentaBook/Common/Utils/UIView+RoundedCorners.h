//
//  UIView+RoundedCorners.h
//  OpenTaxiPassenger
//
//  Created by Instream on 26.09.14.
//  Copyright (c) 2014 Instream. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundedCorners)
- (void) makeRoundCorners;
@end
