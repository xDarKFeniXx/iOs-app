//
//  AffairAddGroupViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AffairAddGroupViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "AffairGroup.h"
#import "Affair.h"
#import "AffairManager.h"
#import "DataBase.h"
#import "HttpUtil.h"

@interface AffairAddGroupViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@end

@implementation AffairAddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Добавить группу";
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0)
        return;
    
    NSMutableDictionary *affairGroup = [[NSMutableDictionary alloc] init];//[AffairGroup MR_createEntity];
    affairGroup[@"title"] = self.categoryTitleLabel.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    affairGroup[@"createat"] = [dateFormatter stringFromDate:[NSDate date]];
    affairGroup[@"id"] = GUIDString();
    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Сохранение";
    [HUD showInView:self.view];
    [[AffairManager shared] createAffair:affairGroup success:^{
        [HUD dismissAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^{
        [HUD dismissAnimated:YES];
    }];
}


@end
