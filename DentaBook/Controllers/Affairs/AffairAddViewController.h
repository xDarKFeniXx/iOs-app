//
//  AffairAddViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBActionSheet.h"
#import "UIImageView+ImageDownloader.h"
#import "PhotoEditViewController.h"

@class AffairGroup;

@interface AffairAddViewController : UIViewController<UITextViewDelegate, UIImagePickerControllerDelegate>

-(id) initWithAffairGroup:(NSDictionary*)affairGroup;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholder;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) IBActionSheet *photoActionSheet;
@property (nonatomic, strong) NSMutableDictionary *currentAffair;
- (IBAction)onAddPhoto:(id)sender;
-(id) initWithAffair:(NSDictionary*)affair;

@end
