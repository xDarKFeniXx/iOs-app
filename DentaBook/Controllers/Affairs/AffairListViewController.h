//
//  AffairListViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AffairGroup;

@interface AffairListViewController : UIViewController

-(id) initWithAffairGroup:(NSDictionary*)affairGroup;
@property (nonatomic,retain) NSMutableArray* affairs_done;
@property (nonatomic,retain) NSMutableArray* affairs_not_done;
@property (nonatomic) BOOL onlyActual;

-(id) initWithTodayGroup;
-(id) initWithWeekGroup;

@end
