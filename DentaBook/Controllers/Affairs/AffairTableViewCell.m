//
//  AffairTableViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 21.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AffairTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <JTSImageViewController/JTSImageViewController.h>

@implementation AffairTableViewCell
@synthesize shadowView, frontView, imgToZoom, owner;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) layoutSubviews
{
    [super layoutSubviews];
    /*shadowView.layer.cornerRadius=3;
    shadowView.layer.masksToBounds=YES;
    frontView.layer.cornerRadius=3;
    frontView.layer.masksToBounds=YES;*/
    frontView.layer.shadowColor = [UIColor blackColor].CGColor;
    frontView.layer.shadowOpacity = 0.5;
    frontView.layer.shadowRadius = 2;
    frontView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    frontView.layer.cornerRadius = 2;
    frontView.layer.masksToBounds = NO;
}

- (IBAction)onCheck:(UIButton*)sender {
    
    sender.selected = !sender.selected;
    
    if(self.didCheckAffair)  {
        self.didCheckAffair(self.indexPath, sender.selected);
    }
}

- (IBAction)onCheckHolder:(id)sender {
    [self.checkButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)tapImg:(id)sender
{
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = imgToZoom.image;
    imageInfo.referenceRect = self.imageView.frame;
    imageInfo.referenceView = self.imageView.superview;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode: JTSImageViewControllerMode_Image
                                           backgroundStyle: JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:owner transition:JTSImageViewControllerTransition_FromOriginalPosition];
}


@end
