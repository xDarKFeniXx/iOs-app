//
//  AffairsCategoryTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 22.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AffairsCategoryTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation AffairsCategoryTableViewCell
@synthesize shadowView, frontView, imgView, txtLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    shadowView.layer.cornerRadius=3;
    shadowView.layer.masksToBounds=YES;
    frontView.layer.cornerRadius=2;
    frontView.layer.masksToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
