//
//  AffairsListViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFlatSearchBar.h"
@interface AffairsListViewController : UIViewController<UISearchBarDelegate>
@property (nonatomic,retain) NSMutableArray* affairs;
@property (weak, nonatomic) IBOutlet UIFlatSearchBar *searchBar;
@property (nonatomic, strong) NSString *searchString;
@property (weak, nonatomic) IBOutlet UITextField *fakeField;
@property (weak, nonatomic) IBOutlet UIImageView *magnifyerImg;
@property (weak, nonatomic) IBOutlet UILabel *searchPlaceholder;
@end
