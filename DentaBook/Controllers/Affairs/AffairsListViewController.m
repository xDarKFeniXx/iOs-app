//
//  AffairsListViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "AffairsListViewController.h"
#import "AffairAddGroupViewController.h"
#import "AffairListViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "AffairGroup.h"
#import "Affair.h"
#import "AffairManager.h"
#import "DataBase.h"
#import "AffairsCategoryTableViewCell.h"
#import "CZPicker.h"
#import "HttpUtil.h"
#import "PercUtils.h"

@interface AffairsListViewController () <NSFetchedResultsControllerDelegate, CZPickerViewDelegate, CZPickerViewDataSource, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation AffairsListViewController
@synthesize affairs, searchBar, searchString, fakeField, magnifyerImg, searchPlaceholder;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Дела";
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AffairsCategoryTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AffairsCategoryTableViewCell class])];
    
    [self fetchTableData];
    
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    for (UIView *subview in [[self.searchBar.subviews lastObject] subviews]) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
    searchBar.delegate = self;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
   
    searchBar.layer.cornerRadius=10.0f;
    searchBar.layer.masksToBounds=YES;
    searchBar.layer.borderColor=[[UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1] CGColor];
    searchBar.layer.borderWidth= 1.0f;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    /*NSString* less5=@"Поиск                                                                            ";
     NSString* six=@"Поиск                                                                                                           ";
     NSString* sixplus=@"Поиск                                                                                                                               ";
     NSLog(@"self.bounds: %f", self.view.bounds.size.width);
     self.searchBarView.placeholder = self.view.bounds.size.width<=320?less5:(self.view.bounds.size.width<=375?six:sixplus);*/
    self.searchBar.placeholder = @"";
    
    searchBar.delegate = self;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    //magnifyerImg.hidden = YES;
    searchPlaceholder.hidden = YES;
    
    return YES;
}



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchString = searchText;
    
    [self fetchTableData];
    [self.tableView reloadData];
    
    if(self.searchString.length == 0)   {
        [self.searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
        [self.view endEditing:YES];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar   {
    if(self.searchString.length > 0)    {
        [self fetchTableData];
        [self.tableView reloadData];
    }
    [self.view endEditing:YES];
}


- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onAdd:(id)sender
{
    AffairAddGroupViewController *addGroupVC = [AffairAddGroupViewController new];
    [self.navigationController pushViewController:addGroupVC animated:YES];
}

-(void)fetchTableData
{
    
    NSArray* tmp = [DataBase getDBArray:@"affair_group" withKey:@"" andValue:@""];
    if(searchString.length==0)
        affairs = [NSMutableArray arrayWithArray:tmp];
    else
    {
        affairs=[[NSMutableArray alloc] init];
        
        for(NSDictionary *d in tmp)
        {
            if([[NSString stringWithFormat:@"%@",d[@"title"]] rangeOfString:self.searchString options:NSCaseInsensitiveSearch].location!=NSNotFound)
            {
                [affairs addObject:d];
            }
        }
    }
    
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return affairs.count+2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    
    //SWTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    AffairsCategoryTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AffairsCategoryTableViewCell class])];
    if(indexPath.row==0)
    {
        titleCell.imgView.image = [UIImage imageNamed:@"today"];
        titleCell.txtLabel.text = @"Дела на сегодня";
        
        
        titleCell.txtLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.txtLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
        titleCell.detLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.detLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    }
    else if(indexPath.row==1)
    {
        titleCell.imgView.image = [UIImage imageNamed:@"toweek-affair"];
        titleCell.txtLabel.text = @"Дела на неделю";
        
        
        titleCell.txtLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.txtLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.detLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    }
    else
    {
        NSDictionary *affairGroup = affairs[indexPath.row-2];//[self.fetchedResultsController objectAtIndexPath:indexPath];
        titleCell.imgView.image = [PercUtils imageWithImage:[UIImage imageNamed:@"affair-list-icon"] scaledToSize:CGSizeMake(25, 20)];
;
        titleCell.txtLabel.text = affairGroup[@"title"];
        
        NSArray *nonDoneAffairArray = [DataBase getDBArray:@"affair" withKey:@"group_id" andValue:affairGroup[@"id"]];
        int not_done_count = 0;
        for(NSDictionary* d in nonDoneAffairArray)
        {
            if(![d[@"isdone"] isEqualToString:@"yes"])
            {
                not_done_count++;
            }
        }
        
        titleCell.detLabel.text = [NSString stringWithFormat:@"%@",@(not_done_count).stringValue];
        
        titleCell.txtLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.txtLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        titleCell.detLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    }
    
    
    
    cell = titleCell;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[UIImage imageNamed:@"delete_white.png"]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.tag = [self.tableView indexPathForCell:cell].row-2;
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
            
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row>=2)
    {
        NSDictionary *affairGroup = affairs[indexPath.row-2];
        
        AffairListViewController *listVC = [[AffairListViewController alloc] initWithAffairGroup:affairGroup];
        [self.navigationController pushViewController:listVC animated:YES];
    }
    else
    {
        if(indexPath.row==0)
        {
            AffairListViewController *listVC = [[AffairListViewController alloc] initWithTodayGroup];
            [self.navigationController pushViewController:listVC animated:YES];
        }
        else
        {
            AffairListViewController *listVC = [[AffairListViewController alloc] initWithWeekGroup];
            [self.navigationController pushViewController:listVC animated:YES];
        }
    }
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
     return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *affairGroup = affairs[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[AffairManager shared] deleteAffairById:affairGroup[@"id"] success:^{
        //
    } failure:^{
        //
    }];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    
    NSDictionary *affairGroup = affairs[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[AffairManager shared] deleteAffairById:affairGroup[@"id"] success:^{
        //
    } failure:^{
        //
    }];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

@end
