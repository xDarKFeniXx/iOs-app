//
//  CalendarViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 09.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *mondayLabel;
@property (weak, nonatomic) IBOutlet UILabel *tuesdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *wednesdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *thursdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *fridayLabel;
@property (weak, nonatomic) IBOutlet UILabel *saturdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *sundayLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLayoutConstraint1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLayoutConstraint2;
-(IBAction)onAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *todayBtn;
- (IBAction)tapToday:(id)sender;
-(id) initWithPatient:(NSDictionary*)patient;
@end
