//
//  EventNameViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 10.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPAutoCompleteTextField.h"

@interface EventNameViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet MLPAutoCompleteTextField *textField;
@property (strong, nonatomic) id delegate;
@end
