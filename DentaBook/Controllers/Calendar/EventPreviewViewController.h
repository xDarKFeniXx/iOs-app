//
//  EventPreviewViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 10.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@interface EventPreviewViewController : UIViewController

-(id) initWithEvent:(NSDictionary*)event;

@end
