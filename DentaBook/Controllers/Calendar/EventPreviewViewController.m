//
//  EventPreviewViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 10.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "EventPreviewViewController.h"
#import "NewEventViewController.h"
#import "CommentViewCell.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Event.h"
#import "DataBase.h"
#import "HttpUtil.h"

@interface EventPreviewViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSDictionary *currentEvent;
@end

@implementation EventPreviewViewController

-(id) initWithEvent:(NSDictionary*)event   {
    self = [super initWithNibName:@"EventPreviewViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentEvent = event;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CommentViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CommentViewCell class])];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    [self.tableView reloadData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    if(indexPath.row == 5)  {
        /*CGFloat h = [CommentViewCell heightOfCellInText:[NSString stringWithFormat:@"%@", ([self.currentEvent[@"commentt"] length] == 0) ? @"Нет комментария" : [NSString stringWithFormat:@"Комментарий:\n%@", self.currentEvent[@"commentt"]]]
                                              cellWidth:CGRectGetWidth(self.tableView.bounds)-21.0];
        return h + 16 + 27;*/
        
        return 80;
    }
    
    if(indexPath.row == 7)
    {
        return 1;
    }
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell2"];
        titleCell.backgroundColor = [UIColor clearColor];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        titleCell.detailTextLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    }
    
    UIView *label = [titleCell.contentView viewWithTag:1];
    UIButton *btn1 = [titleCell.contentView viewWithTag:2];
    UIButton *btn2 = [titleCell.contentView viewWithTag:3];
    [label removeFromSuperview];
    [btn1 removeFromSuperview];
    [btn2 removeFromSuperview];
    
    switch (indexPath.row) {
        case 0: {
            titleCell.backgroundColor = [UIColor whiteColor];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd MMMM yyyy"];
            NSString *strDate = self.currentEvent[@"startdate"];//[dateFormatter stringFromDate:self.currentEvent.startDate];
            
            UILabel *_mainLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            _mainLabel.tag = 1;
            _mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
            _mainLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            _mainLabel.textAlignment = NSTextAlignmentCenter;
            _mainLabel.text = strDate;
            [titleCell.contentView addSubview:_mainLabel];
            [_mainLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(0);
                make.centerY.equalTo(titleCell.contentView.centerY);
                make.width.equalTo(titleCell.contentView.width);
                make.height.equalTo(14);
            }];
        }
            break;
            
        case 1: {
            titleCell.backgroundColor = [UIColor whiteColor];
            titleCell.textLabel.text = self.currentEvent[@"title" ];
            titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        }
            break;
            
        case 2: {
            titleCell.backgroundColor = [UIColor whiteColor];
            titleCell.textLabel.text = @"Начало приёма:";
            
            
            //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            //[dateFormatter setDateFormat:@"HH:mm"];
            NSString *strDate = [NSString stringWithFormat:@"%@:%@", self.currentEvent[@"starthours"], self.currentEvent[@"startminutes"]];
            
            titleCell.detailTextLabel.text = strDate;
            titleCell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        }
            break;
            
        case 3: {
            titleCell.backgroundColor = [UIColor whiteColor];
            titleCell.textLabel.text = @"Конец приёма:";
            
            /*NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm"];
            NSString *strDate = [dateFormatter stringFromDate:self.currentEvent.endDate];*/
            NSString *strDate = [NSString stringWithFormat:@"%@:%@", self.currentEvent[@"endhours"], self.currentEvent[@"endminutes"]];
            
            titleCell.detailTextLabel.text = strDate;
            titleCell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        }
            break;
            
        case 4: {
            titleCell.backgroundColor = [UIColor whiteColor];
            titleCell.textLabel.text = @"Напоминание:";
            titleCell.detailTextLabel.text = @"Нет";
            titleCell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            titleCell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        }
            break;
            
        case 5: {
            CommentViewCell *commentCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentViewCell class])];
            commentCell.backgroundColor = [UIColor whiteColor];
            [commentCell updateCell:[NSString stringWithFormat:@"%@", ([self.currentEvent[@"commentt"] length] == 0) ? @"Нет комментария" : [NSString stringWithFormat:@"Комментарий:\n%@", self.currentEvent[@"commentt"]]]];
            titleCell = commentCell;
        }
            break;
            
        case 6: {
            titleCell.backgroundColor = [UIColor whiteColor];
            titleCell.textLabel.text = @"";
            titleCell.detailTextLabel.text = @"";
            
            UIView *seperateView = [[UIView alloc] initWithFrame:CGRectZero];
            seperateView.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0 alpha:1.0];
            [titleCell.contentView addSubview:seperateView];
            [seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(0);
                make.left.equalTo(titleCell.contentView.centerX);
                make.size.equalTo(CGSizeMake(1, 50));
            }];
            
            UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.tag = 2;
            cancelButton.frame = CGRectMake(0, 0, 86.0, 22.0f);
            [cancelButton setImage:[UIImage imageNamed:@"event-cancel-icon"] forState:UIControlStateNormal];
            [cancelButton setTitle:@"Отменить" forState:UIControlStateNormal];
            [cancelButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
            [cancelButton setTitleColor:[UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            cancelButton.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 9.0f, 0.0f, 0.0f);
            [cancelButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [cancelButton addTarget:self action:@selector(onCancelEvent:) forControlEvents:UIControlEventTouchUpInside];
            [titleCell.contentView addSubview:cancelButton];
            [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(titleCell.contentView.centerY);
                make.centerX.equalTo(titleCell.contentView.centerX).with.dividedBy(2);
                make.size.equalTo(CGSizeMake(86.0f, 22.0f));
            }];
            
            UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            deleteButton.tag = 3;
            deleteButton.frame = CGRectMake(0, 0, 86.0, 22.0f);
            [deleteButton setImage:[UIImage imageNamed:@"event-delete-icon"] forState:UIControlStateNormal];
            [deleteButton setTitle:@"Удалить" forState:UIControlStateNormal];
            [deleteButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
            [deleteButton setTitleColor:[UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            deleteButton.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 9.0f, 0.0f, 0.0f);
            [deleteButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [deleteButton addTarget:self action:@selector(onDeleteEvent:) forControlEvents:UIControlEventTouchUpInside];
            [titleCell.contentView addSubview:deleteButton];
            [deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(titleCell.contentView.centerY);
                make.left.equalTo(cancelButton.right).with.offset(80);
                make.size.equalTo(CGSizeMake(86.0f, 22.0f));
            }];
        }
            break;
            
        default:
            break;
    }
    
    cell = titleCell;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void) onCancelEvent:(id)button    {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) onDeleteEvent:(id)button    {
    [UIAlertView showWithTitle:@"" message:@"Вы уверены что хотите удалить событие?"
             cancelButtonTitle:@"Отмена"
             otherButtonTitles:@[@"Да"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)   {
                 if(buttonIndex == 1)
                 {
                     //[self.currentEvent MR_deleteEntity];
                     //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                     [DataBase killDB:@"events" withKey:@"id" andValue:self.currentEvent[@"id"]];
                     
                     [self.navigationController popViewControllerAnimated:YES];
                 }
             }];
}

@end
