//
//  NewEventViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 10.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "NewEventViewController.h"
#import "JJTopMenuViewController.h"
#import "EventNameViewCell.h"
#import "WLPActionSheet.h"
#import "DBFulldateTimeController.h"
#import "DBTimeDateController.h"
#import "AllDayViewCell.h"
#import "CommentViewCell.h"
#import "UIView+Helpers.h"

#import <MagicalRecord/MagicalRecord.h>
#import "CZPicker.h"

#import "Event.h"
#import "DataBase.h"
#import "MLPAutoCompleteTextField.h"
#import "PatientAutocompleteObj.h"
#import <QuartzCore/QuartzCore.h>
#import "HttpUtil.h"
#import "UIView+Toast.h"
#import <EventKit/EventKit.h>

@interface NewEventViewController () <UITextFieldDelegate, CZPickerViewDataSource, CZPickerViewDelegate>
@property (nonatomic) CalendarEventMode eventMode;
//@property (nonatomic, strong) JJTopMenuViewController *menuVC;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) WLPActionSheet *fullDateTimeActionSheet;
@property (nonatomic, strong) DBFulldateTimeController *fullDateTimeController;

@property (nonatomic, strong) WLPActionSheet *timeDateActionSheet;
@property (nonatomic, strong) DBTimeDateController *timeDateController;

@property (nonatomic, strong) NSArray *reminderNameArray;

@property (nonatomic, strong) UITextView *commonCommentTextView;

@property (nonatomic, retain) NSDictionary* currentPatient;
@property (nonatomic, retain) NSDictionary* currentEvent;

@end

@implementation NewEventViewController  {
    NSDate *startDate, *endDate;
    NSString *titleString;
    NSInteger selectedReminderRow;
    BOOL isAllDay;
    NSString *commentString;
    NSString* placeString;
}
@synthesize menuSelector,patLabel, dealActiveBtn, patientInactiveBtn, dealInactiveBtn, patientActiveBtn, delimeterView;
-(id) initWithPatient:(NSDictionary*)patient
{
    self = [super initWithNibName:@"NewEventViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.eventMode = kCalendarPatientMode;
    }
    return self;
}

-(id) initWithEvent:(NSDictionary*)event andPatient:(NSDictionary*)patient
{
    self = [super initWithNibName:@"NewEventViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentEvent = event;
        //self.eventMode = kCalendarPatientMode;
        self.currentPatient = patient;
        self.eventMode = kCalendarPatientMode;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        startDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@:%@",event[@"startdate"],event[@"starthours"],event[@"startminutes"]] ];
        endDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@:%@",event[@"enddate"],event[@"endhours"],event[@"endminutes"]] ];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!self.currentEvent)
        [self clearAllData];
    
    dealActiveBtn.layer.cornerRadius = 3.0f;
    dealActiveBtn.layer.masksToBounds = YES;
    
    patientInactiveBtn.layer.cornerRadius = 3.0f;
    patientInactiveBtn.layer.masksToBounds = YES;
    
    dealInactiveBtn.layer.cornerRadius = 3.0f;
    dealInactiveBtn.layer.masksToBounds = YES;
    
    patientActiveBtn.layer.cornerRadius = 3.0f;
    patientActiveBtn.layer.masksToBounds = YES;
    
    if(self.currentPatient)
    {
        self.eventMode = kCalendarPatientMode;

        self.title = @"Календарь";
        self.dealView.hidden=YES;
        self.patientView.hidden=YES;
        self.delimeterView.hidden=YES;
        commentString = @"";
    }
    else
    {
        if(self.currentEvent)
        {
            self.title = @"Календарь";
            
            self.eventMode = kCalendarAffairMode;
            self.dealView.hidden=NO;
            self.patientView.hidden=YES;
            
            commentString=self.currentEvent[@"title"];
        }
        else
        {
            self.title = @"Календарь";
            
            self.eventMode = kCalendarAffairMode;
            self.dealView.hidden=NO;
            self.patientView.hidden=YES;
            commentString = @"";
        }
    }
    
    selectedReminderRow = 0;
    self.reminderNameArray = @[@"Нет", @"В момент события", @"За 5 мин", @"За 15 мин", @"За 30 мин", @"За 1 ч", @"За 2 ч", @"За 1 дн", @"За 2 дн", @"За 1 нед"];
    
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Добавить" forState:UIControlStateNormal];
    [button setTitle:@"Добавить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.commonCommentTextView = [UITextView new];

    self.fullDateTimeController = [DBFulldateTimeController new];
    self.timeDateController = [DBTimeDateController new];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EventNameViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EventNameViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CommentViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CommentViewCell class])];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commentChanged:) name:@"commentChanged" object:nil];
    
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    //self.tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    self.tableView.userInteractionEnabled = YES;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    dealActiveBtn.backgroundColor = getColorSchemeXZ();
    patientActiveBtn.backgroundColor = getColorSchemeXZ();
    delimeterView.backgroundColor = getColorSchemeXZ();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.fullDateTimeController.datePicker.minuteInterval = 15;
    self.timeDateController.datePicker.minuteInterval = 15;
    selectedReminderRow = 4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    if(self.currentPatient)
    {
        self.eventMode = kCalendarPatientMode;
        [self.tableView reloadData];
        patLabel.hidden = NO;
        menuSelector.hidden=YES;
    }
    else
    {
        self.eventMode = kCalendarPatientMode;
        patLabel.hidden = YES;
        menuSelector.hidden=NO;
        self.dealView.hidden=YES;
        self.patientView.hidden=NO;
    }
}

- (void) commentChanged:(NSNotification*) n
{
    commentString=n.object[@"text"];
}

- (void)AddEventToCalendar:(NSDictionary*) eventEntity
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if(granted) {
            // create/edit your event here
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd.MM.yyyy HH:mm"];
            EKEvent *event = [EKEvent eventWithEventStore:eventStore];
            /*eventEntity[@"place"] = placeString;
             = [dateFormat stringFromDate:startDate];
            eventEntity[@"enddate"] = [dateFormat stringFromDate:endDate];
            eventEntity[@"cleardate"] = [dateFormat stringFromDate:clearDate];*/

            // title of the event
            event.title = eventEntity[@"title"];
            
            // star tomorrow
            NSDate* dt =[dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", eventEntity[@"startdate"], eventEntity[@"starthours"], eventEntity[@"startminutes"]]];
            event.startDate =  [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", eventEntity[@"startdate"], eventEntity[@"starthours"], eventEntity[@"startminutes"]]];
            
            // duration = 1 h
            dt=[dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", eventEntity[@"enddate"], eventEntity[@"endhours"], eventEntity[@"endminutes"]]];
            event.endDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", eventEntity[@"enddate"], eventEntity[@"endhours"], eventEntity[@"endminutes"]]];
            
            event.notes = eventEntity[@"commentt"];
            event.location = eventEntity[@"place"];

            
            // set the calendar of the event. - here default calendar
            [event setCalendar:[eventStore defaultCalendarForNewEvents]];
            
            // store the event
            NSError *err;
            [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
            [eventStore commit:&err];
        }
    }];
    
    
}

-(void)onAdd:(id)sender
{
    EventNameViewCell *nameCell = (EventNameViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    titleString = nameCell.textField.text;
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    
    if(self.eventMode == kCalendarAffairMode)
    {
        nameCell = (EventNameViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        placeString = nameCell.textField.text;
        
        if(titleString.length == 0 || startDate == nil)
        {
            [self.view makeToast:@"На задано название/дата"];
            return;
        }
        if((!isAllDay&&(startDate == nil || endDate == nil))||(isAllDay && startDate == nil))
        {
            [self.view makeToast:@"На задана дата"];
            return;
        }
        
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:startDate];
        NSDate *clearDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        
        NSMutableDictionary *eventEntity = [[NSMutableDictionary alloc] init];
        eventEntity[@"type"] = @(self.eventMode);
        eventEntity[@"title"] = titleString;
        eventEntity[@"place"] = placeString;
        eventEntity[@"startdate"] = [dateFormat stringFromDate:startDate];
        eventEntity[@"enddate"] = [dateFormat stringFromDate:endDate];
        eventEntity[@"cleardate"] = [dateFormat stringFromDate:clearDate];
        if(!isAllDay)
        {
            [dateFormat setDateFormat:@"HH"];
            eventEntity[@"starthours"] = [dateFormat stringFromDate:startDate];
            eventEntity[@"endhours"] = [dateFormat stringFromDate:endDate];
            [dateFormat setDateFormat:@"mm"];
            eventEntity[@"startminutes"] = [dateFormat stringFromDate:startDate];
            eventEntity[@"endminutes"] = [dateFormat stringFromDate:endDate];
        }
        else
        {
            eventEntity[@"starthours"] = @"0";
            eventEntity[@"endhours"] = @"23";
            eventEntity[@"startminutes"] = @"0";
            eventEntity[@"endminutes"] = @"59";
        }
        eventEntity[@"isallday"] = @(isAllDay);
        eventEntity[@"commentt"] = commentString;
        eventEntity[@"id"] = GUIDString();
        [DataBase putDB:eventEntity toTable:@"events" withKey:@"id"];
        [self AddEventToCalendar:eventEntity];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if(titleString.length == 0 || startDate == nil)
            return;
        
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:startDate];
        NSDate *clearDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        

        NSMutableDictionary *eventEntity = [[NSMutableDictionary alloc] init];
        eventEntity[@"type"] = @(self.eventMode);
        if(self.currentPatient)
        {
            eventEntity[@"title"] = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
            eventEntity[@"patientid"] = self.currentPatient[@"patientid"];
        }
        else
        {
            eventEntity[@"title"] = titleString;
        }
        
        eventEntity[@"startdate"] = [dateFormat stringFromDate:startDate];
        eventEntity[@"enddate"] = [dateFormat stringFromDate:endDate];
        eventEntity[@"cleardate"] = [dateFormat stringFromDate:clearDate];
        eventEntity[@"isallday"] = @(isAllDay);
        eventEntity[@"commentt"] = commentString;
        
        [dateFormat setDateFormat:@"HH"];
        eventEntity[@"starthours"] = [dateFormat stringFromDate:startDate];
        eventEntity[@"endhours"] = [dateFormat stringFromDate:endDate];
        [dateFormat setDateFormat:@"mm"];
        eventEntity[@"startminutes"] = [dateFormat stringFromDate:startDate];
        eventEntity[@"endminutes"] = [dateFormat stringFromDate:endDate];
        eventEntity[@"id"] = GUIDString();
        
        if([self checkPatientIntersectionWithStart:startDate withEnd:endDate])
        {
            NSLog(@"INTERSECTION");
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Наложение времени приема!" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Продолжить"];
            picker.headerBackgroundColor = getColorScheme();
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = NO;
            picker.tag=2;
            picker.linkedObject = eventEntity;
            picker.allowMultipleSelection = YES;
            [picker show];

        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
            [self AddEventToCalendar:eventEntity];
            [DataBase putDB:eventEntity toTable:@"events" withKey:@"id"];
            [DataBase putDB:eventEntity toTable:@"visits" withKey:@"id"];
        }
    }
}

- (BOOL) checkPatientIntersectionWithStart:(NSDate*) dateFrom withEnd:(NSDate*) dateTo
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy HH:mm"];
    
    NSInteger days = 0;
    
    for(NSDictionary* dd in [DataBase getDBArray:@"events" withKey:@"type" andValue:[@(kCalendarPatientMode) stringValue]])
    {
        NSDate *range1Start = dateFrom;
        NSDate *range1End = dateTo;
        
        NSDate *userStart   = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", dd[@"startdate"], dd[@"starthours"], dd[@"startminutes"]]];
        NSDate *userEnd     = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@:%@", dd[@"enddate"], dd[@"endhours"], dd[@"endminutes"]]];
        
        NSDate *overlapFrom = [range1Start laterDate:userStart];
        NSDate *overlapTo   = [range1End earlierDate:userEnd];
    
        
        if ([overlapFrom compare:overlapTo] > 0) {
            // Date ranges do not overlap
            days = 0;
        } else {
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *comp = [calendar components:NSMinuteCalendarUnit fromDate:overlapFrom toDate:overlapTo options:0];
            days = [comp day];
            
            break;
        }
        
        NSLog(@"%ld", (long)days);
    }
    
    return days!=0;
}

#pragma mark - JJTopMenuDelegate
-(void)topMenu:(JJTopMenuViewController *)menu didSelectMenuItemAtIndex:(NSUInteger)index
{
    NSLog(@"Selected menu at index: %lu", (unsigned long)index);
    
    [self clearAllData];
    
    self.eventMode = (CalendarEventMode)index;
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.eventMode == kCalendarAffairMode)
        return 7;
    else
        return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.eventMode == kCalendarAffairMode && indexPath.row == 6)
        return 80.0f;
    else if(self.eventMode == kCalendarPatientMode && indexPath.row == 4)
        return 80.0f;
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell2"];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        titleCell.detailTextLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    }
    
    if(self.eventMode == kCalendarAffairMode)
    {
        self.fullDateTimeController.datePicker.minuteInterval = 15;
        self.timeDateController.datePicker.minuteInterval = 15;
        if(indexPath.row == 0)
        {
            EventNameViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EventNameViewCell class])];
            nameCell.textField.placeholder = @"Название";
            nameCell.delegate = self;
            titleCell = nameCell;
            if(self.currentEvent)
            {
                nameCell.textField.text = self.currentEvent[@"title"];
            }
        }
        else if(indexPath.row == 1)
        {
            EventNameViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EventNameViewCell class])];
            nameCell.textField.placeholder = @"Место";
            nameCell.delegate = self;
            titleCell = nameCell;
            if(self.currentEvent)
            {
                nameCell.textField.text = self.currentEvent[@"place"];
            }
        }
        else if(indexPath.row == 2)
        {
            NSString *identifier = @"Switcher";
            titleCell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!titleCell) {
                titleCell = [[AllDayViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                ((AllDayViewCell*)titleCell).delegate = self;
            }
            ((AllDayViewCell*)titleCell).textLabel.text = @"Весь день";
            ((AllDayViewCell*)titleCell).detailTextLabel.text = @"";
            ((AllDayViewCell*)titleCell).textLabel.font = [UIFont systemFontOfSize:12];
            ((AllDayViewCell*)titleCell).detailTextLabel.font = [UIFont systemFontOfSize:12];
            ((AllDayViewCell*)titleCell).textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            
            if(self.currentEvent)
            {
                //[((AllDayViewCell*)titleCell) setState:self.currentEvent[@"isallday"]];
            }
        }
        else if(indexPath.row == 3)
        {
            titleCell.textLabel.text = @"Начало";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
            
            if(startDate)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:startDate];
            }
            else
            {
                titleCell.detailTextLabel.text = @"";
            }
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            
            if(self.currentEvent)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:startDate];
            }
        }
        else if(indexPath.row == 4)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm"];
            titleCell.textLabel.text = @"Окончание";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            if(endDate)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:endDate];
            }
            else
            {
                titleCell.detailTextLabel.text = @"";
            }
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            
            if(self.currentEvent)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:endDate];
            }
        }
        else if(indexPath.row == 5)
        {
            titleCell.textLabel.text = @"Напоминание";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            titleCell.detailTextLabel.text = self.reminderNameArray[selectedReminderRow];
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        }
        else
        {
            CommentViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentViewCell class])];
            [nameCell updateCell:commentString];
            titleCell = nameCell;
        }
        cell = titleCell;
        
    }
    else
    {
        self.fullDateTimeController.datePicker.minuteInterval = 15;
        self.timeDateController.datePicker.minuteInterval = 15;
        
        if(indexPath.row == 0)  {
            EventNameViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EventNameViewCell class])];
            nameCell.textField.placeholder = @"Фамилия Имя Отчество";
            nameCell.delegate = self;
            if(self.currentPatient)
            {
                nameCell.textField.text = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"], self.currentPatient[@"lastname"]];
            }

            titleCell = nameCell;
        } else if(indexPath.row == 1)
        {
            titleCell.textLabel.text = @"Приём";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            if(startDate)
            {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:startDate];
            }
            else
            {
                titleCell.detailTextLabel.text = @"";
            }
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
        }
        else if(indexPath.row == 2)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm"];
            titleCell.textLabel.text = @"Окончание";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            if(endDate)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:endDate];
            }
            else
            {
                titleCell.detailTextLabel.text = @"";
            }
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            
            if(self.currentEvent)
            {
                titleCell.detailTextLabel.text = [dateFormatter stringFromDate:endDate];
            }
        }
        else  if(indexPath.row == 3)
        {
            titleCell.textLabel.text = @"Напоминание";
            titleCell.textLabel.font = [UIFont systemFontOfSize:12];
            titleCell.detailTextLabel.text = self.reminderNameArray[selectedReminderRow];
            titleCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        }
        else
        {
            CommentViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentViewCell class])];
            [nameCell updateCell:commentString];
            titleCell = nameCell;
        }
        cell = titleCell;
    }
    
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing:YES];
    
    if(self.eventMode == kCalendarAffairMode)
    {
        if(indexPath.row == 0)
        {
            EventNameViewCell *nameCell = (EventNameViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            nameCell.textField.delegate = self;
            [nameCell.textField becomeFirstResponder];
        }
        else if(indexPath.row == 1)
        {
            EventNameViewCell *nameCell = (EventNameViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            nameCell.textField.delegate = self;
            [nameCell.textField becomeFirstResponder];
        }
        else if(indexPath.row == 2)
        {
            //
        }
        else if(indexPath.row == 3)
        {
            self.fullDateTimeActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Начало приёма" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.fullDateTimeController];
            self.fullDateTimeActionSheet.tag = 1;
            self.fullDateTimeActionSheet.delegate = self;
            [self.fullDateTimeActionSheet showInView:self.view];
        }
        else  if(indexPath.row == 4)
        {
            self.timeDateActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Конец приёма" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.timeDateController];
            self.timeDateActionSheet.tag = 2;
            self.timeDateActionSheet.delegate = self;
            [self.timeDateActionSheet showInView:self.view];
        }
        else  if(indexPath.row == 5)
        {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Напоминание" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Готово"];
            picker.headerBackgroundColor = getColorScheme();
            picker.delegate = self;
            picker.dataSource = self;
            picker.tag=1;
            picker.allowMultipleSelection = NO;
            [picker show];
        }
        else
        {
            [self onCommonComment];
        }
    }
    else
    {
        if(indexPath.row == 0)  {
            EventNameViewCell *nameCell = (EventNameViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            nameCell.textField.delegate = self;
            [nameCell.textField becomeFirstResponder];
        } else if(indexPath.row == 1)  {
            self.fullDateTimeActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Начало приёма" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.fullDateTimeController];
            self.fullDateTimeActionSheet.tag = 1;
            self.fullDateTimeActionSheet.delegate = self;
            [self.fullDateTimeActionSheet showInView:self.view];
        }
        else  if(indexPath.row == 2)
        {
            self.timeDateActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Окончание приёма" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.timeDateController];
            self.timeDateActionSheet.tag = 2;
            self.timeDateActionSheet.delegate = self;
            [self.timeDateActionSheet showInView:self.view];
        }
        else  if(indexPath.row == 3)  {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Напоминание" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Готово"];
            picker.headerBackgroundColor = getColorScheme();
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = NO;
            picker.tag=1;
            [picker show];
        } else  {
            [self onCommonComment];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    [self.view endEditing:YES];
}

-(void) switchAllDayCell:(AllDayViewCell*)cell stateChanged:(BOOL)state {
    isAllDay = state;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    if(actionSheet.tag == 1)    {
        startDate = self.fullDateTimeController.datePicker.date;
    } else  {
        endDate = self.timeDateController.datePicker.date;
    }
    [self.tableView reloadData];
}

-(void) clearAllData    {
    [self.view endEditing:YES];
    
    startDate = nil;
    endDate = nil;
    EventNameViewCell *nameCell = (EventNameViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    nameCell.textField.text = @"";
    titleString = @"";
    commentString = @"";
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row{
    if(pickerView.tag==1)
    {
        return self.reminderNameArray[row];
    }
    else
    {
        return @"Да";
    }
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    if(pickerView.tag==1)
    {
        return self.reminderNameArray.count;
    }
    else
    {
        return 0;
    }
    
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    if(pickerView.tag==1)
    {
        NSLog(@"%@ is chosen!", self.reminderNameArray[row]);
        selectedReminderRow = row;
        [self.tableView reloadData];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        [DataBase putDB:pickerView.linkedObject toTable:@"events" withKey:@"id"];
        [DataBase putDB:pickerView.linkedObject toTable:@"visits" withKey:@"id"];
    }
    
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    
    if(pickerView.tag==1)
    {

    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        [DataBase putDB:pickerView.linkedObject toTable:@"events" withKey:@"id"];
        [DataBase putDB:pickerView.linkedObject toTable:@"visits" withKey:@"id"];
    }
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

- (void) onCommonComment {

}

-(void) noProcess   {
    //
}

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
      possibleCompletionsForString:(NSString *)string
{
    if(self.eventMode == kCalendarAffairMode)
    {
        return @[];
    }
    else
    {
        NSMutableArray* result = [[NSMutableArray alloc] init];
        
        for(NSDictionary* d in[DataBase getDBArray:@"patient" withKey:@"" andValue:@""])
        {
            if([[NSString stringWithFormat:@"%@ %@ %@",d[@"firstname"],d[@"middlename"],d[@"lastname"]] rangeOfString:string options:NSCaseInsensitiveSearch].location!=NSNotFound)
            {
                PatientAutocompleteObj* pat = [[PatientAutocompleteObj alloc] initWithPatient:d];
                [result addObject:pat];
            }
        }
        
        return result;
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    PatientAutocompleteObj* res = (PatientAutocompleteObj*) selectedObject;
    
    self.currentPatient = res.pat;
}

-(void)doneCommonPad:(id)sender
{
    
}

- (IBAction)tapMenu:(id)sender
{
    self.eventMode = menuSelector.selectedSegmentIndex;
    
    [self.tableView reloadData];
    
}
- (IBAction)toggleMenu:(id)sender
{
    self.eventMode=self.eventMode==0?1:0;
    self.dealView.hidden=!self.dealView.hidden;
    self.patientView.hidden=!self.patientView.hidden;
    [self.tableView reloadData];
}

- (CGFloat) AACStatusBarHeight
{
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    return MIN(statusBarSize.width, statusBarSize.height);
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    /*CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.view.frame = f;
    }];*/
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
    /*[UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;//+self.navigationController.navigationBar.frame.size.height+[self AACStatusBarHeight];
        self.view.frame = f;
    }];*/
    
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

@end
