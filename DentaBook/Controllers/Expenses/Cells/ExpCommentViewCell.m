//
//  ExpCommentViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ExpCommentViewCell.h"

@implementation ExpCommentViewCell
@synthesize commentTextView, commentPlaceholder;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    commentTextView.delegate=self;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    commentPlaceholder.hidden=YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length]>0)
    {
    }
    else
    {
        commentPlaceholder.hidden=NO;
    }
}

@end
