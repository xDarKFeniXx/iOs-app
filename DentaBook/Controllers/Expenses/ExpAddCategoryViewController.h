//
//  ExpAddCategoryViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExpenseCategory;

@interface ExpAddCategoryViewController : UIViewController

-(id) initWithExpenseCategory:(NSDictionary*)expenseCategory;

@end
