//
//  ExpAddCategoryViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ExpAddCategoryViewController.h"
#import "ExpenseCategory.h"
#import "ExpenseSubcategory.h"
#import "ExpenseManager.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>
#import "DataBase.h"
#import "HttpUtil.h"

@interface ExpAddCategoryViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@property (nonatomic, strong) NSDictionary *currentCategory;
@end

@implementation ExpAddCategoryViewController

-(id) initWithExpenseCategory:(NSDictionary*)expenseCategory  {
    self = [super initWithNibName:@"ExpAddCategoryViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentCategory = expenseCategory;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.currentCategory == nil)  {
        self.title = @"Добавить категорию";
    } else  {
        self.title = @"Добавить подкатегорию";
    }
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0)
        return;
    
    if(self.currentCategory == nil)
    {
        NSMutableDictionary *expenseCategory = [[NSMutableDictionary alloc] init];//[ExpenseCategory MR_createEntity];
        expenseCategory[@"id"] = GUIDString();
        expenseCategory[@"title"] = self.categoryTitleLabel.text;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        expenseCategory[@"createat"] = [dateFormatter stringFromDate:[NSDate date]];
        
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        [[ExpenseManager shared] createExpenseCategory:expenseCategory success:^{
            [HUD dismissAnimated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
    }
    else
    {
        NSMutableDictionary *expenseCategory = [[NSMutableDictionary alloc] init];
        expenseCategory[@"id"] = GUIDString();
        expenseCategory[@"title"] = self.categoryTitleLabel.text;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        expenseCategory[@"createat"] = [dateFormatter stringFromDate:[NSDate date]];
        expenseCategory[@"categoryid"] = self.currentCategory[@"id"];
        
        JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
        HUD.textLabel.text = @"Сохранение";
        [HUD showInView:self.view];
        [[ExpenseManager shared] createExpenseSubcategory:expenseCategory success:^{
            [HUD dismissAnimated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^{
            [HUD dismissAnimated:YES];
        }];
    }
}

@end
