//
//  ExpensesViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ExpensesViewController.h"
#import "PutExpenseViewController.h"
#import "ExpenceTableViewCell.h"
#import "UIView+Helpers.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "Expense.h"
#import "ExpenseCategory.h"
#import "ExpenseSubcategory.h"
#import "ExpenseManager.h"
#import "DataBase.h"
#import "DataManager.h"
#import "ImageRequestModel.h"
#import "UIImageView+ImageDownloader.h"
#import "PercUtils.h"
#import "CZPickerView.h"
#import "HttpUtil.h"

@interface ExpensesViewController ()<CZPickerViewDataSource, CZPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ExpensesViewController
@synthesize expences, totalMonthExpencesLabel, addExpenceView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Расходы";
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ExpenceTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ExpenceTableViewCell class])];
    
    [self fetchTableData];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    addExpenceView.backgroundColor = getColorScheme();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    // This will remove extra separators from tableview
    self.tableView.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onPutExpense:(id)sender
{
    PutExpenseViewController *putExpenseVC = [PutExpenseViewController new];
    [self.navigationController pushViewController:putExpenseVC animated:YES];
}

-(void)fetchTableData
{
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"expence" withKey:@"" andValue:@""]];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *arbitraryDate = [NSDate date];
    NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:arbitraryDate];
    [comp setDay:1];
    NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
    
    NSDate *curDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:curDate]; // Get necessary date components
    
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *lastDayOfMonthDate = [calendar dateFromComponents:comps];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    float totalMonthExpences=0;
    
    for(NSDictionary* pat in tmp)
    {
        
        if(
           [[dateFormatter dateFromString:pat[@"createat"]] timeIntervalSince1970]<=[lastDayOfMonthDate timeIntervalSince1970]
           &&
           [[dateFormatter dateFromString:pat[@"createat"]] timeIntervalSince1970]>=[firstDayOfMonthDate timeIntervalSince1970]
           )
        {
            [tmp_sections addObject:pat[@"createat"]];
            totalMonthExpences+=[pat[@"amount"] floatValue];
        }
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:@(totalMonthExpences)];
    totalMonthExpencesLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    totalMonthExpencesLabel.textColor = [UIColor colorWithRed:231.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0];
    
    expences = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"createat"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [expences addObject:ndms];
    }
    
    NSLog(@"Data: %@", expences);
}

#pragma mark -
#pragma mark Option Table View
- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return expences.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray*)expences[section][@"content"]).count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSDictionary* therapyEntity = expences[section];
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    
    headerView.backgroundColor = [UIColor clearColor];
    
    NSString *dateString = therapyEntity[@"section"];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = headerView.bounds;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:12];
    headerLabel.text = dateString;
    headerLabel.textAlignment = NSTextAlignmentLeft;
    headerLabel.left = 20;
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *therapyEntity = expences[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    NSDictionary *expenseCategory = (NSDictionary*)[DataBase getDB:@"expence_category" withKey:@"id" andValue:therapyEntity[@"categoryid"]];//[ExpenseCategory MR_findFirstByAttribute:@"remoteId" withValue:therapyEntity.categoryId];
    NSLog(@"expenseCategory: %@", expenseCategory);
    
    NSDictionary *expenseSubCategory = (NSDictionary*)[DataBase getDB:@"expence_subcategory" withKey:@"id" andValue:therapyEntity[@"subcategoryid"]];//[ExpenseSubcategory MR_findFirstByAttribute:@"remoteId" withValue:therapyEntity.subCategoryId];
    NSLog(@"expenseSubCategory: %@", expenseSubCategory);
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    ExpenceTableViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ExpenceTableViewCell class])];
    chooseCell.expenceTitleLabel.text = [NSString stringWithFormat:@"%@:%@", expenseCategory[@"title"], expenseSubCategory[@"title"]];
    chooseCell.amountLabel.text = [NSString stringWithFormat:@"%@ ₽", [numberFormatter stringFromNumber:@([therapyEntity[@"amount"] integerValue])]];
    chooseCell.owner = self;
    chooseCell.delegate = self;
    chooseCell.rightUtilityButtons = [self rightButtons];

    chooseCell.imgToZoom.imageModel.imageCachePolicy = ImageCachePolicyMemoryAndFileCache;
    chooseCell.imgToZoom.imageModel.queuePriority = NSOperationQueuePriorityHigh;
    chooseCell.imgToZoom.contentMode = UIViewContentModeScaleAspectFit;//UIViewContentModeScaleAspectFit; UIViewContentModeScaleAspectFill
    NSDictionary* imgData = (NSDictionary*)[DataBase getDB:@"photo" withKey:@"expenceid" andValue:therapyEntity[@"id"]];
    NSArray* tst = [DataBase getDBArray:@"photo" withKey:@"" andValue:@""];
    NSLog(@"photos: %@", tst);
    if(imgData)
    {
        chooseCell.hasImageBtn.hidden = NO;
        chooseCell.hasImageImage.hidden = NO;
        [chooseCell.imgToZoom  setImageWithURLString:[NSString stringWithFormat:[DataManager urlForPath:@"expences/%@/image"], imgData[@"id"]]
                                             success:^(ImageRequestModel *object, UIImage *image) {
                                                 
                                             } failure:^(
                                                         ImageRequestModel *object, NSError *error) {
                                                 
                                             } progress:^(ImageRequestModel *object, CGFloat progress) {
                                                 //
                                             }];
    }
    else
    {
        chooseCell.hasImageBtn.hidden = YES;
        chooseCell.hasImageImage.hidden = YES;
    }
    
    return chooseCell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index)
    {
        case 0:
        {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.selectedCell = [self.tableView indexPathForCell:cell];
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *therapyEntity = expences[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    PutExpenseViewController *putExpenseVC = [[PutExpenseViewController alloc] initWithExpense:therapyEntity];
    [self.navigationController pushViewController:putExpenseVC animated:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *entity = expences[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[ExpenseManager shared] deleteExpenseById:entity[@"id"] success:^{
        
        //
    } failure:^{
        //
    }];
    [self.tableView reloadData];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    
    NSDictionary *entity = expences[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[ExpenseManager shared] deleteExpenseById:entity[@"id"] success:^{
        
        //
    } failure:^{
        //
    }];
    [self.tableView reloadData];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

@end
