//
//  FinancialIntervalViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 24.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "FinancialIntervalViewController.h"
#import "UIView+Helpers.h"
#import "WLPActionSheet.h"
#import "DBFullDateController.h"

#import <NSDate+Calendar/NSDate+Calendar.h>
#import "HttpUtil.h"

@interface FinancialIntervalViewController ()

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (nonatomic, strong) WLPActionSheet *monthYearActionSheet;
@property (nonatomic, strong) DBFullDateController *monthYearStartDateController;
@property (nonatomic, strong) DBFullDateController *monthYearEndDateController;

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

@end

@implementation FinancialIntervalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onSuccess)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Готово" forState:UIControlStateNormal];
    [button setTitle:@"Готово" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.monthYearStartDateController = [DBFullDateController new];
    self.monthYearEndDateController = [DBFullDateController new];
    
    self.startDate = [NSDate dateWithYear:[NSDate date].year month:[NSDate date].month day:1];
    self.endDate = [NSDate dateWithYear:[NSDate date].year month:[NSDate date].month day:[NSDate date].day];

    self.monthYearStartDateController.datePicker.date = self.startDate;
    self.monthYearEndDateController.datePicker.minimumDate = self.startDate;
    self.monthYearEndDateController.datePicker.date = self.endDate;
    
    [self updateStartLabel];
    [self updateEndLabel];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
}


-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) onSuccess   {
    if(self.didChooseInterval)  {
        self.didChooseInterval(self.startDate, self.endDate);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onStartDate:(id)sender {
    self.monthYearActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата начала" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.monthYearStartDateController];
    self.monthYearActionSheet.tag = 1;
    self.monthYearActionSheet.delegate = self;
    [self.monthYearActionSheet showInView:self.view];
}

- (IBAction)onEndDate:(id)sender {
    self.monthYearActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата окончания" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.monthYearEndDateController];
    self.monthYearActionSheet.tag = 2;
    self.monthYearActionSheet.delegate = self;
    [self.monthYearActionSheet showInView:self.view];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    if(actionSheet.tag == 1)    {
        self.startDate = self.monthYearStartDateController.datePicker.date;
        [self updateStartLabel];
    } else  {
        self.monthYearEndDateController.datePicker.minimumDate = self.startDate;
        self.endDate = self.monthYearEndDateController.datePicker.date;
        [self updateEndLabel];
    }
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}

-(void) updateStartLabel    {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMMM yyyy"];
    NSString *dateStr = dateStr = [df stringFromDate:self.startDate];
    dateStr = [[[dateStr substringToIndex:1] uppercaseString] stringByAppendingString:[dateStr substringFromIndex:1]];
    self.startDateLabel.text = [NSString stringWithFormat:@"%@", dateStr];
}

-(void) updateEndLabel    {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMMM yyyy"];
    NSString *dateStr = dateStr = [df stringFromDate:self.endDate];
    dateStr = [[[dateStr substringToIndex:1] uppercaseString] stringByAppendingString:[dateStr substringFromIndex:1]];
    self.endDateLabel.text = [NSString stringWithFormat:@"%@", dateStr];
}

@end
