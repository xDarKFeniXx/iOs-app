//
//  MoneyDepositViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface MoneyDepositViewController : UIViewController<UITextViewDelegate>

-(id) initWithPatient:(NSDictionary*)patient withMoney:(float)sum;
-(id) initWithPatient:(NSDictionary*)patient withMoney:(float)sum andVisit:(NSString*) visit_id;
@property (weak, nonatomic) IBOutlet UITextView *commentField;
@property (weak, nonatomic) IBOutlet UILabel *placeholderTextView;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *patientName;

@end
