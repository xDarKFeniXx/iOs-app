//
//  MoneyDepositViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "MoneyDepositViewController.h"
#import "WLPActionSheet.h"
#import "DBFullDateController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy+Custom.h"
#import "TherapyItem.h"
#import "Payment.h"
#import "PaymentManager.h"
#import "DataBase.h"
#import "PercUtils.h"
#import "HttpUtil.h"

@interface MoneyDepositViewController () <WLPActionSheetDelegate>
@property (nonatomic, strong) NSDictionary *currentPatient;
@property (nonatomic) float currentSum;
@property (nonatomic, strong) NSString *currentVisit;

@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;

@property (nonatomic, strong) WLPActionSheet *dateActionSheet;
@property (nonatomic, strong) DBFullDateController *dateController;

@end

@implementation MoneyDepositViewController  {
    NSDate *selectedDate;
    double enteredSum;
}
@synthesize commentField, placeholderTextView, illnessImg, patientName;
-(id) initWithPatient:(NSDictionary*)patient withMoney:(float)sum {
    self = [super initWithNibName:@"MoneyDepositViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.currentSum = sum;
        self.currentVisit = nil;
    }
    return self;
}

-(id) initWithPatient:(NSDictionary*)patient withMoney:(float)sum andVisit:(NSString*) visit_id
{
    self = [super initWithNibName:@"MoneyDepositViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.currentSum = sum;
        self.currentVisit = visit_id;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"Внести деньги";
    
    //self.navigationItem.titleView = label;
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        patientName.attributedText = attachmentString;
    }
    else
    {
        patientName.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    enteredSum = 0;
    
    if(self.currentSum != 0)    {
        self.amountTextField.text = [@(self.currentSum) stringValue];
        enteredSum = self.currentSum;
    }
    
    self.dateController = [DBFullDateController new];
    selectedDate = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:selectedDate];
    self.dateTextField.text = strDate;
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 30, 28);
    [button setTitle:@"    " forState:UIControlStateNormal];
    [button setTitle:@"    " forState:UIControlStateSelected];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    commentField.delegate = self;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
    }
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.amountTextField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
}

-(void) viewWillAppear:(BOOL)animated   {
    [self.amountTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onDate:(id)sender {
    [self.view endEditing:YES];
    
    self.dateActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Дата" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.dateController];
    self.dateActionSheet.tag = 1;
    self.dateActionSheet.delegate = self;
    [self.dateActionSheet showInView:self.view];
}

-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet  {
    selectedDate = self.dateController.datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:selectedDate];
    self.dateTextField.text = strDate;
}

-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet   {
    NSLog(@"DESTRUCTIVE");
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    NSLog(@"text changed: %@", theTextField.text);
    
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setGroupingSeparator:@" "];
    [formatter setGroupingSize:3];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setDecimalSeparator:@"."];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    self.amountTextField.text=formattedOutput;
}

- (IBAction)onTransfer:(id)sender {
    
    if(enteredSum == 0) {
        return;
    }
    
    //NSInteger index = [Payment MR_findAll].count;
    
    NSMutableDictionary *paymentEntity = [[NSMutableDictionary alloc] init];//[Payment MR_createEntity];
    paymentEntity[@"amount"] = @(enteredSum);
    paymentEntity[@"visitid"] = self.currentVisit;
    paymentEntity[@"comment"] = commentField.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    paymentEntity[@"createat"] = [dateFormatter stringFromDate:selectedDate];
    paymentEntity[@"id"] = GUIDString();
    paymentEntity[@"patientid"] = self.currentPatient[@"patientid"];
    //[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Сохранение";
    [HUD showInView:self.view];
    [[PaymentManager shared] createPayment:paymentEntity success:^{
        [HUD dismissAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needClose" object:nil];
    } failure:^{
        [HUD dismissAnimated:YES];
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    placeholderTextView.hidden=YES;
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text length]>0)
    {
    }
    else
    {
        placeholderTextView.hidden=NO;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    /*if(1==1)
    {
        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        enteredSum = [cleanCentString floatValue];
    
    }*/
    enteredSum = [[[[textField text] stringByReplacingCharactersInRange:range withString:string]stringByReplacingOccurrencesOfString:@" " withString:@""] floatValue];
    return YES;
}




-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
