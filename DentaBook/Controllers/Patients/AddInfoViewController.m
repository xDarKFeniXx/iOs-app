//
//  AddInfoViewController.m
//  DentaBook
//
//  Created by Mac Mini on 29.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AddInfoViewController.h"
#import "ViewPhoneTableViewCell.h"
#import "PercUtils.h"
#import "HttpUtil.h"
#import "BirthdayTableViewCell.h"

@interface AddInfoViewController ()
@property (nonatomic, strong) NSDictionary *currentPatient;
@end

@implementation AddInfoViewController
@synthesize table,phoneLabel, nameLabel, illnessImg, birthdayLabel;

-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"AddInfoViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
    }
    return self;
}

- (IBAction)tapCall:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.currentPatient[@"phone"]]]];
}

- (IBAction)tapSMS:(id)sender
{
    [self showSMS:self.currentPatient[@"phone"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    table.delegate = self;
    table.dataSource = self;
    
    self.title = @"Дополнительная информация";
    
    // Do any additional setup after loading the view from its nib.
    [self.table registerNib:[UINib nibWithNibName:@"ViewPhoneTableViewCell" bundle:nil] forCellReuseIdentifier:@"phoneCell"];
    [self.table registerNib:[UINib nibWithNibName:@"BirthdayTableViewCell" bundle:nil] forCellReuseIdentifier:@"birthdayCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(smsCalled:) name:@"smsCalled" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emailCalled:) name:@"emailCalled" object:nil];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
    }
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    if(self.currentPatient[@"birthdate"])
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.yyyy"];
        
        NSDate* dt =  [df dateFromString:self.currentPatient[@"birthdate"]];
        
        
        NSDateComponents *components;
        NSInteger years;
        
        components = [[NSCalendar currentCalendar] components: NSYearCalendarUnit
                                                     fromDate: dt toDate: [NSDate date] options:0];
        years = [components year];
        
        birthdayLabel.text=[NSString stringWithFormat:@"%@, возраст %ld", self.currentPatient[@"birthdate"], (long)years];
    }
    
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.table.tableFooterView = [UIView new];
    
}

- (void) smsCalled:(NSNotification*) n
{
    [self showSMS:n.object];
}

- (void)showSMS:(NSString*)file {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[file];
    NSString *message = @"";//[NSString stringWithFormat:@"", file];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void) emailCalled:(NSNotification*) n
{
    [self showSMS:n.object];
}

- (void)sendEmail:(NSString*) email
{
    // Email Subject
    NSString *emailTitle = @"";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:email];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) viewDidAppear:(BOOL)animated
{
    phoneLabel.text = self.currentPatient[@"phone"];
    [table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.currentPatient[@"additional_info"] allKeys].count + 2;// + self.titleItemArray.count + ((((NSArray*)self.currentPatient[@"illness"]).count > 0) ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section<2)
    {
        return 1;
    }
    else
    {
        NSString* key = [self.currentPatient[@"additional_info"] allKeys][section-2];
        return ((NSArray*)self.currentPatient[@"additional_info"][key]).count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1)
    {
        if(self.currentPatient[@"birthdate"])
        {
            return 44.0f;
        }
        else return 0;
    }
    
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* title_code = [self.currentPatient[@"additional_info"] allKeys][section];
    NSString* title_name;
    
    if([title_code isEqualToString:@"phone"])
        title_name=@"Телефон";
    if([title_code isEqualToString:@"email"])
        title_name=@"E-mail";
    if([title_code isEqualToString:@"address"])
        title_name=@"Адрес";
    if([title_code isEqualToString:@"occupancy"])
        title_name=@"Род деятельности";
    if([title_code isEqualToString:@"custom"])
        title_name=@"Дополнительное поле";
    
    return title_name;
}*/

/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(15, 12, 320, 20);
    myLabel.font = [UIFont systemFontOfSize:12];
    myLabel.textColor = [UIColor colorWithRed:72.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1];
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, tableView.frame.size.width, 1)];
    [headerView addSubview:myLabel];
    
    CGRect sepFrame = CGRectMake(0, headerView.frame.size.height-1, headerView.frame.size.width, 1);
    UIView* seperatorView = [[UIView alloc] initWithFrame:sepFrame];
    seperatorView.backgroundColor = [UIColor colorWithRed:224.0f/255.0f green:223.0f/255.0f blue:224.0f/255.0f alpha:1];
    [headerView addSubview:seperatorView];
    
    return headerView;
}*/



#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section<2)
    {
        if(indexPath.section==0)
        {
            ViewPhoneTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
            
            titleCell.phoneLabel.text = self.currentPatient[@"phone"];
            titleCell.phoneTypeLabel.text = @"Телефон";
            titleCell.phoneBtn.hidden = NO;
            titleCell.smsBtn.hidden = NO;
            titleCell.emailBtn.hidden = YES;
            
            return titleCell;

        }
        else
        {
            if(self.currentPatient[@"birthdate"])
            {
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"dd.MM.yyyy"];
                
                NSDate* dt =  [df dateFromString:self.currentPatient[@"birthdate"]];
                
                
                NSDateComponents *components;
                NSInteger years;
                
                components = [[NSCalendar currentCalendar] components: NSYearCalendarUnit
                                                             fromDate: dt toDate: [NSDate date] options:0];
                years = [components year];
                
                BirthdayTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"birthdayCell"];
                titleCell.birthdayLabel.text=[NSString stringWithFormat:@"%@, возраст %ld", self.currentPatient[@"birthdate"], (long)years];
                
                return titleCell;
            }
            else
            {
                UITableViewCell *chooseCell;
                chooseCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
                if (chooseCell == nil)
                {
                    chooseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell"];
                }

                return chooseCell;
            }
        }
    }
    else
    {
        NSString* key = [self.currentPatient[@"additional_info"] allKeys][indexPath.section-2];
        NSDictionary *addInfo = ((NSArray*)self.currentPatient[@"additional_info"][key])[indexPath.row];
        
        if([key isEqualToString:@"phone"])
        {
            ViewPhoneTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
            
            titleCell.phoneLabel.text = addInfo[@"value"];
            titleCell.phoneTypeLabel.text = addInfo[@"field_name"];
            titleCell.phoneBtn.hidden = NO;
            titleCell.smsBtn.hidden = NO;
            titleCell.emailBtn.hidden = YES;
            
            return titleCell;
        }
        else if([key isEqualToString:@"email"])
        {
            ViewPhoneTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
            
            titleCell.phoneLabel.text = addInfo[@"value"];
            titleCell.phoneTypeLabel.text = addInfo[@"field_name"];
            titleCell.phoneBtn.hidden = YES;
            titleCell.smsBtn.hidden = YES;
            titleCell.emailBtn.hidden = NO;
            
            return titleCell;
        }
        else if([key isEqualToString:@"address"])
        {
            UITableViewCell *chooseCell;
            chooseCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
            if (chooseCell == nil) {
                chooseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell"];
            }
            
            chooseCell.textLabel.text = addInfo[@"field_name"];
            chooseCell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@, %@, %@",addInfo[@"value"][@"city"],addInfo[@"value"][@"street"],addInfo[@"value"][@"house"],addInfo[@"value"][@"flat"]];
            
            chooseCell.textLabel.font = [UIFont systemFontOfSize:12];
            chooseCell.textLabel.textColor = [UIColor colorWithRed:72.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1];
            
            chooseCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            chooseCell.detailTextLabel.textColor = [UIColor colorWithRed:114.0f/255.0f green:114.0f/255.0f blue:114.0f/255.0f alpha:1];
            
            return chooseCell;
        }
        else
        {
            UITableViewCell *chooseCell;
            chooseCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
            if (chooseCell == nil) {
                chooseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell"];
            }
            
            chooseCell.textLabel.text = addInfo[@"field_name"];
            chooseCell.detailTextLabel.text = addInfo[@"value"];
            
            chooseCell.textLabel.font = [UIFont systemFontOfSize:12];
            chooseCell.textLabel.textColor = [UIColor colorWithRed:72.0f/255.0f green:72.0f/255.0f blue:72.0f/255.0f alpha:1];
            
            chooseCell.detailTextLabel.font = [UIFont systemFontOfSize:12];
            chooseCell.detailTextLabel.textColor = [UIColor colorWithRed:114.0f/255.0f green:114.0f/255.0f blue:114.0f/255.0f alpha:1];
            
            return chooseCell;
        }
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
