//
//  AddressTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 07.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AddressTableViewCell.h"

@implementation AddressTableViewCell

@synthesize typeButton, city_field, streetField, houseField, texts, type, field_name, idi, flatField;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    city_field.delegate = self;
    streetField.delegate = self;
    houseField.delegate = self;
    flatField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)tapTypeButton:(id)sender {
    CustomIOS7AlertView *alertView = [CustomIOS7AlertView alertWithTitle:@"Выберите тип" message:nil andOwnr:sender];
    [alertView setButtonTitles:texts];

    [alertView setDelegate:self];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %ld.", buttonIndex, (long)[alertView tag]);
        [alertView close];
    }];
    [alertView show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
    [typeButton setTitle:texts[buttonIndex] forState:UIControlStateNormal];
    [typeButton setTitle:texts[buttonIndex] forState:UIControlStateSelected];
    field_name=texts[buttonIndex];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":@{@"city":city_field.text, @"street":streetField.text, @"house":houseField.text, @"flat":flatField.text}, @"type":type, @"field_name":field_name, @"id":idi}];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == city_field)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":@{@"city":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"street":streetField.text, @"house":houseField.text, @"flat":flatField.text}, @"type":type, @"field_name":field_name, @"id":idi}];
    else if(textField == streetField)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":@{@"city":city_field.text, @"street":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"house":houseField.text, @"flat":flatField.text}, @"type":type, @"field_name":field_name, @"id":idi}];
    else if(textField == houseField)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":@{@"city":city_field.text, @"street":streetField.text, @"house":[[textField text] stringByReplacingCharactersInRange:range withString:string], @"flat":flatField.text}, @"type":type, @"field_name":field_name, @"id":idi}];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"customChanged" object:@{@"text":@{@"city":city_field.text, @"street":streetField.text, @"house":houseField.text, @"flat":[[textField text] stringByReplacingCharactersInRange:range withString:string]}, @"type":type, @"field_name":field_name, @"id":idi}];
    
    return true;
}

- (IBAction)tapRemove:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"customDeleted" object:@{@"type":type, @"field_name":field_name, @"id":idi}];
}

@end
