//
//  CustomCustomCell.h
//  DentaBook
//
//  Created by Mac Mini on 30.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCustomCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *valueField;

@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *field_name;
@property (retain, nonatomic) NSString *idi;
- (IBAction)tapRemove:(id)sender;

@end
