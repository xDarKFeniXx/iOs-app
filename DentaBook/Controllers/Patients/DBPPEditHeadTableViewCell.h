//
//  DBPPEditHeadTableViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 05.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HCSStarRatingView;

@interface DBPPEditHeadTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *patientLogoImageView;
@property (weak, nonatomic) IBOutlet UITextField *patientFirstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *patientLastNameTextField;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@property (copy, nonatomic) void (^didClickAddPhotoToPatient)();
- (IBAction)didClickDiscount:(id)sender;

@end
