//
//  DBPPEditHeadTableViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 05.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBPPEditHeadTableViewCell.h"
#import "HCSStarRatingView.h"
#import "PercUtils.h"

@implementation DBPPEditHeadTableViewCell
@synthesize discountLabel,patientFirstNameTextField,patientLastNameTextField, ratingView;
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    patientFirstNameTextField.delegate = self;
    patientLastNameTextField.delegate = self;
    self.patientLogoImageView.layer.cornerRadius = self.patientLogoImageView.frame.size.width/2;
    self.patientLogoImageView.layer.masksToBounds = YES;
    [ratingView addTarget:self action:@selector(didChangeRating:) forControlEvents:UIControlEventValueChanged];
    [ratingView setEmptyStarImage:[PercUtils imageWithImage:[UIImage imageNamed:@"star_unselected.png"] scaledToSize:CGSizeMake(12.0f, 12.0f)]];
    [ratingView setFilledStarImage:[PercUtils imageWithImage:[UIImage imageNamed:@"star_selected.png"] scaledToSize:CGSizeMake(12.0f, 12.0f)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)onPhoto:(id)sender {
    if(self.didClickAddPhotoToPatient)  {
        self.didClickAddPhotoToPatient();
    }
}

- (void) didChangeRating:(id) sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ratingChanged" object:@{@"rating":[NSNumber numberWithFloat:((HCSStarRatingView*) sender).value] }];
}

- (IBAction)didClickDiscount:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"discountMenuCalled" object:@{@"sender":sender}];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == patientFirstNameTextField)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"lastnameChanged" object:@{@"text":[[textField text] stringByReplacingCharactersInRange:range withString:string]}];
    if(textField == patientLastNameTextField)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"firstnameChanged" object:@{@"text":[[textField text] stringByReplacingCharactersInRange:range withString:string]}];
    
    return true;
}
@end
