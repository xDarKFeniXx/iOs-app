//
//  DBPPHeadTableViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBPPHeadTableViewCell.h"
#import "HCSStarRatingView.h"
@implementation DBPPHeadTableViewCell
@synthesize discountLabel, ratingView;

- (void)awakeFromNib {
    
    self.patientLogoImageView.layer.cornerRadius = self.patientLogoImageView.frame.size.width/2;
    self.patientLogoImageView.layer.masksToBounds = YES;
    [ratingView setEmptyStarImage:[UIImage imageNamed:@"star_unselected.png"]];
    [ratingView setFilledStarImage:[UIImage imageNamed:@"star_selected.png"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)discountBtnClick:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"discountMenuCalled" object:nil];
}
@end
