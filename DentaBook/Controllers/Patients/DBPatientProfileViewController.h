//
//  DBPatientProfileViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface DBPatientProfileViewController : UIViewController

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSArray* events;
@property (nonatomic, retain) NSDictionary* last_event;
@property (nonatomic, retain) NSDictionary* last_past_event;
@property (nonatomic, retain) NSArray* visits;

@end
