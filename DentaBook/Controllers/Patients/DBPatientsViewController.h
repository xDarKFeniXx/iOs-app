//
//  DBPatientsViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBMainViewController.h"
#import "UIFlatSearchBar.h"
@interface DBPatientsViewController : DBMainViewController
@property (nonatomic, retain) NSMutableArray* patients;
@property (nonatomic, retain) NSMutableArray* sections;
- (IBAction)changeSort:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSelector;
- (IBAction)tapSelector:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *suggestView;
@property (weak, nonatomic) IBOutlet UIImageView *magnifyerImg;
@property (weak, nonatomic) IBOutlet UILabel *searchPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *slideView;

@end
