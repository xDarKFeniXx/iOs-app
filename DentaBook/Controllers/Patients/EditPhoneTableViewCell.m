//
//  EditPhoneTableViewCell.m
//  DentaBook
//
//  Created by Mac Mini on 11.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "EditPhoneTableViewCell.h"

@implementation EditPhoneTableViewCell
@synthesize phoneTextField;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.phoneTextField.delegate=self;
    //[self.phoneTextField addTarget:self action:@selector(phoneFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [/*[NSString stringWithFormat:@"%@%@",textField.text,string]*/[[textField text] stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:@"" withString:@""];
    
    // if it's the phone number textfield format it.
    if(textField.tag==textField.tag/*102*/) {
        if (range.length == 1)
        {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        }
        else
        {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"phoneChanged" object:@{@"text":[textField text]}];
        return false;
    }
    
    
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar
{
    @try
    {
        NSString* tst_string = [simpleNumber copy];
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        if(simpleNumber.length==0)
        {
            
            if([tst_string hasPrefix:@"+"])
                return [NSString stringWithFormat:@"+%@", simpleNumber];
            else
                return simpleNumber;
        }
        // use regex to remove non-digits(including spaces) so we are left with just the numbers
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
        simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
        
        // check if the number is to long
        if(simpleNumber.length>14) {
            // remove last extra chars.
            simpleNumber = [simpleNumber substringToIndex:14];
        }
        
        if(deleteLastChar) {
            // should we delete the last digit?
            simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
        }
        
        // 123 456 7890
        // format the number.. if it's less then 7 digits.. then use this regex.
        if(simpleNumber.length>11)
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d{3})(\\d{3})(\\d{2})(\\d{2})(\\d+)"
                                                                    withString:@"$1($2) $3-$4-$5-$6"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        
        else if(simpleNumber.length==11)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                   withString:@"$1($2) $3-$4-$5"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==10)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                   withString:@"$1($2) $3-$4-$5"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==9)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{1})(\\d+)"
                                                                   withString:@"$1($2) $3-$4-$5"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==8)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d+)"
                                                                   withString:@"$1($2) $3-$4"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==7)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{2})(\\d+)"
                                                                   withString:@"$1($2) $3-$4"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==6)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{1})(\\d+)"
                                                                   withString:@"$1($2) $3-$4"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==5)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d+)"
                                                                   withString:@"$1($2) $3"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else if(simpleNumber.length==4)  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d+)"
                                                                   withString:@"$1($2)"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        else  // else do this one..
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                   withString:@"$1($2) $3-$4-$5"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        if([tst_string hasPrefix:@"+"])
            return [NSString stringWithFormat:@"+%@", simpleNumber];
        else
            return simpleNumber;
    }
    @catch(NSException* ex)
    {
        
    }
    
    return simpleNumber;
}

@end
