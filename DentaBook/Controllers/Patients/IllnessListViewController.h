//
//  IllnessListViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 30.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface IllnessListViewController : UIViewController

-(id) initWithPatient:(NSMutableDictionary*)patient isReadOnly:(BOOL)isReadOnly;
@property (nonatomic, retain) NSArray* diagnoses;
@property (nonatomic, retain) NSArray* patient_illness;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
