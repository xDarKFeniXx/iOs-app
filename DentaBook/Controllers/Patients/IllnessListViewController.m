//
//  IllnessListViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 30.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "IllnessListViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "PatientIllness.h"
#import "PatientIllnessRef.h"
#import "DataBase.h"
#import "IllnessAddGroupViewController.h"
#import "IllnessManager.h"
#import "PercUtils.h"
#import "HttpUtil.h"

@interface IllnessListViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *currentPatient;
@property (nonatomic) BOOL isReadOnly;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation IllnessListViewController
@synthesize diagnoses, patient_illness, nameLabel;

-(id) initWithPatient:(NSMutableDictionary*)patient isReadOnly:(BOOL)isReadOnly {
    self = [super initWithNibName:@"IllnessListViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        self.isReadOnly = isReadOnly;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Заболевания";
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        nameLabel.attributedText = attachmentString;
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button setTitle:@"Сохранить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            tempRect.origin.y += 20.0f; //Height of status bar
            [sub setFrame:tempRect];
        }
    }
    
    [self fetchTableData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    //self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView.tableFooterView = [UIView new];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) editIllnessList {
    self.tableView.userInteractionEnabled = YES;
    [self.tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

-(void)fetchTableData {
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
    if(!self.isReadOnly)
    {
        diagnoses = [DataBase getDBArray:@"illness" withKey:@"" andValue:@""];
    }
    else
    {
        diagnoses = self.currentPatient[@"illness"];
    }
    //patient_illness = [DataBase getDBArray:@"illness" withKey:@"" andValue:@""];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.isReadOnly?diagnoses.count:diagnoses.count+1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row<diagnoses.count)
    {
        UITableViewCell *chooseCell;
        
        NSDictionary *patientIllness = diagnoses[indexPath.row];//[PatientIllness MR_findAllSortedBy:@"remoteId" ascending:YES][indexPath.row];
        
        chooseCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
        if (chooseCell == nil) {
            chooseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell"];
        }
        
        chooseCell.textLabel.text = patientIllness[@"title"];
        chooseCell.textLabel.font = [UIFont systemFontOfSize:12];
        chooseCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        
        BOOL is_found = false;
        
        for(NSDictionary* dic in self.currentPatient[@"illness"])
        {
            if([dic[@"id"] isEqualToString:patientIllness[@"id"]])
            {
                is_found = true;
                break;
            }
        }
        
        /*PatientIllnessRef *illnessRef = [PatientIllnessRef MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"index == %@ AND patient.remoteId == %@", patientIllness.remoteId, self.currentPatient.remoteId]];*/
        if(!self.isReadOnly)
        {
            if(is_found)  {
                UIImageView *checkImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"patientillness-check-icon"]];
                chooseCell.accessoryView = checkImageView;
            } else  {
                UIImageView *uncheckImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"patientillness-uncheck-icon"]];
                chooseCell.accessoryView = uncheckImageView;
            }
        }
        chooseCell.selectionStyle = UITableViewCellSelectionStyleNone;
        /*  }*/
        
        return chooseCell;
    }
    else
    {
        UITableViewCell *chooseCell;
        
        chooseCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell1"];
        if (chooseCell == nil) {
            chooseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"titleCell1"];
        }
        
        chooseCell.textLabel.text = @"Добавить заболевание";
        chooseCell.textLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:162.0/255.0 blue:243.0/255.0 alpha:1.0];
        chooseCell.textLabel.font = [UIFont systemFontOfSize:12];
        
        return chooseCell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.isReadOnly)
    {
        if(indexPath.row<diagnoses.count)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            //NS *patientIllness = [PatientIllness MR_findAllSortedBy:@"remoteId" ascending:YES][indexPath.row];
            NSDictionary* curr = diagnoses[indexPath.row];
            //PatientIllnessRef *illnessRef = [PatientIllnessRef MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"index == %@ AND patient.remoteId == %@", patientIllness.remoteId, self.currentPatient.remoteId]];
            BOOL is_found = false;
            id to_remove;
            
            for(NSDictionary* dic in self.currentPatient[@"illness"])
            {
                if([dic[@"id"] isEqualToString:curr[@"id"]])
                {
                    //[DataBase killDB:@"patient_illness" withKey:@"id" andValue:dic[@"id"]];
                    is_found = true;
                    to_remove = dic;
                    break;
                }
            }
            
            if(!is_found)
            {
                [self.currentPatient[@"illness"] addObject:curr];
            }
            else
            {
                [self.currentPatient[@"illness"] removeObject:to_remove];
            }
            
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        else
        {
            IllnessAddGroupViewController *dnt = [IllnessAddGroupViewController new];
            dnt.didClickCreateName = ^(NSString *name)  {
                //            PatientIllness *categoryTherapy = [PatientIllness MR_createEntity];
                //            categoryTherapy.remoteId = @([PatientIllness MR_findAll].count);
                //            categoryTherapy.name = name;
                //            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                
                [[IllnessManager shared] createIllnessByTitle:name success:^{

                } failure:^{

                }];
            };
            [self.navigationController pushViewController:dnt animated:YES];
        }
    }
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
