//
//  PhotoCollectionViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PhotoCollectionViewCell.h"
#import "UIView+Helpers.h"

@interface PhotoCollectionViewCell()
@property (nonatomic, strong) UITextView *commentTextField;
@property (nonatomic, strong) UIButton *trashButton;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UIView *seperateView;
@end

@implementation PhotoCollectionViewCell
@synthesize selectView;
- (id)initWithFrame:(CGRect)aRect   {
    self = [super initWithFrame:aRect];
    if(self)    {
        [self setup];
    }
    return self;
}

-(void) setup   {
    CGFloat squareSize = ([[UIScreen mainScreen] bounds].size.width-40);
    self.backgroundColor = [UIColor whiteColor];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-0, self.frame.size.height-0)];
    self.imageView.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216/255.0 blue:216.0/255.0 alpha:1.0];
    [self.contentView addSubview:self.imageView];
    
    self.selectView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-10, self.frame.size.height-10, 15, 15)];
    self.selectView.image = [UIImage imageNamed:@"affair-check-icon"];
    [self.contentView addSubview:self.selectView];
    
    self.trashButton = [[UIButton alloc] initWithFrame:CGRectMake(15, (self.imageView.frame.origin.y + self.imageView.frame.size.height) + 7.0, 18.0, 22.0)];
    [self.trashButton setImage:[UIImage imageNamed:@"gallery-trash-icon"] forState:UIControlStateNormal];
    [self.trashButton addTarget:self action:@selector(onDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.trashButton];
    
    self.moreButton = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width - 15.0 - 32.0), (self.imageView.frame.origin.y + self.imageView.frame.size.height) + 7.0, 32.0, 22.0)];
    [self.moreButton setImage:[UIImage imageNamed:@"gallery-more-icon"] forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(onMore:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.moreButton];
    
    self.seperateView = [[UIView alloc] initWithFrame:CGRectMake(12.0, (self.trashButton.frame.origin.y + self.trashButton.frame.size.height) + 7, squareSize-16, 1.0)];
    self.seperateView.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0 alpha:1.0];
    [self.contentView addSubview:self.seperateView];
    
    self.commentTextField = [[UITextView alloc] init];
    self.commentTextField.editable = NO;
    self.commentTextField.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    self.commentTextField.backgroundColor = [UIColor clearColor];
    self.commentTextField.textColor = [UIColor colorWithWhite:74.0f/255.0f alpha:1];
    [self.contentView addSubview:self.commentTextField];
}

-(void) setMode:(BOOL)isFullMode    {
    if(isFullMode)  {
        self.commentTextField.hidden = NO;
        self.trashButton.hidden = NO;
        self.moreButton.hidden = NO;
        self.seperateView.hidden = NO;
        self.imageView.frame = CGRectMake(2, 2, self.frame.size.width-4, self.frame.size.width-4);
    } else  {
        self.commentTextField.hidden = YES;
        self.trashButton.hidden = YES;
        self.moreButton.hidden = YES;
        self.seperateView.hidden = YES;
        self.imageView.frame = CGRectMake(2, 2, self.frame.size.width-4, self.frame.size.height-4);
    }
}

-(void) setCommentString:(NSString *)commentString  {
    CGFloat squareSize = ([[UIScreen mainScreen] bounds].size.width-40);
    self.trashButton.frame = CGRectMake(15, (self.imageView.frame.origin.y + self.imageView.frame.size.height) + 7.0, 18.0, 22.0);
    self.moreButton.frame = CGRectMake((self.frame.size.width - 15.0 - 32.0), (self.imageView.frame.origin.y + self.imageView.frame.size.height) + 7.0, 32.0, 22.0);
    
    if(commentString.length > 0)    {
        self.seperateView.frame = CGRectMake(12.0, (self.trashButton.frame.origin.y + self.trashButton.frame.size.height) + 7, squareSize-16, 1.0);

        CGFloat height = [PhotoCollectionViewCell heightText:commentString];
        self.commentTextField.frame = CGRectMake(12.0, (self.seperateView.frame.origin.y + self.seperateView.frame.size.height) + 7.0, squareSize-16, height);
        self.commentTextField.text = commentString;
        //self.commentTextField
    } else  {
        self.commentTextField.hidden = YES;
        self.seperateView.hidden = YES;
    }
}

+ (CGFloat) heightText:(NSString *)text {
    CGFloat squareSize = ([[UIScreen mainScreen] bounds].size.width-40);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:3];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, [text length])];
    
    CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(squareSize, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return rect.size.height+10;
}

-(void) onDelete:(id)sender {
    if(self.didClickDeleteButton)   {
        self.didClickDeleteButton(self.indexPath);
    }
}

-(void) onMore:(id)sender   {
    if(self.didClickMoreButton)   {
        self.didClickMoreButton(self.indexPath);
    }
}

@end
