//
//  ShareViewController.m
//  DentaBook
//
//  Created by Mac Mini on 11.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ShareViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HttpUtil.h"
@interface ShareViewController ()

@end

@implementation ShareViewController
@synthesize bgView, cancelBackView, multiple;
- (void)viewDidLoad {
    [super viewDidLoad];
    bgView.layer.cornerRadius = 2;
    bgView.layer.masksToBounds = NO;
    cancelBackView.backgroundColor = getColorScheme();
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tapCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)tapPrint:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:multiple?@"needPrintMultiple":@"needPrint" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapConvert:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:multiple?@"needConvertMultiple":@"needConvert" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapEmail:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:multiple?@"needEmailMultiple":@"needEmail" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
