//
//  AboutViewController.m
//  DentaBook
//
//  Created by Mac Mini on 17.02.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "AboutViewController.h"
#import "HttpUtil.h"
#import <MessageUI/MessageUI.h>
#import "UserAgreementViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize table;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"О программе";
    
    table.delegate = self;
    table.dataSource = self;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    /*UIView* uwv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
     UIView* uwv1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
     uwv1.backgroundColor = [UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f blue:243.0f/255.0f alpha:1];
     [uwv addSubview:uwv1];
     return uwv;*/
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if(indexPath.row == 3)
        return 0;
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"titleCell2"];
        titleCell.accessoryType = UITableViewCellAccessoryNone;
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    }
    
    switch (indexPath.row) {
        case 0:
            titleCell.textLabel.text = @"Версия";
            titleCell.detailTextLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:11];
            titleCell.detailTextLabel.textColor = [UIColor colorWithRed:155.0f/255.0 green:155.0f/255.0 blue:155.0f/255.0 alpha:1.0];
            break;
            
        case 1:
            titleCell.textLabel.text = @"Поддержка";
            titleCell.detailTextLabel.text = @"support@wnfx.ru";
            titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:11];
            titleCell.detailTextLabel.textColor = [UIColor colorWithRed:155.0f/255.0 green:155.0f/255.0 blue:155.0f/255.0 alpha:1.0];
            break;
            
        case 2:
            titleCell.textLabel.text = @"Пользовательское соглашение";
            break;
            
            
        default:
            break;
    }
    
    cell = titleCell;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0)
    {
        
    }
    else if(indexPath.row == 1)
    {
        NSString *emailTitle = [NSString stringWithFormat:@"Dr. Smart v. %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        NSString *messageBody = @"";
        NSArray *toRecipents = [[NSArray alloc] initWithObjects:@"support@wnfx.ru", nil];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        if([MFMailComposeViewController canSendMail])
        {
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTintColor:getColorSchemeBar()];
            
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
    }
    else if(indexPath.row == 2)
    {
        UserAgreementViewController *servicesViewController = [[UserAgreementViewController alloc] init];
        [self.navigationController pushViewController:servicesViewController animated:YES];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
