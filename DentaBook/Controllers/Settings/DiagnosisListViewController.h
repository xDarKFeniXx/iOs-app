//
//  DiagnosisListViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kDataContentDiagnosis = 1,
    kDataContentIllness
}SettingsDataContentMode;

@interface DiagnosisListViewController : UIViewController

-(id) initWithContentMode:(SettingsDataContentMode)mode;
@property (nonatomic, retain) NSArray* diagnoses;

@end
