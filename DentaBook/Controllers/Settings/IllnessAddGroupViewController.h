//
//  IllnessAddGroupViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 12.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IllnessAddGroupViewController : UIViewController

@property (copy, nonatomic) void (^didClickCreateName)(NSString *nameString);

@end
