//
//  IllnessAddGroupViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 12.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "IllnessAddGroupViewController.h"
#import <MagicalRecord/MagicalRecord.h>

#import "DentalDiagnosis.h"
#import "HttpUtil.h"

@interface IllnessAddGroupViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryTitleTopConstraints;
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *extendTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@end

@implementation IllnessAddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Добавить заболевание";
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onSave:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Сохранить" forState:UIControlStateNormal];
    [button setTitle:@"Сохранить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated   {
    [self.categoryTitleLabel becomeFirstResponder];
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0)
        return;
    
    if(self.didClickCreateName) {
        self.didClickCreateName(self.categoryTitleLabel.text);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
