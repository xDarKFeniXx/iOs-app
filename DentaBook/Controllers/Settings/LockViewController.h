//
//  LockViewController.h
//  DentaBook
//
//  Created by Mac Mini on 17.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LockViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISwitch *lockScreenSwitcher;
@property (weak, nonatomic) IBOutlet UITextField *pinField;

@end
