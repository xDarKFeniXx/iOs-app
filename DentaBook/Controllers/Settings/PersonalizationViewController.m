//
//  PersonalizationViewController.m
//  DentaBook
//
//  Created by Mac Mini on 17.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "PersonalizationViewController.h"
#import "HTTPUtil.h"


@interface PersonalizationViewController ()

@end

@implementation PersonalizationViewController
@synthesize showFioSwitcher, divideSumSwitcher, countStrsControl, schemeControl, blueGrayLabel, blueWhiteLabel, greenGrayLabel, greenWhiteLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.title = @"Персонализация";
    // Do any additional setup after loading the view from its nib.
}


-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    UIColor* color = getColorScheme();
    showFioSwitcher.onTintColor = color;
    divideSumSwitcher.onTintColor = color;
    countStrsControl.tintColor = color;
    schemeControl.tintColor = color;
    
    schemeControl.selectedSegmentIndex = getColorSchemeInt();
    
    if(getColorSchemeInt()==0)
    {
        blueGrayLabel.hidden = YES;
        blueWhiteLabel.hidden = NO;
        greenGrayLabel.hidden = NO;
        greenWhiteLabel.hidden = YES;
    }
    else
    {
        blueGrayLabel.hidden = NO;
        blueWhiteLabel.hidden = YES;
        greenGrayLabel.hidden = YES;
        greenWhiteLabel.hidden = NO;
    }
    
    countStrsControl.selectedSegmentIndex = getRowsCount();
    showFioSwitcher.on = getNeedName();
    divideSumSwitcher.on = getNeedSumSplit();
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSchemeChanged:(id)sender
{
    setColorScheme((int)schemeControl.selectedSegmentIndex);
    UIColor* color = getColorScheme();
    showFioSwitcher.onTintColor = color;
    divideSumSwitcher.onTintColor = color;
    countStrsControl.tintColor = color;
    schemeControl.tintColor = color;
    
    if((int)schemeControl.selectedSegmentIndex==0)
    {
        blueGrayLabel.hidden = YES;
        blueWhiteLabel.hidden = NO;
        greenGrayLabel.hidden = NO;
        greenWhiteLabel.hidden = YES;
    }
    else
    {
        blueGrayLabel.hidden = NO;
        blueWhiteLabel.hidden = YES;
        greenGrayLabel.hidden = YES;
        greenWhiteLabel.hidden = NO;
    }
    
    
    [[UINavigationBar appearance] setBarTintColor: [UIColor redColor]];
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"schemeChanged" object:nil];
}

- (IBAction)onRowsChanged:(id)sender
{
    setRowsCount((int)countStrsControl.selectedSegmentIndex);
}

- (IBAction)onNameChanged:(id)sender
{
    setNeedName(showFioSwitcher.on);
}

- (IBAction)onSumChanged:(id)sender
{
    setNeedSumSplit(divideSumSwitcher.on);
}
@end
