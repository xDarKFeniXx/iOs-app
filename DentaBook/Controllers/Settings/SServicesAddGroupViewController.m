//
//  AffairAddGroupViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "SServicesAddGroupViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "CategoryTherapy+Custom.h"
#import "TherapyManager.h"
#import "HttpUtil.h"

@interface SServicesAddGroupViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@end

@implementation SServicesAddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Добавить группу";
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0)
        return;
    
//    CategoryTherapy *categoryTherapy = [CategoryTherapy MR_createEntity];
//    categoryTherapy.remoteId = @([CategoryTherapy MR_findAll].count + 1);
//    categoryTherapy.name = self.categoryTitleLabel.text;
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Сохранение";
    [HUD showInView:self.view];
    [[TherapyManager shared] createTherapyCategoryByTitle:self.categoryTitleLabel.text success:^{
        [HUD dismissAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^{
        [HUD dismissAnimated:YES];
    }];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
