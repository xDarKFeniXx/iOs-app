//
//  AffairAddGroupViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "SServicesAddItemViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <JGProgressHUD/JGProgressHUD.h>

#import "CategoryTherapy+Custom.h"
#import "Service+Custom.h"
#import "TherapyManager.h"
#import "DataBase.h"
#import "HttpUtil.h"

@interface SServicesAddItemViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *categoryCostLabel;
@property (nonatomic, strong) NSDictionary *categoryTherapy;
@property (nonatomic, strong) NSMutableDictionary *currentItem;
@end

@implementation SServicesAddItemViewController

-(id) initWithCategory:(NSDictionary*)category {
    self = [super initWithNibName:@"SServicesAddItemViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.categoryTherapy = category;
        self.currentItem = nil;
    }
    return self;
}

-(id) initWithItem:(NSDictionary*)category {
    self = [super initWithNibName:@"SServicesAddItemViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentItem = [NSMutableDictionary dictionaryWithDictionary:category];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.title = @"Добавить позицию";
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    if(self.currentItem)
    {
        self.categoryTitleLabel.text = self.currentItem[@"title"];
        self.categoryCostLabel.text = [NSString stringWithFormat:@"%@",self.currentItem[@"cost"]];
    }
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSave:(id)sender {
    if(self.categoryTitleLabel.text.length == 0 || self.categoryCostLabel.text.length == 0)
        return;
    if(self.currentItem)
    {
        self.currentItem[@"title"] = self.categoryTitleLabel.text;
        self.currentItem[@"cost"] = [NSNumber numberWithDouble:[self.categoryCostLabel.text doubleValue]];
        [DataBase putDB:self.currentItem toTable:@"services" withKey:@"id"];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [[TherapyManager shared] createTherapyServiceByTitle:self.categoryTitleLabel.text
                                                        cost:[self.categoryCostLabel.text doubleValue]
                                              linkToRemoteId:self.categoryTherapy[@"id"] success:^{
                                                  [self.navigationController popViewControllerAnimated:YES];
                                              } failure:^{
                                              }];
    }
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
