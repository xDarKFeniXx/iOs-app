//
//  SServicesViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 08.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "SServicesListViewController.h"
#import "SServicesAddItemViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import <SWTableViewCell/SWTableViewCell.h>

#import "CategoryTherapy+Custom.h"
#import "Service+Custom.h"
#import "TherapyManager.h"

#import "DataBase.h"
#import "PercUtils.h"
#import "CZPicker.h"
#import "HttpUtil.h"

@interface SServicesListViewController ()<NSFetchedResultsControllerDelegate, SWTableViewCellDelegate, CZPickerViewDataSource, CZPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSDictionary *categoryTherapy;
@end

@implementation SServicesListViewController
@synthesize services;

-(id) initWithCategory:(NSDictionary*)category {
    self = [super initWithNibName:@"SServicesListViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.categoryTherapy = category;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Добавить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(onAdd:)];*/
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Добавить" forState:UIControlStateNormal];
    [button setTitle:@"Добавить" forState:UIControlStateSelected];
    button.frame = CGRectMake(0, 0, 65, 18);
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;

    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.title = @"Прайс-лист";
    
    [self fetchTableData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self.tableView reloadData];
}

-(void)onAdd:(id)sender
{
    SServicesAddItemViewController *servicesAddItemVC = [[SServicesAddItemViewController alloc] initWithCategory:self.categoryTherapy];
    [self.navigationController pushViewController:servicesAddItemVC animated:YES];
}

-(void)fetchTableData {
    services=[DataBase getDBArray:@"services" withKey:@"category_id" andValue:self.categoryTherapy[@"id"]];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSInteger count = [self.fetchedResultsController.fetchedObjects count];
    return services.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSDictionary *serviceTherapy = [services objectAtIndex:indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    SWTableViewCell *titleCell = (SWTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"titleCell2"];
    if (titleCell == nil) {
        titleCell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"titleCell2"];
        titleCell.delegate = self;
        titleCell.rightUtilityButtons = [self rightButtons];
        titleCell.textLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        titleCell.detailTextLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
        titleCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        
        [titleCell.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.top.equalTo(3);
            make.right.equalTo(titleCell.contentView).with.offset(-100);
            make.height.equalTo(40);
        }];
        
        [titleCell.detailTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleCell.textLabel.right);
            make.top.equalTo(3);
            make.right.equalTo(titleCell.contentView).with.offset(-15);
            make.height.equalTo(40);
        }];
    }
    titleCell.textLabel.text = serviceTherapy[@"title"];
    NSString *theString = [numberFormatter stringFromNumber:serviceTherapy[@"cost"]];
    titleCell.detailTextLabel.text = [NSString stringWithFormat:@"%@ ₽",  theString];
    
    cell = titleCell;
    
    return cell;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"pencil_white.png"] scaledToSize:CGSizeMake(10, 25)] ];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Отмена" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.tag = [self.tableView indexPathForCell:cell].row;
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            [picker show];
        }
            break;
        case 1: {
            NSDictionary *serviceTherapy = [services objectAtIndex:[self.tableView indexPathForCell:cell].row];
            SServicesAddItemViewController *servicesAddItemVC = [[SServicesAddItemViewController alloc] initWithItem:serviceTherapy];
            [self.navigationController pushViewController:servicesAddItemVC animated:YES];
        }
            break;
        default:
            break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
    /*DentalDiagnosis *denta = [DentalDiagnosis MR_findAllSortedBy:@"remoteId" ascending:YES][row];
     return denta.name;*/
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSDictionary *categoryTherapy = services[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[TherapyManager shared] deleteTherapyCategoryById:categoryTherapy[@"id"] success:^{
        [self fetchTableData];
        [self.tableView reloadData];
        //
    } failure:^{
        //
    }];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {

    NSDictionary *serviceTherapy = services[pickerView.tag];//[self.fetchedResultsController objectAtIndexPath:cellIndexPath];
    [[TherapyManager shared] deleteTherapyServiceById:serviceTherapy[@"id"] success:^{
        //
        [self fetchTableData];
        [self.tableView reloadData];
    } failure:^{
        //
    }];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}


@end
