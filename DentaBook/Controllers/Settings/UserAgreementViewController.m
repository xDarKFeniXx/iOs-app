//
//  UserAgreementViewController.m
//  DentaBook
//
//  Created by Mac Mini on 17.02.17.
//  Copyright © 2017 denisdbv. All rights reserved.
//

#import "UserAgreementViewController.h"
#import "HttpUtil.h"

@interface UserAgreementViewController ()

@end

@implementation UserAgreementViewController
@synthesize webView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Условия использования";
    
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"terms_of_use" withExtension:@"rtf"];
    
    NSError *error = nil;
    NSAttributedString *string = [[NSAttributedString alloc] initWithFileURL:url
                                                                     options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType}
                                                          documentAttributes:NULL
                                                                       error:&error];
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    webView.attributedText = string;
    // Do any additional setup after loading the view from its nib.
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
