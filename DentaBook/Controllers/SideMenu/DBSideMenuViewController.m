//
//  DBSideMenuViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 04.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBSideMenuViewController.h"
#import "DBLeftMenuViewController.h"

#import "SKSplashView.h"
#import "SKSplashIcon.h"
#import "HttpUtil.h"

@interface DBSideMenuViewController () <SKSplashDelegate>
@property (strong, nonatomic) DBLeftMenuViewController *leftMenuViewController;
@property (strong, nonatomic) SKSplashView *splashView;
@end

@implementation DBSideMenuViewController

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self)
    {
        self.view.backgroundColor = [UIColor whiteColor];
        
        _leftMenuViewController = [DBLeftMenuViewController new];
        
        [self setLeftViewEnabledWithWidth:250.f
                        presentationStyle:LGSideMenuPresentationStyleScaleFromBig
                     alwaysVisibleOptions:0];
        
        self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
        [self.leftView addSubview:_leftMenuViewController.view];
        
        /*SKSplashIcon *twitterSplashIcon = [[SKSplashIcon alloc] initWithImage:[UIImage imageNamed:@"splash-tooth-image"] animationType:SKIconAnimationTypeBounce];
        _splashView = [[SKSplashView alloc] initWithSplashIcon:twitterSplashIcon backgroundImage:[UIImage imageNamed:@"background"] animationType:SKSplashAnimationTypeNone];
        _splashView.delegate = self;
        _splashView.animationDuration = 1;
        twitterSplashIcon.center = _splashView.center;
        [self.view addSubview:_splashView];
        [_splashView startAnimation];*/
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeScheme) name:@"schemeChanged" object:nil];
    }
    
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

- (void) viewDidAppear:(BOOL)animated
{
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

- (void) didChangeScheme
{
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super leftViewWillLayoutSubviewsWithSize:size];
    
    _leftMenuViewController.view.frame = CGRectMake(0.f , 0.f, size.width, size.height);
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

#pragma mark - Delegate methods

- (void) splashView:(SKSplashView *)splashView didBeginAnimatingWithDuration:(float)duration
{
    NSLog(@"Started animating from delegate");
    //To start activity animation when splash animation starts
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

- (void) splashViewDidEndAnimating:(SKSplashView *)splashView
{
    NSLog(@"Stopped animating from delegate");
    //To stop activity animation when splash animation ends
    self.leftViewBackgroundImage = [UIImage imageNamed:getColorSchemeImg()];
}

@end
