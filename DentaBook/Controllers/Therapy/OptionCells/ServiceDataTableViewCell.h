//
//  ServiceDataTableViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDataTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveButton;

-(void) setDetailMode:(NSString*)leftText rightText:(NSString*)rightText;
-(void) setButtonMode;

@end
