//
//  TherapyEditViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Therapy;

@interface TherapyEditViewController : UIViewController

-(id) initWithTherapy:(NSMutableDictionary*)therapy;
@property (nonatomic, retain) NSMutableArray* sections;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@end
