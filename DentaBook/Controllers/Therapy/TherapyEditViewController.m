//
//  TherapyEditViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyEditViewController.h"
#import "UIView+Helpers.h"
#import "ChoosedServiceViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy.h"
#import "TherapyItem.h"
#import "DataBase.h"

#import "HttpUtil.h"

@interface TherapyEditViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *currentTherapy;

@property (strong, nonatomic) IBOutlet UITableView *optionTableView;

@property (strong, nonatomic) IBOutlet UIView *optionInfoBoxView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;

@end

@implementation TherapyEditViewController
@synthesize sections,discountLabel;
-(id) initWithTherapy:(NSMutableDictionary*)therapy {
    self = [super initWithNibName:@"TherapyEditViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentTherapy = therapy;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Редактировать";
    
    [self.optionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ChoosedServiceViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.optionInfoBoxView.top = 0;
    self.optionInfoBoxView.left = 0;
    self.optionInfoBoxView.width = screenRect.size.width;
    //[self.infoCell.contentView addSubview:self.optionInfoBoxView];
    
    [self.optionTableView setEditing:YES animated:YES];
    [self fetchTableData];
    [self updateBuyInfo];
    [self.optionTableView reloadData];
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)fetchTableData {
    /*_fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];*/
    NSArray* tmp = self.currentTherapy[@"items"];//[DataBase getDBArray:@"photo" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]];
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        
        //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"categoryid"]];
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"categoryid"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }

}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TherapyItem"];
    [fetchRequest setFetchBatchSize:20];
    
    // Predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy];
    [fetchRequest setPredicate:predicate];
    
    // Sort
    NSSortDescriptor* dateSortDescripror;
    dateSortDescripror = [NSSortDescriptor sortDescriptorWithKey:@"serviceId"
                                                       ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:dateSortDescripror, nil]];
    
    NSFetchedResultsController *theFetchedResultsController;
    theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                      managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                        sectionNameKeyPath:@"categoryId"
                                                                                 cacheName:nil];
    theFetchedResultsController.delegate = self;
    self.fetchedResultsController = theFetchedResultsController;
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.optionTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.optionTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.optionTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationBottom];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.optionTableView endUpdates];
    
    [self updateBuyInfo];
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSInteger count = //[[self.fetchedResultsController sections] count];
    return sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    /*id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger count2 = [sectionInfo numberOfObjects];*/
    return ((NSArray*)sections[section][@"content"]).count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section    {
    
    /*id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    TherapyItem *buyRef = theSection.objects[0];
    CategoryTherapy *categoryRef = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:buyRef.categoryId];*/
    NSDictionary *categoryRef = (NSDictionary*)[DataBase getDB:@"services_group" withKey:@"id" andValue:sections[section][@"section"]];//[CategoryTherapy
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(20, 2, tableView.frame.size.width - 5, 18);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    headerLabel.text = categoryRef[@"title"];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    return 32.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*TherapyItem *buyRef = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];*/
    //NSArray *array = sections[indexPath.section][@"content"];
    NSDictionary *buyRef = sections[indexPath.section][@"content"][indexPath.row];
    NSArray* ser_arr = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]];
    NSDictionary *serviceRef = ser_arr==nil?nil:ser_arr[0];
    
    ChoosedServiceViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChoosedServiceViewCell class])];
    chooseCell.countTooth.text = @"";
    chooseCell.nameLabel.text = serviceRef[@"title"];
    chooseCell.countService.text = [NSString stringWithFormat:@"%ld", [buyRef[@"countt"] integerValue]];
    chooseCell.costService.text = [serviceRef[@"cost"] stringValue];
    chooseCell.indexPath = indexPath;
    
    chooseCell.didClickDelete = ^(NSIndexPath *indexPath)
    {
        /*self.toothCounter.indexPath = indexPath;
        self.toothCounterActionSheet = [[WLPActionSheet alloc] initWithTitle:@"Количество" cancelButtonTitle:@"ОК" destructiveButtonTitle:nil withViewController:self.toothCounter];
        self.toothCounterActionSheet.tag = 3;
        self.toothCounterActionSheet.delegate = self;
        [self.toothCounterActionSheet showInView:self.view];*/
        NSString* idi=((NSMutableArray*)sections[indexPath.section][@"content"])[indexPath.row][@"id"];
        id to_remove=nil;
        for(NSDictionary *d in self.currentTherapy[@"items"])
        {
            if([d[@"id"] isEqualToString:idi])
            {
                to_remove=d;
                break;
            }
        }
        
        if(to_remove!=nil)
        {
            [self.currentTherapy[@"items"] removeObject:to_remove];
            [self fetchTableData];
            [self updateBuyInfo];
            [self.optionTableView reloadData];
        }
        
    };

    
    return chooseCell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath    {
    NSInteger count = [[self.fetchedResultsController sections] count];
    if(indexPath.section != count)
        return UITableViewCellEditingStyleDelete;
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView
    commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
    forRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    TherapyItem *buyRef = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [buyRef MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void) updateBuyInfo  {
    /*NSInteger cost = 0;
    
    for(TherapyItem *buyRef in [TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]])   {
        Service *serviceRef = [Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef.cost integerValue] * [buyRef.count integerValue];
    }
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%d руб.", cost];
    self.sumWithDiscountLabel.text = [NSString stringWithFormat:@"%.2f руб.", cost - ((cost * 30.0)/100)];*/
    NSInteger cost = 0;
    
    for(NSDictionary *buyRef in self.currentTherapy[@"items"] /*[TherapyItem MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"therapy == %@", self.currentTherapy]]*/)
    {
        NSDictionary *serviceRef = [DataBase getDBArray:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]][0];//[Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
        cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
    }
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)cost];
    self.sumWithDiscountLabel.text = [NSString stringWithFormat:@"%.2ld руб.", cost - ((cost * [self.currentTherapy[@"discount"] integerValue])/100)];
    discountLabel.text = [NSString stringWithFormat:@"%@%%", self.currentTherapy[@"discount"]];
}

@end
