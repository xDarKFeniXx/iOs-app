//
//  TherapyViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 25.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@class Patient, Therapy;

@interface TherapyViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate, SWTableViewCellDelegate, UIScrollViewDelegate>

-(id) initWithPatient:(NSDictionary*)patient placeToPlan:(BOOL)isPlan andNumber:(int) counter;
-(id) initWithTherapy:(NSDictionary*)therapy andPatient:(NSDictionary*)patient;
-(id) initWithTherapies:(NSArray*)therapies andPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSMutableArray* sections;
@property (weak, nonatomic) IBOutlet UIView *extendedView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholder;
- (IBAction)tapApply:(id)sender;
- (IBAction)tabPriceList:(id)sender;
- (IBAction)tapEditPlanName:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *planNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *bigSaveBtn;
@property (weak, nonatomic) IBOutlet UIView *saveTherapyView;
@property (weak, nonatomic) IBOutlet UIView *applyTherapyView;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UIButton *extButton;
@property (strong, nonatomic) NSIndexPath* selectedRow;
- (IBAction)tapPay:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *secondVisitView;
@property (weak, nonatomic) IBOutlet UIView *firstVisitView;
@property (weak, nonatomic) IBOutlet UIView *undergroundView;
@property (nonatomic, retain) UIBarButtonItem* edit_btn;
- (IBAction)tapExport:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView1;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholder1;
@property (weak, nonatomic) IBOutlet UIView *delimeterFirstVisitView;
@property (weak, nonatomic) IBOutlet UIView *extendedDelimeterView;
@property (weak, nonatomic) IBOutlet UIView *showPriceBtn;
///////////// converter data
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *fioLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *licenseLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *patientLabel;
///
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalWithDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *clinicLabel;
@property (weak, nonatomic) IBOutlet UIView *tableHeadView;
@property (weak, nonatomic) IBOutlet UIButton *changeNameButton;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIView *planNameView;

@property (weak, nonatomic) IBOutlet UILabel *discountLabelLabel;
@property (nonatomic) int convertMode;


@end
