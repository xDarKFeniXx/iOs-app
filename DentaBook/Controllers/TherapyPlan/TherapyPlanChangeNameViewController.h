//
//  TherapyPlanChangeNameViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Therapy;

@interface TherapyPlanChangeNameViewController : UIViewController

-(id) initWithTherapy:(Therapy*)therapy;

@end
