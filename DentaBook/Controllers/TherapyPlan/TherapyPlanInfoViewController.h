//
//  TherapyPlanInfoViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Therapy;

@interface TherapyPlanInfoViewController : UIViewController

-(id) initWithTherapy:(NSDictionary*)therapy withAutoName:(NSString*)autoName;
@property (nonatomic, retain) NSMutableArray* sections;

@end
