//
//  TherapyPlanViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>
#import "SWTableViewCell.h"

@interface TherapyPlanViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;

@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftCheckButtonConstraints;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (copy, nonatomic) void (^didChoosePlan)(NSIndexPath *indexPath, BOOL selected);
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *frontView;

@end
