//
//  TherapyPlanViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyPlanViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TherapyPlanViewCell
@synthesize shadowView, frontView;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];

    frontView.layer.shadowColor = [UIColor blackColor].CGColor;
    frontView.layer.shadowOpacity = 0.5;
    frontView.layer.shadowRadius = 2;
    frontView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    frontView.layer.cornerRadius = 2;
    frontView.layer.masksToBounds = NO;
}

- (IBAction)onCheck:(UIButton*)sender {
    
    //sender.selected = !sender.selected;
    
    if(self.didChoosePlan)  {
        self.didChoosePlan(self.indexPath, !sender.selected);
    }
}

- (IBAction)onCheckHolder:(id)sender {
    [self.checkButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

@end
