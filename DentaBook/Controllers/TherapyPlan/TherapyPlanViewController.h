//
//  TherapyPlanViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface TherapyPlanViewController : UIViewController

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSMutableArray* therapies;
@property (nonatomic, retain) NSMutableArray* sections;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *patientLabel;
@property (retain, nonatomic) UIBarButtonItem *add_button;
@property (retain, nonatomic) UIBarButtonItem *convert_button;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@end
