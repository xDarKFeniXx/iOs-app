//
//  TherapyPlanViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TherapyPlanViewController.h"
#import "TherapyPlanViewCell.h"
#import "TherapyViewController.h"
#import "TherapyPlanInfoViewController.h"

#import <MagicalRecord/MagicalRecord.h>

#import "Patient.h"
#import "CategoryTherapy.h"
#import "Service.h"
#import "Therapy+Custom.h"
#import "TherapyItem.h"
#import "DataBase.h"
#import "SWTableViewCell.h"
#import "PercUtils.h"
#import "HttpUtil.h"
#import "CZPicker.h"
#import "ShareViewController.h"
#import "UIViewController+PresentedOver.h"
#import <MessageUI/MessageUI.h>
#import "UIView+Toast.h"

@interface TherapyPlanViewController () <SWTableViewCellDelegate, CZPickerViewDelegate, CZPickerViewDataSource>
@property (nonatomic, strong) NSDictionary *currentPatient;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstaraints;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *sumWithoutDiscountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumWithDiscountLabel;

@end

@implementation TherapyPlanViewController   {
    NSMutableArray *calculateArray;
    BOOL has_selected;
    BOOL need_recalc;
}

@synthesize therapies, sections, illnessImg, patientLabel, add_button, convert_button, footerView;
-(id) initWithPatient:(NSDictionary*)patient {
    self = [super initWithNibName:@"TherapyPlanViewController" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.currentPatient = patient;
        therapies = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
        NSLog(@"Count therapies: %lu", (unsigned long)therapies.count);
        need_recalc=YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont boldSystemFontOfSize: 14.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    self.title=@"Планы лечения";
    
    if(((NSArray*)self.currentPatient[@"illness"]).count>0)
    {
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image =  [PercUtils imageWithImage:[UIImage imageNamed:@"patient-illness-icon.png"] scaledToSize:CGSizeMake(15, 15)];
        attachment.bounds = CGRectMake(0, -3, 18, 18);
        NSMutableAttributedString *attachmentString = [[NSAttributedString attributedStringWithAttachment:attachment] mutableCopy];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]]];
        [attachmentString appendAttributedString:myString];
        
        patientLabel.attributedText = attachmentString;
    }
    else
    {
        patientLabel.text = [NSString stringWithFormat:@"  %@ %@ %@", self.currentPatient[@"lastname"], self.currentPatient[@"firstname"], self.currentPatient[@"middlename"]];
    }
    
    UIImage *image2 = [UIImage imageNamed:@"back-button"];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(onBack:)forControlEvents:UIControlEventTouchUpInside];
    [button2 setBackgroundImage:image2 forState:UIControlStateNormal];
    button2.frame = CGRectMake(0, 0, 11, 18);
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.leftBarButtonItem = barButton2;
    
    UIImage *image = [UIImage imageNamed:@"patient-add-icon"];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(onAdd:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 18, 18);
    add_button = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = add_button;
    
    UIImage *image1 = [UIImage imageNamed:@"therapy-save-icon-white"];
    UIButton* button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 addTarget:self action:@selector(tapExport:)forControlEvents:UIControlEventTouchUpInside];
    [button1 setBackgroundImage:image1 forState:UIControlStateNormal];
    button1.frame = CGRectMake(0, 0, 18, 18);
    convert_button = [[UIBarButtonItem alloc] initWithCustomView:button1];

    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TherapyPlanViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([TherapyPlanViewCell class])];
    
    [self fetchTableData];
    
    calculateArray = [NSMutableArray new];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]&&getNeedName())
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!getNeedName())
    {
        CGRect tempRect;
        for(UIView *sub in [[self view] subviews])
        {
            tempRect = [sub frame];
            if([sub isKindOfClass:[UITableView class]])
            {
                tempRect.origin.y += 20.0f; //Height of status bar
                tempRect.size.height -= 20.0f; //Height of status bar
                [sub setFrame:tempRect];
            }
        }
    }
    
    self.navigationController.navigationBar.barTintColor = getColorSchemeBar();
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = getColorSchemeStatus();
    }
    
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPrintConvert) name:@"needPrintMultiple" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onConvertConvert) name:@"needConvertMultiple" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEmailConvert) name:@"needEmailMultiple" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSwitch:) name:@"imageReady" object:nil];
}


- (IBAction)tapExport:(id)sender {
    ShareViewController *editTherapyViewController = [[ShareViewController alloc] init];
    editTherapyViewController.multiple = YES;
    
    [self presentTransparentViewController:editTherapyViewController animated:NO completion:nil];
}


-(void) viewWillAppear:(BOOL)animated
{
    [self fetchTableData];
    [self updateBuyInfo];
    [self.tableView reloadData];
    [self checkNeedShowConvert];
}

-(void) viewDidAppear:(BOOL)animated
{
    [self fetchTableData];
    [self updateBuyInfo];
    [self.tableView reloadData];
    [self checkNeedShowConvert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onAdd:(id)sender
{
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithPatient:self.currentPatient placeToPlan:YES andNumber:(int)therapies.count];
    [self.navigationController pushViewController:therapyViewController animated:YES];
}

-(void)fetchTableData
{
    therapies = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSMutableArray* tmp_sections = [[NSMutableArray alloc] init];
    
    for(NSDictionary* pat in tmp)
    {
        
        //nmd[@"index"] = [[(NSString*)pat[@"firstname"] substringWithRange:NSMakeRange(0, 1)] uppercaseString];
        [tmp_sections addObject:pat[@"createat"]];
        if([pat[@"selected"] isEqualToString:@"true"])
        {
            has_selected=YES;
            //break;
        }
    }
    
    sections = [[NSMutableArray alloc] init];
    for(NSString* hd in [tmp_sections valueForKeyPath:@"@distinctUnionOfObjects.self"])
    {
        NSMutableDictionary* ndms = [[NSMutableDictionary alloc] init];
        ndms[@"section"] = hd;
        NSMutableArray* mna=[[NSMutableArray alloc] init];
        for(NSDictionary* dict in tmp)
        {
            if([[(NSString*)dict[@"createat"] uppercaseString] isEqualToString:[hd uppercaseString]])
            {
                [mna addObject:dict];
            }
        }
        ndms[@"content"]=mna;
        [sections addObject:ndms];
    }
    
}

#pragma mark -
#pragma mark Option Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray*)sections[section][@"content"]).count;//count2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 25)];
    
    headerView.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy"];
    NSString *dateString = sections[section][@"section"];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(10, 0, tableView.frame.size.width-20, 25);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    headerLabel.text = dateString;
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:headerLabel];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section    {
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    headerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *seperateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seperate-cell"]];
    [headerView addSubview:seperateImageView];
    seperateImageView.center = headerView.center;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section   {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section   {
    return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *therapyEntity = sections[indexPath.section][@"content"][indexPath.row];
    
    TherapyPlanViewCell *chooseCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TherapyPlanViewCell class])];
    chooseCell.delegate = self;
    chooseCell.rightUtilityButtons = [self rightButtons];
    
    if([(NSString*)therapyEntity[@"name"] length] == 0)
        chooseCell.descriptionLabel.text = [NSString stringWithFormat:@"План лечения №%ld", indexPath.row+1];
    else
        chooseCell.descriptionLabel.text = therapyEntity[@"name"];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:@([therapyEntity[@"total_cost_with_discount"] floatValue])];
    
    chooseCell.costLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    chooseCell.indexPath = indexPath;
    
    chooseCell.checkButton.selected = [therapyEntity[@"selected"] isEqualToString:@"true"];
    
    if([therapyEntity[@"selected"] isEqualToString:@"true"])
        [self addIndexToCalculate:indexPath];
    
    chooseCell.didChoosePlan = ^(NSIndexPath *indexPath, BOOL selected)
    {
        if(selected)    {
            
            NSMutableDictionary *therapyEntity = [NSMutableDictionary dictionaryWithDictionary:sections[indexPath.section][@"content"][indexPath.row]];
            therapyEntity[@"selected"]=@"true";
            [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
            [self addIndexToCalculate:indexPath];
            [self fetchTableData];
            [self.tableView reloadData];
        } else  {
            if(has_selected&&[therapyEntity[@"selected"] isEqualToString:@"true"]&&need_recalc)
            {
                CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Отменить выбор?" cancelButtonTitle:@"Нет" confirmButtonTitle:@"Да"];
                picker.headerBackgroundColor = getColorScheme();
                picker.delegate = self;
                picker.dataSource = self;
                picker.allowMultipleSelection = YES;
                picker.tag=1;
                picker.selectedCell = indexPath;
                [picker show];
            }
            else
            {
                
                NSMutableDictionary* therapyEntity = [NSMutableDictionary dictionaryWithDictionary:sections[indexPath.section][@"content"][indexPath.row]];
                therapyEntity[@"selected"]=@"false";
                [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
                [self removeIndexToCalculate:indexPath];
                [self fetchTableData];
                [self.tableView reloadData];
            }
        }
        
        [self checkNeedShowConvert];
    };
    
    return chooseCell;
}

- (void) checkNeedShowConvert
{
    BOOL tmp_has_selected=NO;
    
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    for(NSDictionary* pat in tmp)
    {
        if([pat[@"selected"] isEqualToString:@"true"])
        {
            tmp_has_selected=YES;
            break;
        }
    }
    
    if(tmp_has_selected)
    {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItems = @[add_button, convert_button];
    }
    else
    {
        self.navigationItem.rightBarButtonItems = nil;
        self.navigationItem.rightBarButtonItem = add_button;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [calculateArray removeAllObjects];
    NSDictionary *therapyEntity = sections[indexPath.section][@"content"][indexPath.row];//[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapy:therapyEntity andPatient:self.currentPatient];
    
    [self.navigationController pushViewController:therapyViewController animated:YES];
}

-(BOOL) isIndexHas:(NSIndexPath*)indexPath {
    BOOL isHas = NO;
    for(NSIndexPath *inIndexPath in calculateArray) {
        if([inIndexPath compare:indexPath] == NSOrderedSame)    {
            isHas = YES;
            break;
        }
    }
    
    return isHas;
}

-(void) addIndexToCalculate:(NSIndexPath*)indexPath {
    
    BOOL isNew = YES;
    for(NSIndexPath *inIndexPath in calculateArray) {
        if([inIndexPath compare:indexPath] == NSOrderedSame)    {
            isNew = NO;
            break;
        }
    }
    
    if(isNew)   {
        [calculateArray addObject:indexPath];
    }
    
    [self updateBuyInfo];
}

-(void) removeIndexToCalculate:(NSIndexPath*)indexPath {
    
    BOOL isHas = NO;
    for(NSIndexPath *inIndexPath in calculateArray) {
        if([inIndexPath compare:indexPath] == NSOrderedSame)    {
            isHas = YES;
            break;
        }
    }
    
    if(isHas)   {
        [calculateArray removeObject:indexPath];
    }
    
    [self updateBuyInfo];
}

-(void) updateBuyInfo
{
    /*if(calculateArray.count > 0)
    {
        self.bottomTableViewConstaraints.constant = 74.;*/
        
    NSInteger costWithoutDiscount = 0;
    CGFloat costWithDiscount = 0;
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    
    for(NSDictionary *therapyEntity in tmp)
    {
        
        NSInteger cost = 0;
        if([therapyEntity[@"selected"] isEqualToString:@"true"])
        {
            for(NSDictionary *buyRef in therapyEntity[@"items"])
            {
                
                NSDictionary *serviceRef = (NSDictionary*)[DataBase getDB:@"services" withKey:@"id" andValue:buyRef[@"serviceid"]];//[Service MR_findFirstByAttribute:@"remoteId" withValue:buyRef.serviceId];
                cost += [serviceRef[@"cost"] integerValue] * [buyRef[@"countt"] integerValue];
            }
            
            costWithoutDiscount += cost;
            costWithDiscount += cost - ((cost * [therapyEntity[@"discount"] floatValue])/100);
        }
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setDecimalSeparator:@"."];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    
    NSString *theString = [numberFormatter stringFromNumber:@(costWithoutDiscount)];
    
    self.sumWithoutDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    
    theString = [numberFormatter stringFromNumber:@(costWithDiscount)];
    self.sumWithDiscountLabel.text = [NSString stringWithFormat:@"%@ ₽", theString];
    
    /*} else  {
        self.bottomTableViewConstaraints.constant = 0.;
    }*/
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];

    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:231.0f/255.0f green:76.0f/255.0f blue:60.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"delete_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:20.0f/255.0f green:162.0f/255.0f blue:243.0f/255.0f alpha:1.0f] icon:[PercUtils imageWithImage:[UIImage imageNamed:@"copy_white.png"] scaledToSize:CGSizeMake(20, 25)]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell   {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0: {
            CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Удалить?" cancelButtonTitle:@"Нет" confirmButtonTitle:@"Да"];
            picker.headerBackgroundColor = getColorScheme();
            picker.delegate = self;
            picker.dataSource = self;
            picker.allowMultipleSelection = YES;
            picker.tag=2;
            picker.selectedCell = [self.tableView indexPathForCell:cell];
            [picker show];
        }
            break;
        case 1:
        {
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            NSMutableDictionary *therapyEntity = [NSMutableDictionary dictionaryWithDictionary:sections[cellIndexPath.section][@"content"][cellIndexPath.row]];
            therapyEntity[@"name"]=[NSString stringWithFormat:@"%@ копия", therapyEntity[@"name"]];
            therapyEntity[@"id"] = GUIDString();
            [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
            [self fetchTableData];
            [self.tableView reloadData];
        }

        default:
            break;
    }
}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row
{
    return @"Да";
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return 0;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    if(pickerView.tag==1)
    {
        [self removeIndexToCalculate:pickerView.selectedCell];
        need_recalc=NO;
        NSMutableDictionary *therapyEntity = [NSMutableDictionary dictionaryWithDictionary:sections[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row]];
        therapyEntity[@"selected"]=@"false";
        [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
        [self fetchTableData];
        [self.tableView reloadData];
    }
    else
    {
        NSDictionary *therapyEntity = sections[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row];
        [DataBase killDB:@"patient_therapy" withKey:@"id" andValue:therapyEntity[@"id"]];
        [self fetchTableData];
        [self.tableView reloadData];
    }
    
    [self checkNeedShowConvert];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows   {
    
    if(pickerView.tag==1)
    {
        [self removeIndexToCalculate:pickerView.selectedCell];
        need_recalc=NO;
        NSMutableDictionary *therapyEntity = [NSMutableDictionary dictionaryWithDictionary:sections[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row]];
        therapyEntity[@"selected"]=@"false";
        [DataBase putDB:therapyEntity toTable:@"patient_therapy" withKey:@"id"];
        [self fetchTableData];
        [self.tableView reloadData];
    }
    else
    {
        NSDictionary *therapyEntity = sections[pickerView.selectedCell.section][@"content"][pickerView.selectedCell.row];
        [DataBase killDB:@"patient_therapy" withKey:@"id" andValue:therapyEntity[@"id"]];
        [self fetchTableData];
        [self.tableView reloadData];
    }
    
    [self checkNeedShowConvert];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    NSLog(@"Canceled.");
}

//////////////////////////////////////////////////////////////////////////////////////
// DISPATCH METHODS
//////////////////////////////////////////////////////////////////////////////////////
- (void) onPrintConvert
{
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSMutableArray* to_convert = [[NSMutableArray alloc] init];
    
    for(NSDictionary* p in tmp)
    {
        if([p[@"selected"] isEqualToString:@"true"])
        {
            [to_convert addObject:p];
        }
    }
    
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapies:to_convert andPatient:self.currentPatient];
    therapyViewController.convertMode = 1;
    [self.navigationController pushViewController:therapyViewController animated:NO];
}

- (void) onConvertConvert
{
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSMutableArray* to_convert = [[NSMutableArray alloc] init];
    
    for(NSDictionary* p in tmp)
    {
        if([p[@"selected"] isEqualToString:@"true"])
        {
            [to_convert addObject:p];
        }
    }
    
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapies:to_convert andPatient:self.currentPatient];
    therapyViewController.convertMode = 0;
    [self.navigationController pushViewController:therapyViewController animated:NO];
}

- (void) onEmailConvert
{
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSMutableArray* to_convert = [[NSMutableArray alloc] init];
    
    for(NSDictionary* p in tmp)
    {
        if([p[@"selected"] isEqualToString:@"true"])
        {
            [to_convert addObject:p];
        }
    }
    
    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapies:to_convert andPatient:self.currentPatient];
    therapyViewController.convertMode = 2;
    [self.navigationController pushViewController:therapyViewController animated:NO];
}


- (void) onSwitch:(NSNotification*) dict
{
    if([dict.object[@"mode"] floatValue] == 1)
    {
        [self printItem:(UIImage*) dict.object[@"img"]];
    }
    
    if([dict.object[@"mode"] floatValue] == 2)
    {
        [self onEmail:(UIImage*) dict.object[@"img"]];
    }
    
    if([dict.object[@"mode"] floatValue] == 0)
    {
        [self onConvert:(UIImage*) dict.object[@"img"]];
    }
}
//////////////////////////////////////////////////////////////////////////////////////
/*- (UIImage *)captureView:(UIView*) view {
    
    //hide controls if needed
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width, view.frame.size.height));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

- (UIImage *)joinImages:(UIImage *)im1 secondImage:(UIImage *)im2
{
    //Joins 3 UIImages together, stitching them vertically
    CGSize size = CGSizeMake(im1.size.width, im1.size.height+im2.size.height);
    UIGraphicsBeginImageContext(size);
    
    CGPoint image1Point = CGPointMake(0, 0);
    [im1 drawAtPoint:image1Point];
    
    CGPoint image2Point = CGPointMake(0, im1.size.height);
    [im2 drawAtPoint:image2Point];
    
    UIImage* finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}*/

/*- (UIImage*) generateTogetherView
{
    NSArray* tmp = [NSMutableArray arrayWithArray:[DataBase getDBArray:@"patient_therapy" withKey:@"patientid" andValue:self.currentPatient[@"patientid"]]];
    NSMutableArray* to_convert = [[NSMutableArray alloc] init];
    
    for(NSDictionary* p in tmp)
    {
        if([p[@"selected"] isEqualToString:@"true"])
        {
            [to_convert addObject:p];
        }
    }

    TherapyViewController *therapyViewController = [[TherapyViewController alloc] initWithTherapies:to_convert andPatient:self.currentPatient];
    [self.navigationController pushViewController:therapyViewController animated:NO];
    return nil;
}*/

- (void) printItem:(UIImage*) img
{
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    //pic.delegate = self;
    
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = @"PrintJob";
    pic.printInfo = printInfo;
    
    
    pic.showsPageRange = YES;
    pic.printingItem = img;
    
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if (!completed && error) {
            NSLog(@"Printing could not complete because of error: %@", error);
        }
    };
    
    
    [pic presentAnimated:YES completionHandler:completionHandler];
}

- (void) onConvert:(UIImage*)img
{
    
    UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
    
    [self.view makeToast:[NSString stringWithFormat:@"Изображение сохранено в галерее"]];
}



/*- (void) onPrint:(UIImage*) img
{
    [self printItem:img];
}

- (void) onConvert
{
    
    UIImageWriteToSavedPhotosAlbum([self generateTogetherView], nil, nil, nil);
    
    [self.view makeToast:[NSString stringWithFormat:@"Изображение сохранено в галерее"]];
}*/

- (void)showEmail:(UIImage*) img {
    
    NSString *emailTitle = [NSString stringWithFormat:@"%@ %@ %@", self.currentPatient[@"firstname"], self.currentPatient[@"middlename"], self.currentPatient[@"lastname"]];
    NSString *messageBody = @"";
    NSArray *toRecipents = [[NSArray alloc] init];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail])
    {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [mc.navigationBar setTintColor:[UIColor orangeColor]];
        
        
        
        //UIImage* img = [self generateTogetherView];
        
        // Get the resource path and read the file using NSData
        NSData *fileData = UIImagePNGRepresentation(img);// = [NSData dataWithContentsOfFile:filePath];
        
        // Determine the MIME type
        NSString *mimeType = @"image/png";
        
        // Add attachment
        [mc addAttachmentData:fileData mimeType:mimeType fileName:@"test"];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onEmail:(UIImage*) img
{
    [self showEmail:img];
}


@end
