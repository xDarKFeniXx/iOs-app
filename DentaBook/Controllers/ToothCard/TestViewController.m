//
//  TestViewController.m
//  DentaBook
//
//  Created by Denis Dubov on 15.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIView *bbb;
@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 100, 200, 160)];
    [self.view addSubview:self.scrollView];
    self.bbb = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, 160)];
    self.bbb.backgroundColor = [UIColor yellowColor];
}

-(void) viewDidLayoutSubviews   {
    
    [self.scrollView addSubview:self.bbb];
    self.scrollView.contentSize = self.bbb.frame.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
