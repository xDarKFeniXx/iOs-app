//
//  ToothCardListViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "SWTableViewCell.h"

@class Patient;

@interface ToothCardListViewController : UIViewController <CustomIOS7AlertViewDelegate>

-(id) initWithPatient:(NSDictionary*)patient;
@property (nonatomic, retain) NSMutableArray* tooths;
@property (nonatomic, retain) NSArray* diagnoses;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;

@end
