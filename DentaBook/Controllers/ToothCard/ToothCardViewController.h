//
//  ToothCardViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBToothMapScroller.h"


@class Patient, ToothList;

@interface ToothCardViewController : UIViewController <UITextViewDelegate>

-(id) initWithToothMode:(ToothMapMode)mode withPatient:(NSDictionary*)patient;
-(id) initShowToothList:(NSDictionary*)toothList withPatient:(NSDictionary*)patient;
-(id) initShowToothListForce:(NSDictionary*)toothList withPatient:(NSDictionary*)patient;

@property (nonatomic, retain) NSArray* diagnoses;
@property (nonatomic, retain) UIBarButtonItem* edit_btn;
@property (nonatomic, retain) UIBarButtonItem* save_btn;
@property (nonatomic) BOOL isCreationMode;
@property (weak, nonatomic) IBOutlet UIButton *callMenuBtn;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


@end
