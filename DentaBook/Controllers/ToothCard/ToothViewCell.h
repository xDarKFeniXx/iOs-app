//
//  ToothViewCell.h
//  DentaBook
//
//  Created by Denis Dubov on 12.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface ToothViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *toothBoxView;
@property (nonatomic, retain) NSDictionary* toothmap;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@end
