//
//  ToothViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 12.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "ToothViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ToothViewCell
@synthesize toothmap, shadowView, toothBoxView;
- (void)awakeFromNib {
    // Initialization code
    self.offset = 10;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    /*shadowView.layer.cornerRadius=3;
    shadowView.layer.masksToBounds=YES;
    toothBoxView.layer.cornerRadius=3;
    toothBoxView.layer.masksToBounds=YES;*/
    
    toothBoxView.layer.shadowColor = [UIColor blackColor].CGColor;
    toothBoxView.layer.shadowOpacity = 0.5;
    toothBoxView.layer.shadowRadius = 2;
    toothBoxView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    toothBoxView.layer.cornerRadius = 2;
    toothBoxView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) cellSwiped
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toothcardCopied" object:toothmap];
}

@end
