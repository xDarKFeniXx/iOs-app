//
//  VisitHistoryViewCell.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "VisitHistoryViewCell.h"

@implementation VisitHistoryViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    for(UIView* view in self.subviews)
    {
        if(view != self.contentView)
            [view removeFromSuperview];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
