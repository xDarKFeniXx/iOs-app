//
//  VisitHistoryViewController.h
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Patient;

@interface VisitHistoryViewController : UIViewController <UIGestureRecognizerDelegate>

-(id) initWithPatient:(NSDictionary*)patient;
@property(nonatomic, retain) NSArray* visits;
@property(nonatomic) BOOL needLast;
@property(nonatomic, retain) NSIndexPath* lastIndexPath;
@property(nonatomic, retain) NSDate* lastVisitDate;
@property(nonatomic, retain) NSString* lastVisitDateString;
@property (weak, nonatomic) IBOutlet UIImageView *illnessImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) NSMutableArray* selected_items;
@property (weak, nonatomic) IBOutlet UIView *optionView;
- (IBAction)tapOptional:(id)sender;


@end
