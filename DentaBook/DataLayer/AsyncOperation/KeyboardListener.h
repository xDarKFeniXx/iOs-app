//
//  KeyboardListener.h
//  DentaBook
//
//  Created by Mac Mini on 13.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyboardListener : NSObject
+(KeyboardListener*) sharedInstance;

// clue for improper use (produces compile time error)
+(KeyboardListener*) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(KeyboardListener*) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(KeyboardListener*) new __attribute__((unavailable("new not available, call sharedInstance instead")));

@property (nonatomic, getter=isVisible) BOOL visible;

@end
