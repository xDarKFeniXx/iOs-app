//
//  KeyboardListener.m
//  DentaBook
//
//  Created by Mac Mini on 13.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "KeyboardListener.h"

@implementation KeyboardListener
@synthesize visible;
+(KeyboardListener*) sharedInstance {
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

-(KeyboardListener*) initUniqueInstance {
    if ((self = [super init])) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(didShow) name:UIKeyboardDidShowNotification object:nil];
        [center addObserver:self selector:@selector(didHide) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (BOOL)isVisible
{
    return visible;
}

- (void)didShow
{
   visible = YES;
}

- (void)didHide
{
    //visible = NO;
}


@end
