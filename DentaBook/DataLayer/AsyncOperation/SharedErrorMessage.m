//
//  ErrorMessage.m
//  FuckinRotation
//
//  Created by Instream on 17.07.13.
//  Copyright (c) 2013 Instream. All rights reserved.
//

#import "SharedErrorMessage.h"

@implementation SharedErrorMessage

@synthesize title, message;

void Err(NSString* title, NSString* message)
{
    SharedErrorMessage* mes=[SharedErrorMessage sharedInstance];
    mes.title=title;
    mes.message=message;
}

void ErrC(NSString* title, const char * message)
{
    SharedErrorMessage* mes=[SharedErrorMessage sharedInstance];
    mes.title=title;
    mes.message=[NSString stringWithUTF8String:message];
}

+(SharedErrorMessage*) sharedInstance {
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

-(SharedErrorMessage*) initUniqueInstance {
    return [super init];
}

- (NSString*) description
{
    return [[NSString alloc] initWithFormat:@"%@: %@",title, message];
}

@end
