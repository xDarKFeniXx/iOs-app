//
//  FileUtils.m
//  DFM
//
//  Created by Evgeniy Krivoshein on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileUtils.h"

@implementation FileUtils

+ (BOOL)existsFile:(NSString *)fileName
{
    BOOL exist = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:NSHomeDirectory()];
    NSString *name;
    while (name = [dirEnum nextObject]) {
        if([name compare:fileName] == NSOrderedSame)
        {
            exist = YES;
            break;
        }
    }
    return exist;
}

+ (NSString *)findFile:(NSString *)fileName withExtension:(NSString *)fileExt
{
    return [[NSBundle mainBundle] pathForResource:fileName ofType:fileExt];
}

+ (void)copyFile:(NSString *)orgName withNewName:(NSString *)newName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSData *fileData = [NSData dataWithContentsOfFile:orgName];
    if(fileData)
    {
        BOOL success = [fileManager createFileAtPath:newName contents:fileData attributes:nil];
        if( success == NO)
        {
            NSLog(@"Error create file.");
        }
    }
    else
    {
        NSLog(@"Error open file");
    }
}

@end
