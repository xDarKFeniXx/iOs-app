//
//  Param.m
//  DentaBook
//
//  Created by Mac Mini on 20.10.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Param.h"

@implementation Param
@synthesize name, val, type, relation;

+ (Param*) paramWithName:(NSString*) name stringValue:(NSString*) val andType:(NSString*) type
{
    Param* p=[[Param alloc] init];
    p.name = name;
    p.val = val;
    p.type = type;
    return p;
}

+ (Param*) paramWithName:(NSString*) name intValue:(int) val andType:(NSString*) type
{
    Param* p=[[Param alloc] init];
    p.name = name;
    p.val = [NSString stringWithFormat:@"%d",val];
    p.type = type;
    return p;
}

+ (Param*) paramWithName:(NSString*) name floatValue:(float) val andType:(NSString*) type
{
    Param* p=[[Param alloc] init];
    p.name = name;
    p.val = [NSString stringWithFormat:@"%f",val];
    p.type = type;
    return p;
}

+ (Param*) paramWithName:(NSString*) name doubleValue:(double) val andType:(NSString*) type
{
    Param* p=[[Param alloc] init];
    p.name = name;
    p.val = [NSString stringWithFormat:@"%f",val];
    p.type = type;
    return p;
}

- (id) init
{
    self=[super init];
    if (self) {
        self.name = nil;
        self.val = nil;
        self.type = nil;
    }
    return self;
}

- (NSString*) castType
{
    if([[type uppercaseString] isEqualToString:@"STRING"])
    {
        return @"TEXT";
    }
    else if([[type uppercaseString] isEqualToString:@"FLOAT"]||[[type uppercaseString] isEqualToString:@"INT"]||[[type uppercaseString] isEqualToString:@"DOUBLE"])
    {
        return @"NUMERIC";
    }
    else
    {
        return @"TEXT";
    }
}
@end
