//
//  PatientAutocompleteObj.h
//  DentaBook
//
//  Created by Mac Mini on 14.11.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompletionObject.h"

@interface PatientAutocompleteObj : NSObject <MLPAutoCompletionObject>
@property (nonatomic, retain) NSDictionary* pat;
-(id) initWithPatient:(NSDictionary*)patient;
@end
