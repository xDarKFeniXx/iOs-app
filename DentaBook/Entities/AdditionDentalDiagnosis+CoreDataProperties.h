//
//  AdditionDentalDiagnosis+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 05.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AdditionDentalDiagnosis.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdditionDentalDiagnosis (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *remoteId;

@end

NS_ASSUME_NONNULL_END
