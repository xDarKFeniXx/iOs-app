//
//  Affair+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Affair+CoreDataProperties.h"

@implementation Affair (CoreDataProperties)

@dynamic createAt;
@dynamic date;
@dynamic isDone;
@dynamic notifyDistance;
@dynamic title;
@dynamic createdAt;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic affair_group;

@end
