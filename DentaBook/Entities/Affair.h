//
//  Affair.h
//  DentaBook
//
//  Created by Denis Dubov on 04.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AffairGroup;

NS_ASSUME_NONNULL_BEGIN

@interface Affair : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Affair+CoreDataProperties.h"
