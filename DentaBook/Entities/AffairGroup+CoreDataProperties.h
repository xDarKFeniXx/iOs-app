//
//  AffairGroup+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AffairGroup.h"

NS_ASSUME_NONNULL_BEGIN

@interface AffairGroup (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSSet<Affair *> *affairs;
@property (nullable, nonatomic, retain) User *user;

@end

@interface AffairGroup (CoreDataGeneratedAccessors)

- (void)addAffairsObject:(Affair *)value;
- (void)removeAffairsObject:(Affair *)value;
- (void)addAffairs:(NSSet<Affair *> *)values;
- (void)removeAffairs:(NSSet<Affair *> *)values;

@end

NS_ASSUME_NONNULL_END
