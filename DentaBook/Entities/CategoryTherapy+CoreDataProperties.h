//
//  CategoryTherapy+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CategoryTherapy.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoryTherapy (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSSet<Service *> *services;

@end

@interface CategoryTherapy (CoreDataGeneratedAccessors)

- (void)addServicesObject:(Service *)value;
- (void)removeServicesObject:(Service *)value;
- (void)addServices:(NSSet<Service *> *)values;
- (void)removeServices:(NSSet<Service *> *)values;

@end

NS_ASSUME_NONNULL_END
