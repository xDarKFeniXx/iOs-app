//
//  CategoryTherapy+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CategoryTherapy+CoreDataProperties.h"

@implementation CategoryTherapy (CoreDataProperties)

@dynamic name;
@dynamic remoteId;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic services;

@end
