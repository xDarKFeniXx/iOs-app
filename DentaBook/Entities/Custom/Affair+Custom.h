//
//  Affair+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Affair.h"
#import "FastEasyMapping.h"

@interface Affair (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
