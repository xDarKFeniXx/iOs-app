//
//  Gallery+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Gallery.h"
#import "FastEasyMapping.h"

@interface Gallery (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
