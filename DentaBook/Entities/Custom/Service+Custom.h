//
//  Service+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "Service.h"
#import "FastEasyMapping.h"

@interface Service (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
