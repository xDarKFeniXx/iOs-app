//
//  TherapyItem+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "TherapyItem.h"
#import "FastEasyMapping.h"

@interface TherapyItem (Custom)

+ (FEMManagedObjectMapping *)mapObject;

@end
