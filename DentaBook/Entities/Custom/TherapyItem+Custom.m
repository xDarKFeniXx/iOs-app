//
//  TherapyItem+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "TherapyItem+Custom.h"

@implementation TherapyItem (Custom)

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"TherapyItem"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromDictionary:@{@"categoryId": @"categoryId"}];
                                               [mapping addAttributesFromDictionary:@{@"serviceId": @"serviceId"}];
                                               [mapping addAttributesFromDictionary:@{@"toothId": @"toothId"}];
                                               [mapping addAttributesFromDictionary:@{@"count": @"count"}];
                                               [mapping addAttributesFromDictionary:@{@"toothId": @"toothId"}];
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createAt"
                                                                                           toKeyPath:@"date"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
