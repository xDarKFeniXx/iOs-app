//
//  ToothList+Custom.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "ToothList.h"
#import "FastEasyMapping.h"

@interface ToothList (Custom)

+ (FEMManagedObjectMapping *)mapObject;
+ (FEMManagedObjectMapping *)mapObjectSync;

@end
