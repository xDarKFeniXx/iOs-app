//
//  User+Custom.m
//  DentaBook
//
//  Created by Denis Dubov on 27.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "User+Custom.h"
#import "AffairGroup+Custom.h"
#import "Expense+Custom.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation User (Custom)

+(User*) currentUser    {
    return [User MR_findFirst];
}

+ (FEMManagedObjectMapping *)mapObject {
    
    return [FEMManagedObjectMapping mappingForEntityName:@"User"
                                           configuration:^(FEMManagedObjectMapping *mapping) {
                                               [mapping setPrimaryKey:@"remoteId"];  // object uniquing
                                               
                                               [mapping addAttributesFromDictionary:@{@"remoteId": @"id"}];
                                               [mapping addAttributesFromDictionary:@{@"login": @"login"}];
                                               [mapping addToManyRelationshipMapping:[AffairGroup mapObject] forProperty:@"affair_groups" keyPath:@"Affairs"];
                                               [mapping addToManyRelationshipMapping:[Expense mapObject] forProperty:@"expenses" keyPath:@"Expenses"];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"createdAt"
                                                                                           toKeyPath:@"createdAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                               
                                               [mapping addAttribute:[FEMAttribute mappingOfProperty:@"updatedAt"
                                                                                           toKeyPath:@"updatedAt"
                                                                                          dateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"]];
                                           }];
}

@end
