//
//  DentalDiagnosis+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DentalDiagnosis+CoreDataProperties.h"

@implementation DentalDiagnosis (CoreDataProperties)

@dynamic name;
@dynamic remoteId;
@dynamic symbolName;
@dynamic createdAt;
@dynamic updatedAt;

@end
