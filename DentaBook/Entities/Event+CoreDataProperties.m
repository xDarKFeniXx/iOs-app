//
//  Event+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 10.03.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Event+CoreDataProperties.h"

@implementation Event (CoreDataProperties)

@dynamic clearDate;
@dynamic endDate;
@dynamic isAllDay;
@dynamic startDate;
@dynamic title;
@dynamic type;
@dynamic comment;
@dynamic createdAt;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic user;

@end
