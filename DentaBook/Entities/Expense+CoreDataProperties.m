//
//  Expense+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Expense+CoreDataProperties.h"

@implementation Expense (CoreDataProperties)

@dynamic amount;
@dynamic categoryId;
@dynamic comment;
@dynamic createAt;
@dynamic subCategoryId;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic remoteId;
@dynamic user;

@end
