//
//  ExpenseCategory+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ExpenseCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExpenseCategory (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSSet<ExpenseSubcategory *> *subcategories;

@end

@interface ExpenseCategory (CoreDataGeneratedAccessors)

- (void)addSubcategoriesObject:(ExpenseSubcategory *)value;
- (void)removeSubcategoriesObject:(ExpenseSubcategory *)value;
- (void)addSubcategories:(NSSet<ExpenseSubcategory *> *)values;
- (void)removeSubcategories:(NSSet<ExpenseSubcategory *> *)values;

@end

NS_ASSUME_NONNULL_END
