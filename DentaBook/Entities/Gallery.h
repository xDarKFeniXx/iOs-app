//
//  Gallery.h
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

NS_ASSUME_NONNULL_BEGIN

@interface Gallery : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Gallery+CoreDataProperties.h"
