//
//  Patient+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 17.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Patient+CoreDataProperties.h"

@implementation Patient (CoreDataProperties)

@dynamic createdAt;
@dynamic firstName;
@dynamic lastName;
@dynamic middleName;
@dynamic money;
@dynamic phoneNumber;
@dynamic photoFileName;
@dynamic rating;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic birthDay;
@dynamic birthMonth;
@dynamic birthYear;
@dynamic galleries;
@dynamic patientIllness;
@dynamic payments;
@dynamic therapies;
@dynamic tooth_list;

@end
