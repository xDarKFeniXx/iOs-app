//
//  PatientIllnessRef+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 30.11.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PatientIllnessRef+CoreDataProperties.h"

@implementation PatientIllnessRef (CoreDataProperties)

@dynamic index;
@dynamic patient;

@end
