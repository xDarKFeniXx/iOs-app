//
//  Service+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Service+CoreDataProperties.h"

@implementation Service (CoreDataProperties)

@dynamic cost;
@dynamic name;
@dynamic remoteId;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic category;

@end
