//
//  Therapy+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Therapy.h"

NS_ASSUME_NONNULL_BEGIN

@interface Therapy (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createAt;
@property (nullable, nonatomic, retain) NSNumber *discount;
@property (nullable, nonatomic, retain) NSNumber *isPlan;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) Patient *patient;
@property (nullable, nonatomic, retain) NSSet<TherapyItem *> *therapyItems;

@end

@interface Therapy (CoreDataGeneratedAccessors)

- (void)addTherapyItemsObject:(TherapyItem *)value;
- (void)removeTherapyItemsObject:(TherapyItem *)value;
- (void)addTherapyItems:(NSSet<TherapyItem *> *)values;
- (void)removeTherapyItems:(NSSet<TherapyItem *> *)values;

@end

NS_ASSUME_NONNULL_END
