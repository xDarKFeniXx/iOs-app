//
//  TherapyItem+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TherapyItem+CoreDataProperties.h"

@implementation TherapyItem (CoreDataProperties)

@dynamic categoryId;
@dynamic count;
@dynamic createAt;
@dynamic serviceId;
@dynamic toothId;
@dynamic createdAt;
@dynamic remoteId;
@dynamic updatedAt;
@dynamic therapy;

@end
