//
//  Tooth+CoreDataProperties.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tooth.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tooth (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *additionDentaDiagnosisArray;
@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSNumber *dentalDiagnosisId;
@property (nullable, nonatomic, retain) NSNumber *index;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSNumber *remoteId;
@property (nullable, nonatomic, retain) ToothList *tooth_list;

@end

NS_ASSUME_NONNULL_END
