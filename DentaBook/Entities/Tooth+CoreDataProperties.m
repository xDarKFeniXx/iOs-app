//
//  Tooth+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Tooth+CoreDataProperties.h"

@implementation Tooth (CoreDataProperties)

@dynamic additionDentaDiagnosisArray;
@dynamic comment;
@dynamic dentalDiagnosisId;
@dynamic index;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic remoteId;
@dynamic tooth_list;

@end
