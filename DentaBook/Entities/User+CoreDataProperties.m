//
//  User+CoreDataProperties.m
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic remoteId;
@dynamic login;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic affair_groups;
@dynamic events;
@dynamic expenses;

@end
