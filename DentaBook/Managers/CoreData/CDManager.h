//
//  CDManager.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/19/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDStack.h"

@interface CDManager : NSObject
@property (strong, nonatomic) CDStack* mainStack;

+ (CDManager*)shared;
- (void)applicationWillTerminate;
- (void)cleanAndResetupDB;

@end