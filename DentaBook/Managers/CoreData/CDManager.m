//
//  CDManager.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/19/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//


#import "CDManager.h"

#import <ALO7ProgressiveMigrationManager/ALO7ProgressiveMigrationManager.h>
#import <MagicalRecord/MagicalRecord.h>
#import "NSFileManager+Path.h"

@implementation CDManager

#pragma mark - Shared Instance and Init
+ (CDManager *)shared {
    
    static CDManager *shared = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        shared = [[self alloc] init];
    });
    
    return shared;
}

-(id)init{
    
    self = [super init];
    if (self) {
   		// your initialization here
        NSURL *mainModelURL = [[NSBundle mainBundle] URLForResource:@"DentaBook" withExtension:@"momd"];
        NSURL *mainStoreURL = [[NSFileManager applicationDocumentsDirectoryURL] URLByAppendingPathComponent:@"DentaBook.sqlite"];
        
        self.mainStack = [[CDStack alloc] initWithModelURL:mainModelURL storeURL:mainStoreURL];
    }
    return self;
}

- (void)applicationWillTerminate {
    [MagicalRecord cleanUp];
}

- (void)cleanAndResetupDB
{
    [MagicalRecord cleanUp];
    
    NSString *dbStore = [MagicalRecord defaultStoreName];
    
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:dbStore];
    NSURL *walURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-wal"];
    NSURL *shmURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-shm"];
    
    NSError *error = nil;
    BOOL result = YES;
    
    for (NSURL *url in @[storeURL, walURL, shmURL]) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:url.path]) {
            result = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
            if(!result) {
                NSLog(@"%@", error);
            }
        }
    }
    
    if (result) {
        NSLog(@"All database is clean");

        NSURL *storeUrl = [NSPersistentStore MR_urlForStoreName:@"DentaBook.sqlite"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:storeUrl.path]) {
            NSError *migrationError = nil;
            
            NSString *resource = @"DentaBook";
            
            NSURL *momURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:resource ofType:@"momd"]];
            
            NSManagedObjectModel *targetModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
            [[ALO7ProgressiveMigrationManager sharedManager] migrateStoreAtUrl:storeUrl
                                                                     storeType:NSSQLiteStoreType
                                                                   targetModel:targetModel
                                                                         error:&migrationError];
            
            if (migrationError) {
                NSLog(@"Error migrating to latest model: %@\n %@", migrationError, migrationError.userInfo);
            }
        }
        
        [MagicalRecord setupAutoMigratingCoreDataStack];
        
    } else {
        NSLog(@"An error has occurred while deleting %@ error %@", dbStore, error);
    }
}


@end
