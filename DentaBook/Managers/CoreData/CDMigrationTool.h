//
//  CDMigrationTool.h
//  WelpyApp
//
//  Created by Alexander Bukov on 3/19/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CDMigrationTool : NSObject {
    
}

+ (BOOL) migrateDB:(NSURL*) sourceFilePath toURL:(NSURL*) destURL toModel:(NSManagedObjectModel*) finalModel usingModelsDir:(NSString*) modelsDir;
+ (BOOL) DBExistAtURL:(NSURL*) DBPath;
+ (NSURL*) findExistedLatestVersionDBFile;

+ (NSString*) currentVersionForModel:(NSManagedObjectModel*) model;
+ (NSArray*) versionsMappingArray;
+ (NSManagedObjectModel*) createModelWithVersion:(NSString*) version withModelsDir:(NSString*) modelsDir;
+ (NSManagedObjectModel*) createModelNextAfterModel:(NSManagedObjectModel*) sourceModel withModelsDir:(NSString*) modelsDir;
+ (void) deleteFiles:(NSArray*) files;


@end

