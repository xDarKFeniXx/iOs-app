//
//  CDStack.m
//  WelpyApp
//
//  Created by Alexander Bukov on 3/19/15.
//  Copyright (c) 2015 Welpy Inc. All rights reserved.
//

#import "CDStack.h"
#import "NSFileManager+Path.h"
#import "CDMigrationTool.h"

static NSString *const kCoreDataStoreFileName = @"Welpy.sqlite";

@interface CDStack ()

@end

@implementation CDStack
#pragma mark -

- (id)initWithModelURL:(NSURL*)modelURL storeURL:(NSURL*)storeURL{
    
    self = [super init];
    if (self) {
        self.modelURL = modelURL;
        self.storeURL = storeURL;
    }
    return self;
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Dash" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
	
	NSString *path = [[NSFileManager applicationDocumentsDirectory] stringByAppendingPathComponent:kCoreDataStoreFileName];
    NSURL *storeUrl = [NSURL fileURLWithPath:path];
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
	
	if ( [CDMigrationTool DBExistAtURL:storeUrl] ) {
		
		NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
																								  URL:storeUrl
																								error:&error];
		
		if ( ![self.managedObjectModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata] ) {
			NSLog(@"CDDB exists but  NOT Compatible. Can't continue");
			_persistentStoreCoordinator = nil;
		} else {
			if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
				NSLog(@"Can't add persistant store: %@ \nwith path:[%@]", [error description], storeUrl);
				_persistentStoreCoordinator = nil;
			}
		}
	} else {
		
		NSURL *oldVersionDBFile = [CDMigrationTool findExistedLatestVersionDBFile];
		if ( oldVersionDBFile == nil ) {
			if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
				NSLog(@"Can't create persistant store: %@ \nwith path:[%@]", [error description], storeUrl);
			}
		} else {
            
			BOOL migrated = [CDMigrationTool migrateDB:oldVersionDBFile toURL:storeUrl toModel:self.managedObjectModel usingModelsDir:[[NSBundle mainBundle] pathForResource:@"Dash" ofType:@"momd"]];
            
			NSLog(@"Migrated %@succesfull", (migrated) ? @" " : @"NOT ");
			
			if ( [[_persistentStoreCoordinator persistentStores] count] == 0 && [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error] == nil) {
				NSLog(@"Can't create persistant store: %@ \nwith path:[%@]", [error description], storeUrl);
				_persistentStoreCoordinator = nil;
			}
		}
	}
    
    return _persistentStoreCoordinator;
}


- (NSError*)save {
	NSError *saveError = nil;
	[_managedObjectContext save:&saveError];
    
//	if (saveError){
//		[CDCategories defaultErrorHandler:saveError];
//	}
	return saveError;
}

- (BOOL)hasChanges{
	return [_managedObjectContext hasChanges];
}


@end
