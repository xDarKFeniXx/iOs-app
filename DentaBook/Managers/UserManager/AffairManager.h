//
//  AffairManager.h
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class AffairGroup, Affair;

@interface AffairManager : NSObject <ABMultitonProtocol>

-(void) createAffair:(NSDictionary*)affairGroup
             success:(void (^)())success
             failure:(void (^)())failure;

-(void) deleteAffairById:(NSString*)remoteId
                 success:(void (^)())success
                 failure:(void (^)())failure;

-(void) createAffairItem:(NSDictionary*)affairItem
                 success:(void (^)())success
                 failure:(void (^)())failure;

-(void) deleteAffairItemById:(NSString*)remoteId
                     success:(void (^)())success
                     failure:(void (^)())failure;

/*-(void) updateAffairItem:(Affair*)affairItem
                 success:(void (^)())success
                 failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncAffairs:(void (^)())success
                               failure:(void (^)())failure;*/

@end
