//
//  AffairManager.m
//  DentaBook
//
//  Created by Denis Dubov on 10.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "AffairManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "AffairGroup+Custom.h"
#import "Affair+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation AffairManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createAffair:(NSDictionary*)affairGroup
             success:(void (^)())success
             failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:affairGroup.createAt];
    
    id params = @{@"title": affairGroup.title,
                  @"createDate":dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:affairGroup toTable:@"affair_group" withKey:@"id"];
    if(success)
        success();
    /*[DataManager post:[NSString stringWithFormat:@"affair"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [AffairGroup mapObject];
                      [FEMManagedObjectDeserializer fillObject:affairGroup fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteAffairById:(NSString*)remoteId
                 success:(void (^)())success
                 failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"affair_group" withKey:@"id" andValue:remoteId];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"affair/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    AffairGroup* affairEntity = [AffairGroup MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (affairEntity) {
                        [affairEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(void) createAffairItem:(NSDictionary*)affairItem
                 success:(void (^)())success
                 failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:affairItem.createAt];
    NSString *dateString2 = [dateFormatter stringFromDate:affairItem.date];
    
    id params = @{@"id": affairItem.affair_group.remoteId,
                  @"title": affairItem.title,
                  @"isDone": affairItem.isDone,
                  @"createDate": dateString,
                  @"notifyDate": dateString2,
                  @"notifyDistance": affairItem.notifyDistance};*/
    
    [DataManager loadCookies];
    [DataBase putDB:affairItem toTable:@"affair" withKey:@"id"];
    if(success)
        success();
    /*[DataManager post:[NSString stringWithFormat:@"affairItem"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Affair mapObject];
                      [FEMManagedObjectDeserializer fillObject:affairItem fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteAffairItemById:(NSString*)remoteId
                     success:(void (^)())success
                     failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    
    [DataBase killDB:@"affair" withKey:@"id" andValue:remoteId];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"affairItem/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    Affair* affairEntity = [Affair MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (affairEntity) {
                        [affairEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

/*-(void) updateAffairItem:(Affair*)affairItem
                 success:(void (^)())success
                 failure:(void (^)())failure   {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormatter stringFromDate:affairItem.createAt];
    NSString *dateString2 = [dateFormatter stringFromDate:affairItem.date];
    
    id params = @{@"id": affairItem.remoteId,
                  @"title": affairItem.title,
                  @"isDone": affairItem.isDone,
                  @"createDate": dateString,
                  @"notifyDate": dateString2,
                  @"notifyDistance": affairItem.notifyDistance};
    
    [DataManager loadCookies];
    [DataManager put:[NSString stringWithFormat:@"affairItem"]
              params:params
             success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Affair mapObject];
                      [FEMManagedObjectDeserializer fillObject:affairItem fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];
}

-(AFHTTPRequestOperation*) syncAffairs:(void (^)())success
                               failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"affair"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  AffairGroup* affairGroupEntity = [AffairGroup MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!affairGroupEntity) {
                                                                      affairGroupEntity = [AffairGroup MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [AffairGroup mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:affairGroupEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}*/

@end
