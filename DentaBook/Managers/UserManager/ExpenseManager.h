//
//  ExpenseManager.h
//  DentaBook
//
//  Created by Denis Dubov on 11.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class ExpenseCategory, ExpenseSubcategory, Expense;

@interface ExpenseManager : NSObject <ABMultitonProtocol>

-(void) createExpenseCategory:(NSDictionary*)expenseCategory
                      success:(void (^)())success
                      failure:(void (^)())failure;
-(void) deleteExpenseCategoryById:(NSString*)remoteId
                          success:(void (^)())success
                          failure:(void (^)())failure;

-(void) createExpenseSubcategory:(NSDictionary*)expenseSubcategory
                         success:(void (^)())success
                         failure:(void (^)())failure;
-(void) deleteExpenseSubcategoryById:(NSString*)remoteId
                             success:(void (^)())success
                             failure:(void (^)())failure;

-(void) createExpense:(NSDictionary*)expense
              success:(void (^)())success
              failure:(void (^)())failure;
-(void) deleteExpenseById:(NSString*)remoteId
                  success:(void (^)())success
                  failure:(void (^)())failure;

/*-(AFHTTPRequestOperation*) syncExpense:(void (^)())success
                               failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncExpenseFixtures:(void (^)())success
                                       failure:(void (^)())failure;*/

@end
