//
//  GalleryManager.m
//  DentaBook
//
//  Created by Denis Dubov on 12.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "GalleryManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "Patient+Custom.h"
#import "Gallery+Custom.h"
#import "DataManager.h"

#import "ImageRequestModel.h"
#import "ImageFileCache.h"
#import "ImageCache.h"
#import "DataBase.h"

@implementation GalleryManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createGallery:(NSDictionary*)galleryEnriry
              success:(void (^)())success
              failure:(void (^)())failure  {
    
    /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:galleryEnriry.createAt];
    
    id params = @{@"id": galleryEnriry.patient.remoteId,
                  @"comment": (galleryEnriry.comment.length > 0)?galleryEnriry.comment:@"",
                  @"date": dateString};*/
    
    [DataManager loadCookies];
    [DataBase putDB:galleryEnriry toTable:@"photo" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"gallery"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      FEMManagedObjectMapping *mapping = [Gallery mapObject];
                      [FEMManagedObjectDeserializer fillObject:galleryEnriry fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

- (void)uploadPhoto:(UIImage*)image
          toPatient:(NSDictionary*)galleryEnriry
         completion:(void(^)(NSError* error))completion {
    
    if(!image)
        return;
    
    ImageRequestModel *imageRequestModel = [[ImageRequestModel alloc] init];
    imageRequestModel.urlString = [NSString stringWithFormat:[DataManager urlForPath:@"gallery/%@/image"], galleryEnriry[@"id"]];
    [imageRequestModel.fileCache cacheImage:image forKey:imageRequestModel.md5Hash];
    [imageRequestModel.memoryCache cacheImage:image forKey:imageRequestModel.md5Hash];
    
}

- (void)uploadPhoto:(UIImage*)image
          toAffair:(NSDictionary*)galleryEntity
         completion:(void(^)(NSError* error))completion {
    
    if(!image)
        return;
    
    ImageRequestModel *imageRequestModel = [[ImageRequestModel alloc] init];
    imageRequestModel.urlString = [NSString stringWithFormat:[DataManager urlForPath:@"affairs/%@/image"], galleryEntity[@"id"]];
    [imageRequestModel.fileCache cacheImage:image forKey:imageRequestModel.md5Hash];
    [imageRequestModel.memoryCache cacheImage:image forKey:imageRequestModel.md5Hash];
    
}

- (void)uploadPhoto:(UIImage*)image
           toExpence:(NSDictionary*)galleryEntity
         completion:(void(^)(NSError* error))completion {
    
    if(!image)
        return;
    
    ImageRequestModel *imageRequestModel = [[ImageRequestModel alloc] init];
    imageRequestModel.urlString = [NSString stringWithFormat:[DataManager urlForPath:@"expences/%@/image"], galleryEntity[@"id"]];
    [imageRequestModel.fileCache cacheImage:image forKey:imageRequestModel.md5Hash];
    [imageRequestModel.memoryCache cacheImage:image forKey:imageRequestModel.md5Hash];
    
}

- (void)uploadPhoto:(UIImage*)image
         forProfile:(BOOL) profile
         completion:(void(^)(NSError* error))completion{
    
    if(!image)
        return;
    
    ImageRequestModel *imageRequestModel = [[ImageRequestModel alloc] init];
    imageRequestModel.urlString = [NSString stringWithFormat:[DataManager urlForPath:@"profile/%@/image"], @"yes"];
    [imageRequestModel.fileCache cacheImage:image forKey:imageRequestModel.md5Hash];
    [imageRequestModel.memoryCache cacheImage:image forKey:imageRequestModel.md5Hash];
    
}


@end
