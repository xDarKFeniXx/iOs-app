//
//  IllnessManager.h
//  DentaBook
//
//  Created by Denis Dubov on 05.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@interface IllnessManager : NSObject <ABMultitonProtocol>

-(void) createIllnessByTitle:(NSString*)title
                     success:(void (^)())success
                     failure:(void (^)())failure;

-(void) deleteIllnessById:(NSNumber*)remoteId
                  success:(void (^)())success
                  failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncIllness:(void (^)())success
                               failure:(void (^)())failure;

@end
