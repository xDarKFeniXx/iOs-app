//
//  PatientManager.h
//  DentaBook
//
//  Created by Denis Dubov on 08.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import <ABMultiton/ABMultitonProtocol.h>
#import <AFNetworking/AFNetworking.h>

@class Patient;

@interface PatientManager : NSObject <ABMultitonProtocol>

-(void) createPatient:(NSDictionary*)patientEntity
              success:(void (^)())success
              failure:(void (^)())failure;

-(void) updatePatient:(NSDictionary*)patientEntity
              success:(void (^)())success
              failure:(void (^)())failure;

-(void) destroyPatient:(NSDictionary*)patient
               success:(void (^)())success
               failure:(void (^)())failure;

-(AFHTTPRequestOperation*) syncPatients:(void (^)())success
                                failure:(void (^)())failure;

- (void) uploadLogo:(UIImage*)image
          toPatient:(Patient*)patient
         completion:(void(^)(NSError* error))completion;

@end
