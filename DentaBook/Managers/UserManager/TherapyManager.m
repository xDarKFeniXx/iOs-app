//
//  TherapyManager.m
//  DentaBook
//
//  Created by Denis Dubov on 06.02.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "TherapyManager.h"
#import "SafeCategories.h"

#import <ABMultiton/ABMultiton.h>
#import <MagicalRecord/MagicalRecord.h>

#import "CategoryTherapy+Custom.h"
#import "Service+Custom.h"
#import "DataManager.h"
#import "DataBase.h"

@implementation TherapyManager

+ (id)sharedInstance
{
    return [ABMultiton sharedInstanceOfClass:[self class]];
}

-(id)init{
    
    self = [super init];
    if (self) {
        //
    }
    return self;
}

-(void) createTherapyCategoryByTitle:(NSString*)title
                             success:(void (^)())success
                             failure:(void (^)())failure  {
    
    id params = @{@"id":GUIDString(), @"title": title};
    
    [DataManager loadCookies];
    [DataBase putDB:params toTable:@"services_group" withKey:@"title"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"therapyCategory"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      CategoryTherapy* categoryTherapyEntity = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!categoryTherapyEntity) {
                          categoryTherapyEntity = [CategoryTherapy MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [CategoryTherapy mapObject];
                      [FEMManagedObjectDeserializer fillObject:categoryTherapyEntity fromExternalRepresentation:json usingMapping:mapping];
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteTherapyCategoryById:(NSString*)_id
                          success:(void (^)())success
                          failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"services_group" withKey:@"id" andValue:_id];
    [DataBase killDB:@"services" withKey:@"category_id" andValue:_id];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"therapyCategory/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    CategoryTherapy* categoryTherapyEntity = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (categoryTherapyEntity) {
                        [categoryTherapyEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(void) createTherapyServiceByTitle:(NSString*)title
                               cost:(double)cost
                     linkToRemoteId:(NSString*)remoteId
                            success:(void (^)())success
                            failure:(void (^)())failure  {
    
    id params = @{
                  @"category_id": remoteId,
                  @"id": GUIDString(),
                  @"title": title,
                  @"cost":[NSNumber numberWithDouble:cost]};
    
    [DataManager loadCookies];
    [DataBase putDB:params toTable:@"services" withKey:@"id"];
    success();
    /*[DataManager post:[NSString stringWithFormat:@"therapyService"]
               params:params
              success:^(AFHTTPRequestOperation *operation, id json) {
                  
                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSNumber* remoteID = [json safeNumberObjectForKey:@"id"];
                      
                      Service* serviceTherapyEntity = [Service MR_findFirstByAttribute:@"remoteId" withValue:remoteID];
                      if (!serviceTherapyEntity) {
                          serviceTherapyEntity = [Service MR_createEntity];
                      }
                      
                      FEMManagedObjectMapping *mapping = [Service mapObject];
                      [FEMManagedObjectDeserializer fillObject:serviceTherapyEntity fromExternalRepresentation:json usingMapping:mapping];
                      
                      CategoryTherapy* categoryTherapyEntity = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                      if (categoryTherapyEntity) {
                          serviceTherapyEntity.category = categoryTherapyEntity;
                      }
                      
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                      
                      success();
                  } else {
                      failure();
                  }
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  failure();
              }];*/
}

-(void) deleteTherapyServiceById:(NSString*)_id
                         success:(void (^)())success
                         failure:(void (^)())failure  {
    
    [DataManager loadCookies];
    [DataBase killDB:@"services" withKey:@"id" andValue:_id];
    success();
    /*[DataManager delete:[NSString stringWithFormat:@"therapyService/%ld", [remoteId longValue]]
                 params:nil
                success:^(AFHTTPRequestOperation *operation, id json) {
                    
                    Service* serviceTherapyEntity = [Service MR_findFirstByAttribute:@"remoteId" withValue:remoteId];
                    if (serviceTherapyEntity) {
                        [serviceTherapyEntity MR_deleteEntity];
                        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    }
                    
                    success();
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    failure();
                }];*/
}

-(AFHTTPRequestOperation*) syncTherapyCategory:(void (^)())success
                                       failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"therapyCategory"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  
                                                                  CategoryTherapy* categoryTherapyEntity = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!categoryTherapyEntity) {
                                                                      categoryTherapyEntity = [CategoryTherapy MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [CategoryTherapy mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:categoryTherapyEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}

-(AFHTTPRequestOperation*) syncTherapyServices:(void (^)())success
                                       failure:(void (^)())failure  {
    return [DataManager createGetHTTPRequestOperation:[NSString stringWithFormat:@"therapyService"]
                                               params:nil
                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                  if ([responseObject isKindOfClass:[NSArray class]]) {
                                                      [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                          for (NSDictionary* jsonDict in responseObject) {
                                                              if ([jsonDict isKindOfClass:[NSDictionary class]]) {
                                                                  NSNumber* remoteID = [jsonDict safeNumberObjectForKey:@"id"];
                                                                  NSNumber* categoryRemoteID = [jsonDict safeNumberObjectForKey:@"TherapyCategoryId"];
                                                                  
                                                                  Service* serviceTherapyEntity = [Service MR_findFirstByAttribute:@"remoteId" withValue:remoteID inContext:localContext];
                                                                  if (!serviceTherapyEntity) {
                                                                      serviceTherapyEntity = [Service MR_createEntityInContext:localContext];
                                                                  }
                                                                  
                                                                  FEMManagedObjectMapping *mapping = [Service mapObject];
                                                                  [FEMManagedObjectDeserializer fillObject:serviceTherapyEntity fromExternalRepresentation:jsonDict usingMapping:mapping];
                                                                  
                                                                  CategoryTherapy* categoryTherapyEntity = [CategoryTherapy MR_findFirstByAttribute:@"remoteId" withValue:categoryRemoteID inContext:localContext];
                                                                  if (categoryTherapyEntity) {
                                                                      serviceTherapyEntity.category = categoryTherapyEntity;
                                                                  }
                                                              }
                                                          }
                                                          
                                                      } completion:^(BOOL _success, NSError *error) {
                                                          success();
                                                      }];
                                                  }else{
                                                      failure();
                                                  }
                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  failure();
                                              }];
}

@end
