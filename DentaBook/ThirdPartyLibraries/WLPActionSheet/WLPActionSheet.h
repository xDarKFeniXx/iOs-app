//
//  WLPActionSheet.h
//  WelpyApp
//
//  Created by Denis Dubov on 01.08.14.
//  Copyright (c) 2014 Welpy Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIControlStateAll UIControlStateNormal & UIControlStateSelected & UIControlStateHighlighted
#define SYSTEM_VERSION_LESS_THAN(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] == NSOrderedAscending)

@class WLPActionSheet, WLPActionSheetTitleView;

@protocol WLPActionSheetDelegate <NSObject>
-(void)actionSheetDidDone:(WLPActionSheet*)actionSheet;
-(void)actionSheetDidDestructive:(WLPActionSheet*)actionSheet;
-(void)actionSheetDidMore:(WLPActionSheet*)actionSheet;
-(void)actionSheetDidDissapear:(WLPActionSheet*)actionSheet;
@end

@interface WLPActionSheet : UIView

@property (nonatomic, strong) id <WLPActionSheetDelegate> delegate;
@property UIView *transparentView;
@property WLPActionSheetTitleView *titleView;
@property NSMutableArray *buttons;

- (void)showInView:(UIView *)theView;
- (void)removeFromView;
- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle destructiveButtonTitle:(NSString *)destructiveTitle withViewController:(UIViewController*)viewController;
- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle destructiveButtonTitle:(NSString *)destructiveTitle andMoreButton:(NSString *)moreTitle withViewController:(UIViewController*)viewController;

@end


#pragma mark - IBActionSheetButton

@interface WLPActionSheetButton : UIButton


- (id)initWithTopCornersRounded;
- (id)initWithAllCornersRounded;
- (id)initWithBottomCornersRounded;
- (void)setTextColor:(UIColor *)color;
-(void) setButtonImage:(UIImage*)image;

@property NSInteger index;
@property UIColor *originalTextColor, *highlightTextColor;
@property UIColor *originalBackgroundColor, *highlightBackgroundColor;


@end


#pragma mark - IBActionSheetTitleView

@interface WLPActionSheetTitleView : UIView

- (id)initWithTitle:(NSString *)title font:(UIFont *)font;

@property UILabel *titleLabel;

@end

