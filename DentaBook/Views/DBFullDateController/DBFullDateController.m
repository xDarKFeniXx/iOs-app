//
//  DBFullDateController.m
//  DentaBook
//
//  Created by Denis Dubov on 19.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBFullDateController.h"
#import "UIView+DeepIterable.h"
@interface DBFullDateController ()

@end

@implementation DBFullDateController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.datePicker.date = [NSDate date];
    self.datePicker.maximumDate = [NSDate date];
    [self.datePicker setValue:[UIColor colorWithWhite:74.0f/255.0f alpha:1] forKeyPath: @"textColor"];
    //[self.datePicker applyKey:[UIFont systemFontOfSize:14] forPath: @"font"];
    /*CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
    self.transform = scaleTransform;*/
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    self.datePicker.datePickerMode = UIDatePickerModeDate; //or whatever your original mode was
}

- (void) viewDidAppear:(BOOL)animated
{
    CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);
    self.datePicker.transform = scaleTransform;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
