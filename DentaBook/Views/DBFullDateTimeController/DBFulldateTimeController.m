//
//  DBFulldateTimeController.m
//  DentaBook
//
//  Created by Denis Dubov on 19.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBFulldateTimeController.h"

@interface DBFulldateTimeController ()

@end

@implementation DBFulldateTimeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.datePicker.date = [NSDate date];
    [self.datePicker setValue:[UIColor colorWithWhite:74.0f/255.0f alpha:1] forKeyPath: @"textColor"];
    //[self.datePicker applyKey:[UIFont systemFontOfSize:14] forPath: @"font"];
    /*CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
     self.transform = scaleTransform;*/
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime; //or whatever your original mode was
    self.datePicker.minuteInterval = 15; //or whatever your original mode was
}

- (void) viewDidAppear:(BOOL)animated
{
    CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);
    self.datePicker.transform = scaleTransform;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
