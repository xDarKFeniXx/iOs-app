//
//  DBMonthYearController.m
//  DentaBook
//
//  Created by Denis Dubov on 19.12.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBMonthYearController.h"
#import "NTMonthYearPicker.h"

@interface DBMonthYearController ()

@end

@implementation DBMonthYearController

-(id) init  {
    self = [super init];
    if(self)    {
        self.datePicker = [[NTMonthYearPicker alloc] init];
        self.datePicker.datePickerMode = NTMonthYearPickerModeMonthAndYear;
        
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        NSCalendar *cal = [NSCalendar currentCalendar];
        
        [comps setDay:1];
        [comps setMonth:1];
        [comps setYear:2000];
        self.datePicker.minimumDate = [cal dateFromComponents:comps];
        
        // Set maximum date to next month
        // This is optional; default is no max date
        [comps setDay:0];
        [comps setMonth:0];
        [comps setYear:0];
        self.datePicker.maximumDate = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
        
        // Set initial date to last month
        // This is optional; default is current month/year
        self.datePicker.date = [NSDate date];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.datePicker];
    //[self.datePicker setValue:[UIColor colorWithWhite:74.0f/255.0f alpha:1] forKeyPath: @"textColor"];
    //[self.datePicker applyKey:[UIFont systemFontOfSize:14] forPath: @"font"];
    /*CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
     self.transform = scaleTransform;*/
    //self.datePicker.datePickerMode = NTDatepic;
    self.datePicker.datePickerMode = NTMonthYearPickerModeMonthAndYear; //or whatever your original mode was
}

- (void) viewDidAppear:(BOOL)animated
{
    CGAffineTransform scaleTransform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);
    self.datePicker.transform = scaleTransform;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
