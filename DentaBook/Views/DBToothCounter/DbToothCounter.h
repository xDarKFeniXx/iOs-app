//
//  DbToothCounter.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DbToothCounter : UIViewController

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic) NSInteger toothCount;
@property (nonatomic) BOOL need0;

@end
