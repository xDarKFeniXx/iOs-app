//
//  DBToothMapScroller.h
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLPActionSheet.h"

typedef enum {
    kToothMapModeLow = 0,
    kToothMapModeHight,
    kToothMapModeCombine
}ToothMapMode;

@interface DBToothMapScroller : UIViewController <WLPActionSheetDelegate>

-(id) initWithToothMapMode:(ToothMapMode)mode;
-(id) initWithToothCard:(NSMutableDictionary*) card;
-(id) initWithPatient:(NSDictionary*)patient;

@property (nonatomic, retain) NSArray* diagnoses;

//input
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSDictionary *currentPatient;
@property (nonatomic, strong) NSMutableDictionary *toothlist;
@property (nonatomic, strong) id delegate;
@property (nonatomic) NSInteger willSelectToothIndex;
@property (copy, nonatomic) void (^didChooseToothId)(NSIndexPath *indexPath, NSInteger toothId);

//output
@property (nonatomic) NSInteger toothIndex;

@end
