//
//  DBToothMapScroller.m
//  DentaBook
//
//  Created by Denis Dubov on 26.10.15.
//  Copyright © 2015 denisdbv. All rights reserved.
//

#import "DBToothMapScroller.h"
#import "UIView+Helpers.h"
#import "DataBase.h"
#import "HttpUtil.h"

#define BUTTON_LINE     50.0f
#define BUTTON_MARGIN   10.0f
#define CENTER_MARGIN   40.0f
#define BUTTON_ID_DELTA  100

@interface DBToothMapScroller ()
@property (nonatomic) ToothMapMode mode;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSArray *leftTopCombineArray;
@property (nonatomic, strong) NSArray *leftTopArray;
@property (nonatomic, strong) NSArray *rightTopCombineArray;
@property (nonatomic, strong) NSArray *rightTopArray;
@property (nonatomic, strong) NSArray *leftBottomCombineArray;
@property (nonatomic, strong) NSArray *leftBottomArray;
@property (nonatomic, strong) NSArray *rightBottomCombineArray;
@property (nonatomic, strong) NSArray *rightBottomArray;
@property (nonatomic, strong) UIView *contentView;

@end

@implementation DBToothMapScroller 
@synthesize delegate, currentPatient, toothlist, diagnoses;
-(id) initWithToothMapMode:(ToothMapMode)mode   {
    self = [super initWithNibName:@"DBToothMapScroller" bundle:[NSBundle mainBundle]];
    if(self)    {
        self.mode = mode;
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    }
    return self;
}

-(id) initWithPatient:(NSDictionary*)patient
{
    self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
    self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
    self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
    self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
    
    self.leftTopCombineArray = @[@(55),@(54),@(53),@(52),@(51)];
    self.rightTopCombineArray = @[@(61),@(62),@(63),@(64),@(65)];
    self.leftBottomCombineArray = @[@(85),@(84),@(83),@(82),@(81)];
    self.rightBottomCombineArray = @[@(71),@(72),@(73),@(74),@(75)];
    self = [super initWithNibName:@"DBToothMapScroller" bundle:[NSBundle mainBundle]];
    if(self)
    {
        self.currentPatient = patient;
        toothlist = [[NSMutableDictionary alloc] init];
        toothlist[@"id"] = GUIDString();
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"dd.MM.yyyy"];
        toothlist[@"createat"] = [dateformate stringFromDate:[NSDate date]];
        toothlist[@"type"] = @(kToothMapModeCombine);
        toothlist[@"patientid"] = self.currentPatient[@"patientid"];
        toothlist[@"tooth_list"] = [[NSMutableDictionary alloc] init];
        toothlist[@"immutable"] = @"true";
        
        for(NSNumber* num in self.leftTopArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] = [NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightTopArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftBottomArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightBottomArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftTopCombineArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] = [NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightTopCombineArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.leftBottomCombineArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        for(NSNumber* num in self.rightBottomCombineArray)
        {
            toothlist[@"tooth_list"][[num stringValue]] =[NSMutableDictionary dictionaryWithDictionary:@{@"id":GUIDString(),@"indexx":[num stringValue], @"diagnosisid":@"", @"comment":@""}];
        }
        
        [DataBase putDB:toothlist toTable:@"toothmap" withKey:@"id"];
        
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    }
    return self;
}

-(void) updateToothMap:(BOOL)isBlue  {
    NSMutableDictionary *toothMapArray = toothlist[@"tooth_list"];//[Tooth MR_findByAttribute:@"tooth_list" withValue:self.currentToothList];
    
    for(UIView *subView in self.contentView.subviews)   {
        if([subView isKindOfClass:[UIButton class]])   {
            UIButton *btn = (UIButton*)subView;
            
            btn.backgroundColor = [UIColor clearColor];
            btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        } else if([subView isKindOfClass:[UILabel class]])   {
            UILabel *btn = (UILabel*)subView;
            btn.text = @"";
        }
    }
    
    if(toothMapArray.count > 0) {
        for(NSString *tooth_index in [toothMapArray allKeys])
        {
            NSDictionary* tooth = toothMapArray[tooth_index];
            NSInteger tag = [tooth_index integerValue];
            
            UIButton *btn = (UIButton*)[self.contentView viewWithTag:tag];
            if([tooth[@"diagnosisid"] length]>0 || [tooth[@"comment"] length]>0 )
            {
                if(isBlue) {
                    btn.backgroundColor = getColorSchemeTooth();//[UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
                    btn.layer.borderColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0].CGColor;
                } else  {
                    btn.backgroundColor = getColorSchemeTooth();//[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0];
                    btn.layer.borderColor = [UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0].CGColor;
                }
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                NSDictionary *dentaDiagnosis;
                
                for(NSDictionary* diag in diagnoses)
                {
                    if([diag[@"id"] isEqualToString:tooth[@"diagnosisid"]])
                    {
                        dentaDiagnosis=diag;
                        break;
                    }
                }
                UILabel *titleLabel = (UILabel*)[self.contentView viewWithTag:tag + BUTTON_ID_DELTA];
                titleLabel.text = dentaDiagnosis[@"subtitle"];
            }
        }
    }
}

-(id) initWithToothCard:(NSMutableDictionary*) card
{
    self = [super initWithNibName:@"DBToothMapScroller" bundle:[NSBundle mainBundle]];
    if(self)
    {
        toothlist = [NSMutableDictionary dictionaryWithDictionary:card];
        //diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
        self.mode = [toothlist[@"type"] intValue];
        diagnoses = [DataBase getDBArray:@"diagnosis" withKey:@"" andValue:@""];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.toothIndex = 0;
    
    if(self.mode == kToothMapModeHight)
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        self.contentView = [self generateView:50.0 buttonMargin:8.0 centerMargin:16.0 withSmallText:NO];
    }
    else if (self.mode == kToothMapModeLow)
    {
        self.leftTopArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomArray = @[@(71),@(72),@(73),@(74),@(75)];
        self.leftTopCombineArray = nil;
        self.rightTopCombineArray = nil;
        self.leftBottomCombineArray = nil;
        self.rightBottomCombineArray = nil;
        self.contentView = [self generateView:50.0 buttonMargin:8.0 centerMargin:16.0 withSmallText:NO];
    }
    else
    {
        self.leftTopArray = @[@(18),@(17),@(16),@(15),@(14),@(13),@(12),@(11)];
        self.rightTopArray = @[@(21),@(22),@(23),@(24),@(25),@(26),@(27),@(28)];
        self.leftBottomArray = @[@(48),@(47),@(46),@(45),@(44),@(43),@(42),@(41)];
        self.rightBottomArray = @[@(31),@(32),@(33),@(34),@(35),@(36),@(37),@(38)];
        
        self.leftTopCombineArray = @[@(55),@(54),@(53),@(52),@(51)];
        self.rightTopCombineArray = @[@(61),@(62),@(63),@(64),@(65)];
        self.leftBottomCombineArray = @[@(85),@(84),@(83),@(82),@(81)];
        self.rightBottomCombineArray = @[@(71),@(72),@(73),@(74),@(75)];
        
        self.contentView = [self generateView:36.0 buttonMargin:6.0 centerMargin:12.0 withSmallText:NO];
    }
    
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.contentView.frame.size.height);
    self.scrollView.contentSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self.scrollView addSubview:self.contentView];
    
    CGFloat newContentOffsetX = (self.scrollView.contentSize.width - self.scrollView.frame.size.width) / 2;
    self.scrollView.contentOffset = CGPointMake(newContentOffsetX, 0);
    
    [self updateToothMap:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIView*) generateView:(CGFloat)buttonLine buttonMargin:(CGFloat)buttonMargin centerMargin:(CGFloat)centerMargin withSmallText:(BOOL)withSmallText {
    CGFloat width = ((buttonLine * self.leftTopArray.count) + (self.leftTopArray.count * buttonMargin)) * 2 + centerMargin;
    CGFloat height;
    
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
        height = (buttonMargin + buttonLine) * 2 + centerMargin+22;
    else
        height = (buttonMargin*2 + buttonLine) * 4 + centerMargin;
    
    UIView *toothMapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    toothMapView.backgroundColor = [UIColor whiteColor];
    
    NSInteger index = 1;
    if(self.leftTopCombineArray == nil&&self.rightTopCombineArray == nil&&self.leftBottomCombineArray == nil&&self.rightBottomCombineArray == nil)
    {
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin+11;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+11-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin + buttonLine + centerMargin+11;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin + buttonLine + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+11+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin+11;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+11-15.0f, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin + buttonLine + centerMargin+11;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin + buttonLine + centerMargin;;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+11+buttonLine, width, 15.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    else
    {
        index=4;
        for(NSNumber *num in self.leftTopCombineArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftTopArray)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in self.leftBottomArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index=4;
        
        for(NSNumber *num in self.leftBottomCombineArray) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = (index * buttonMargin) + ((index-1) * buttonLine);
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = (index * buttonMargin) + ((index-1) * buttonLine);
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 4;
        
        for(NSNumber *num in [[self.rightTopCombineArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightTopArray reverseObjectEnumerator] allObjects])
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonLine+buttonMargin*2*2;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonLine+buttonMargin*2*2;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top-10.0f, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        index = 1;
        
        for(NSNumber *num in [[self.rightBottomArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*2*2 + buttonLine*2 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
        
        
        index = 4;
        
        for(NSNumber *num in [[self.rightBottomCombineArray reverseObjectEnumerator] allObjects]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.left = width - (index * (buttonMargin + buttonLine));
            button.top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            button.width = buttonLine;
            button.height = buttonLine;
            CGFloat left = width - (index * (buttonMargin + buttonLine));
            CGFloat top = buttonMargin*3*2 + buttonLine*3 + centerMargin;
            CGFloat width = buttonLine;
            CGFloat height = buttonLine;
            [button setTitle:[NSString stringWithFormat:@"%d", [num integerValue]] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if(withSmallText)    {
                button.titleLabel.numberOfLines = 1;
                button.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:5.0];
                button.titleLabel.lineBreakMode = NSLineBreakByClipping;
            }
            NSLog(@"%f %f %f",left, top, width);
            button.tag = [num integerValue];
            button.layer.cornerRadius = (buttonLine/2);
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [button addTarget:self action:@selector(onTooth:) forControlEvents:UIControlEventTouchUpInside];
            [toothMapView addSubview:button];
            NSLog(@"%f %f %f",left, top, width);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(left, top+buttonLine, width, 10.0f)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithRed:41.0/255.0 green:127.0/255.0 blue:184.0/255.0 alpha:1.0];
            titleLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            titleLabel.adjustsFontSizeToFitWidth = YES;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.tag = [num integerValue] + BUTTON_ID_DELTA;
            //titleLabel.text = @"SHEISSE";
            [toothMapView addSubview:titleLabel];
            
            index++;
        }
    }
    
    UIView *vertLineView = [[UIView alloc] initWithFrame:CGRectMake(width/2, 0, 1, height)];
    vertLineView.backgroundColor = [UIColor blackColor];
    [toothMapView addSubview:vertLineView];
    
    UIView *horizLineView = [[UIView alloc] initWithFrame:CGRectMake(0, height/2, width, 1)];
    horizLineView.backgroundColor = [UIColor grayColor];
    [toothMapView addSubview:horizLineView];
    
    return toothMapView;
}

-(void) setWillSelectToothIndex:(NSInteger)willSelectToothIndex {
    _willSelectToothIndex = willSelectToothIndex;

    for(id subView in self.contentView.subviews)    {
        if(subView && [subView isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*)subView;
            
            if(button.tag == willSelectToothIndex)  {
                [button setBackgroundColor:getColorSchemeTooth()/*[UIColor blueColor]*/];
                button.layer.borderColor = (getColorSchemeTooth()).CGColor;
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            } else  {
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                button.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [button setBackgroundColor:[UIColor whiteColor]];
            }
        }
    }
}

-(void) onTooth:(UIButton*)button   {
    self.toothIndex = button.tag;
    
    if(self.didChooseToothId)   {
        self.didChooseToothId(self.indexPath, button.tag);
    }
}

-(void)actionSheetDidDestructive:(WLPActionSheet*)actionSheet   {
    self.toothIndex = 0;
    
    if(self.didChooseToothId)   {
        self.didChooseToothId(self.indexPath, 0);
    }
}

-(void)actionSheetDidMore:(WLPActionSheet*)actionSheet
{
    if(actionSheet.tag == 2)
    {   //Выбор зуба
        if([self.delegate respondsToSelector:@selector(actionSheetDidMore:)])
        {
            [self.delegate actionSheetDidMore:actionSheet];
        }
    }
}

@end
