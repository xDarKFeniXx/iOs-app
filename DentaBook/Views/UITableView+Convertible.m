//
//  UITableView+Convertible.m
//  DentaBook
//
//  Created by Mac Mini on 17.12.16.
//  Copyright © 2016 denisdbv. All rights reserved.
//

#import "UITableView+Convertible.h"

@implementation UITableView (Convertible)

- (UIImage *) imageFromTableView
{
    // tempframe to reset view size after image was created
    /*CGRect tmpFrame = self.frame;//CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
    
    // set new Frame
    CGRect aFrame    = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
    //aFrame.size.height  = [self sizeThatFits:[[UIScreen mainScreen] bounds].size].height;
    //self.frame = aFrame;
    
    // do image magic
    UIGraphicsBeginImageContext([self sizeThatFits:[[UIScreen mainScreen] bounds].size]self.contentSize);
    
    CGContextRef resizedContext = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:resizedContext];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // reset Frame of view to origin
    self.frame = tmpFrame;
    return image;*/
    /*CGRect tmpFrame = self.frame;
    
    CGRect frame = self.frame;
    frame.size.height = self.contentSize.height;
    self.frame = frame;
    
    UIGraphicsBeginImageContext(self.contentSize);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    CGSize p = image.size;
    //self.frame = tmpFrame;
    return image;*/
    
    UIImage* image = nil;
    self.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    UIGraphicsBeginImageContextWithOptions(self.contentSize, NO, 0.0);
    
    CGPoint savedContentOffset = self.contentOffset;
    CGRect savedFrame = self.frame;
    
    self.contentOffset = CGPointZero;
    self.frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
    
    [self.layer renderInContext: UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    self.contentOffset = savedContentOffset;
    self.frame = savedFrame;
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIGraphicsEndImageContext();
    CGSize p = image.size;
    return image;
}

@end
